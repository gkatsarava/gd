@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div class="member-inner-page-candidates">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-12 p-0 pt-5  mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
                    <div class="row">
                        <!-- column -->
                        <div class="col-12 p-0 mb-3 d-none d-md-block">
                            <div class="row">
                                <div class="col-12 p-0">
                                    <div class="col-12">
                                        <div class="col-12">
                                            <nav aria-label="breadcrumb">
                                                <ol class="breadcrumb bg-white pl-0">
                                                    <li class="breadcrumb-item_other active" aria-current="page">
                                                        <a href="{{ url('party-structure') }}" class="href-gray">
                                                            @lang('site.party')
                                                            <svg class="ml-3 mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                                                        </a>
                                                    </li>
                                                    <li class="breadcrumb-item_other active" aria-current="page">
                                                        <a href="{{ url('party-structure') }}" class="href-gray">
                                                            @lang('site.structure')
                                                            <svg class="ml-3 mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                                                        </a>
                                                    </li>

                                                    <li class="breadcrumb-item_other active" aria-current="page">
                                                        {{ $data->position }}
                                                    </li>
                                                </ol>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- column -->
                        <div class="col-12 mb-3 d-block d-md-none " style="margin-left: 20px;margin-bottom: 0px!important;">
                            <div class="col-12 p-0">
                                <div class="col-12 p-0">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12 pl-0">
                                                <nav aria-label="breadcrumb">
                                                    <ol class="breadcrumb bg-white pl-0">
                                                        <a href="{{ url('party-structure') }}">
                                                            <li class="breadcrumb-item_other active" aria-current="page">

                                                                <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M0,7.5,7.5,0,15,7.5" transform="translate(0.75 16.061) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                                                                @lang('site.back')

                                                            </li>
                                                        </a>
                                                    </ol>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->


                        <div class="col-12 p-0 col-md-8 margin-top-30-px">
                            <div class="col-12 p-0">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 d-none d-md-block">
                                            <div class="mb-4 bor-one"></div>
                                            <h1 class="about-title-head">{{ $data->name }}</h1>
                                            <div class="breadcrumb_b pl-38 pr-38 pl-18-px pt-3 pb-3 pr-3 d-flex align-items-center mb-20">
                                                <h1 class="breadcrumb-item_s">{{ $data->position }}</h1>
                                            </div>
                                            @foreach($data->PoliticalPartyPositions as $row)
                                                <div class="breadcrumb_b pl-38 pr-38 pl-18-px pt-3 pb-3 pr-3 d-flex align-items-center mb-20">
                                                    <h1 class="breadcrumb-item_s">{{ $row->name }}</h1>
                                                </div>
                                            @endforeach

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-12 d-none d-md-block share">
                                <div class="d-flex justify-content-between">
                                    <div class="d-flex justify-content-start align-items-center">
                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $request->fullUrl() }}">
                                            <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                                    <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                        <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                        <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                    </g>
                                                    <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"></rect>
                                                        <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                        <a target="_blank" href="http://twitter.com/share?text={{ $data->name }}&url={{ $request->fullUrl() }}">
                                            <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                                    <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                        <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                        <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                    </g>
                                                    <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"></rect>
                                                        <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                        <a href="#"><div class="share-on">გააზიარე</div></a>
                                    </div>
                                    <div class="d-flex justify-content-end align-items-center pr-5">
                                        @if($data->facebook!='')
                                        <a target="_blank" href="{{ $data->facebook }}" class="mr-20">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_808" data-name="Group 808" transform="translate(-356 -793)">
                                                    <rect id="Rectangle_893" data-name="Rectangle 893" width="50" height="50" rx="25" transform="translate(356 793)" fill="#0d8cf1"></rect>
                                                    <g id="Group_762" data-name="Group 762" transform="translate(-628 -2038)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"></rect>
                                                        <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#fff"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                        @endif




                                        @if($data->youtube!='')
                                        <a target="_blank" href="{{ $data->youtube }}" class="mr-20">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_806" data-name="Group 806" transform="translate(-496 -793)">
                                                    <rect id="Rectangle_946" data-name="Rectangle 946" width="50" height="50" rx="25" transform="translate(496 793)" fill="red"></rect>
                                                    <g id="Group_763" data-name="Group 763" transform="translate(-488 -2038)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"></rect>
                                                        <path id="Icon_awesome-youtube" data-name="Icon awesome-youtube" d="M19.25,6.545a2.336,2.336,0,0,0-1.643-1.654A55.2,55.2,0,0,0,10.344,4.5a55.2,55.2,0,0,0-7.263.391A2.336,2.336,0,0,0,1.438,6.545a24.5,24.5,0,0,0-.388,4.5,24.5,24.5,0,0,0,.388,4.5,2.3,2.3,0,0,0,1.643,1.628,55.2,55.2,0,0,0,7.263.391,55.2,55.2,0,0,0,7.263-.391,2.3,2.3,0,0,0,1.643-1.628,24.5,24.5,0,0,0,.388-4.5,24.5,24.5,0,0,0-.388-4.5ZM8.443,13.812V8.284L13.3,11.048,8.443,13.812Z" transform="translate(998.695 2844.965)" fill="#fff"></path>
                                                    </g>
                                                </g>
                                            </svg>

                                        </a>
                                        @endif


                                        @if($data->twitter!='')
                                        <a target="_blank" href="{{ $data->twitter }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_805" data-name="Group 805" transform="translate(-566 -793)">
                                                    <rect id="Rectangle_947" data-name="Rectangle 947" width="50" height="50" rx="25" transform="translate(566 793)" fill="#1fa2f2"></rect>
                                                    <g id="Group_764" data-name="Group 764" transform="translate(-418 -2038)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"></rect>
                                                        <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#fff"></path>
                                                    </g>
                                                </g>
                                            </svg>

                                        </a>
                                        @endif


                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- column -->


                        <!-- column -->
                        <div class="col-12 col-md-4">
                            <div class="col-12">
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-12 d-block d-md-none">
                                        <div class="row">

                                            <div class="col-3 d-block d-md-none">
                                                <div class="member-inner-info">
                                                    <div class="mb-3 bor-one"></div>
                                                    <h1 class="member-inner-title">{{ $data->name }}</h1>
                                                </div>
                                            </div>
                                            <div class="col-9 pl-0 pr-0 d-block d-md-none">
                                                <img style="    height: 300px;" class="img-responsive img-grad" src="{{ asset('uploads/political_party/') }}/{{ $data->picture }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 d-block d-md-none mb-2 mt-4 mobile-margin-bottom-none margin-top-31-px">
                                        <div class="col-12 p-0">



                                            <div class="breadcrumb_b pl-38 pr-38 pt-3 pb-2 pr-3 d-flex align-items-center mb-20">
                                                <h1 class="breadcrumb-item_s">{{ $data->position }}</h1>
                                            </div>



                                        </div>
                                    </div>
                                    <!-- column -->
                                    <div class="col-12 pl-0 d-none d-md-block">
                                        <img class="img-fluid float-right" src="{{ asset('uploads/political_party/') }}/{{ $data->picture }}" >

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                       {{-- <div class="col-12 d-block d-md-none mt-4">
                            <div class="col-12 d-flex justify-content-between">
                                <div class="d-flex justify-content-start align-items-center">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $request->fullUrl() }}">
                                        <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                                <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                    <rect width="50" height="50" rx="6" stroke="none"/>
                                                    <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"/>
                                                </g>
                                                <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"/>
                                                    <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                    <a target="_blank" href="http://twitter.com/share?text={{ $data->name }}&url={{ $request->fullUrl() }}">
                                        <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                                <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                    <rect width="50" height="50" rx="6" stroke="none"/>
                                                    <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"/>
                                                </g>
                                                <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"/>
                                                    <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>

                                    <a href="#"><div class="share-on">გააზიარე</div></a>

                                </div>
                                <div class=" d-none justify-content-end align-items-center">
                                    <a href="#">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_808" data-name="Group 808" transform="translate(-356 -793)">
                                                <rect id="Rectangle_893" data-name="Rectangle 893" width="50" height="50" rx="25" transform="translate(356 793)" fill="#0d8cf1"/>
                                                <g id="Group_762" data-name="Group 762" transform="translate(-628 -2038)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                    <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#fff"/>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>--}}


                        <div class="col-12 d-block d-md-none mt-4">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="d-flex justify-content-start align-items-center">
                                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $request->fullUrl() }}">
                                                <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 50 50">
                                                    <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                                        <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                            <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                            <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                        </g>
                                                        <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"></rect>
                                                            <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                            <a target="_blank" href="http://twitter.com/share?text={{ $data->name }}&url={{ $request->fullUrl() }}">
                                                <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 50 50">
                                                    <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                                        <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                            <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                            <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                        </g>
                                                        <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"></rect>
                                                            <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="d-flex justify-content-around align-items-center">
                                            @if($data->facebook!='')
                                            <a target="_blank" href="{{ $data->facebook }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
                                                    <g id="Group_808" data-name="Group 808" transform="translate(-361 -798)">
                                                        <rect id="Rectangle_893" data-name="Rectangle 893" width="40" height="40" rx="20" transform="translate(361 798)" fill="#0d8cf1"></rect>
                                                        <g id="Group_762" data-name="Group 762" transform="translate(-628 -2038)">
                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"></rect>
                                                            <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#fff"></path>
                                                        </g>
                                                    </g>
                                                </svg>

                                            </a>
                                            @endif

                                            @if($data->youtube!='')
                                            <a target="_blank" href="{{ $data->youtube }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
                                                    <g id="Group_806" data-name="Group 806" transform="translate(-501 -798)">
                                                        <rect id="Rectangle_946" data-name="Rectangle 946" width="40" height="40" rx="20" transform="translate(501 798)" fill="red"></rect>
                                                        <g id="Group_763" data-name="Group 763" transform="translate(-488 -2038)">
                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"></rect>
                                                            <path id="Icon_awesome-youtube" data-name="Icon awesome-youtube" d="M19.25,6.545a2.336,2.336,0,0,0-1.643-1.654A55.2,55.2,0,0,0,10.344,4.5a55.2,55.2,0,0,0-7.263.391A2.336,2.336,0,0,0,1.438,6.545a24.5,24.5,0,0,0-.388,4.5,24.5,24.5,0,0,0,.388,4.5,2.3,2.3,0,0,0,1.643,1.628,55.2,55.2,0,0,0,7.263.391,55.2,55.2,0,0,0,7.263-.391,2.3,2.3,0,0,0,1.643-1.628,24.5,24.5,0,0,0,.388-4.5,24.5,24.5,0,0,0-.388-4.5ZM8.443,13.812V8.284L13.3,11.048,8.443,13.812Z" transform="translate(998.695 2844.965)" fill="#fff"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                            @endif

                                            @if($data->twitter!='')
                                            <a target="_blank" href="{{ $data->twitter }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
                                                    <g id="Group_805" data-name="Group 805" transform="translate(-571 -798)">
                                                        <rect id="Rectangle_947" data-name="Rectangle 947" width="40" height="40" rx="20" transform="translate(571 798)" fill="#1fa2f2"></rect>
                                                        <g id="Group_764" data-name="Group 764" transform="translate(-418 -2038)">
                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"></rect>
                                                            <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#fff"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                        <div class="col-12 mt-5 mb-5 pl-0">
                            <div class="col-12">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 mt-4 mb-4" style="border: 1px solid #EBEBEB"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->

                        <!-- column -->
                        <div class="col-12 col-md-7">
                            <div class="col-12">
                                <div class="col-12 pl-0 pl-md-3 pr-0 pr-md-3">
                                    <div class="row">
                                        <div class="col-12 description" style="word-wrap: break-word;">
                                            {!! $data->desc !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop