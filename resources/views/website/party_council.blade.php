@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div class="structure mt-3">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-md-12 pt-5 mb-3  mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white pl-0">
                                        <li class="breadcrumb-item_other active" aria-current="page"> @lang('site.party') <svg class="ml-3 mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg> </li>
                                        <li class="breadcrumb-item_other active" aria-current="page"> @lang('site.political-council')</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 margin-top-50-px">
                    <div class="row">


                    @foreach($party_structure as $row)

                        <!-- column -->
                            <div class="col-12 col-lg-6 p-0">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body pt-0 pr-0 pl-0 pb-0">
                                            <div class="row">
                                                <div class="col-7">
                                                    <div class="d-block d-md-none structure-detail mb-2"></div>
                                                    <a href="{{ url('show-bio') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">
                                                    <h5 class="card-title structure-title">
                                                        {{$row->name}}
                                                    </h5>
                                                    </a>
                                                    <p class="card-text political-desc">{{ $row->position }}</p>
                                                    <div class="d-none d-md-block down">
                                                        <a class="d-flex align-items-center" href="{{ url('show-bio') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">
                                                            <div class="biography">@lang('site.biography')</div>
                                                            <img class="downarrow" src="{{ url('website') }}/images/icondownarrow.png"></a>
                                                    </div>
                                                </div>
                                                <div class="col-5">
                                                    <a href="{{ url('show-bio') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">

                                                    <img  src="{{ asset('uploads/political_party') }}/{{ $row->picture }}" alt="sans" class="img-fluid">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 margin-top-30-px margin-bottom-30-px p-0 d-block d-md-none">
                                        <p class="b-t m-0"></p>
                                    </div>
                                    <div class="col-12 pt-40 pl-0 pr-0 mb-60 d-none d-md-block">
                                        <p class="b-t m-0"></p>
                                    </div>
                                </div>
                            </div>
                            <!-- column -->
                    @endforeach






                    </div>
                </div>

            </div>
        </div>
    </div>



@stop