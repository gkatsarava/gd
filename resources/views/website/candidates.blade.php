@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')

    <div class="majorities-tbilisi mt-3">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-md-12 pt-5 mb-3 d-none d-md-block">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                            @lang('site.elections2020')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">


                        <!-- column -->

                     {{--   <!-- column -->
                        <div class="col-12 col-md-4 mb-100-px d-none d-md-block pr-0">
                            <div class="row">
                                <div class="col-12 d-flex">
                                    <a href="{{ url('/mayor') }}">
                                        <div class="mr-3 breadcrumb-title  pb-15-px">@lang('site.mayor')</div>
                                    </a>
                                    <a href="{{ url('/candidates') }}">
                                        <div class="mr-3 breadcrumb-title active_tab pb-15-px">@lang('site.majoritarians')</div>
                                    </a>
                                    <a href="{{ url('/proportional') }}">
                                        <div class="breadcrumb-title  pb-15-px">@lang('site.proportional-list')</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- column -->

                        <!-- column -->
                        <div class="col-12 d-block d-md-none mb-30">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-3 col-md-12">
                                            <a href="{{ url('/mayor') }}">
                                                <div class="breadcrumb-title text-center ">@lang('site.mayor')</div>
                                            </a>
                                        </div>
                                        <div class="col-4 col-md-12">
                                            <a href="{{ url('/candidates') }}">
                                                <div class="breadcrumb-title text-center active_tab">@lang('site.majoritarians')</div>
                                            </a>
                                        </div>
                                        <div class="col-4 col-md-12 pr-0">
                                            <a href="{{ url('/proportional') }}">
                                                <div class="breadcrumb-title text-center">@lang('site.proportional-list')</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->--}}

                        <div class="col-12 col-md-4 mb-100-px d-none d-md-block pr-0"></div>
                        <div class="col-12 d-block d-md-none mb-30"></div>

                        <!-- column -->
                        <div class="d-none d-md-block col-12 col-md-8 mb-5">
                            <div class="row">

                                {{--desktop--}}
                                <div class="d-none d-md-block w-100">
                                    <div class="row">
                                        <div class="col-4 float-left">
                                            <a class="" href="{{ url('/candidates') }}" style="width: 146px;float: right;">
                                                @if(!isset($request->region))
                                                    <h1 class="mr-3 btn-major btn-major-active">@lang('site.Tbilisi')</h1>
                                                @else
                                                    <h1 class="mr-3 btn-major">@lang('site.Tbilisi')</h1>
                                                @endif
                                            </a>
                                        </div>
                                        <div class="col-4" style="float:left;width:280px;">
                                            <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                                <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                                    <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                        <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    </g>
                                                </svg>

                                                <select class="selectpicker"  style="width:280px;"  onchange="this.form.submit()" name="region">

                                                    <option value=""> @lang('site.region')</option>
                                                    @foreach($regions as $row)
                                                        <option value="{{ $row->id }}" {{ isset($request->region) && $request->region!='' && $request->region==$row->id ? "selected" : "" }}> {{ $row->name }} </option>
                                                    @endforeach
                                                </select>
                                            </form>
                                        </div>
                                        <div class="col-4" style="float:left;width:280px;">
                                            <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                                <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                                    <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                        <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    </g>
                                                </svg>
                                                <input type="text" name="region" value="{{ $request->region }}" hidden>

                                                <select class="selectpicker"  style="width:280px;"  onchange="this.form.submit()" name="city">

                                                    <option value="">  @lang('site.raioni') </option>
                                                    @if(isset($request->region))

                                                    @foreach($city as $r)
                                                        <option value="{{ $r->id }}" {{ isset($request->city) && $request->city!='' && $request->city==$r->id ? "selected" : "" }}> {{ $r->name }} </option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </form>
                                        </div>
                                    </div>




                                </div>




                            </div>
                        </div>

                        <div class="col-12 mb-5 d-block d-md-none">

                            <div class="row">
                                <div class="col-12 pr-0 mb-3 pr-5">
                                    <a class="" href="{{ url('/candidates') }}" style="/*width: 146px;float: right;*/">
                                        @if(!isset($request->region))
                                            <div class="btn-majoritarian btn-major-active">
                                                @lang('site.Tbilisi')
                                            </div>
                                        @else
                                            <div class="btn-major" style="      padding:unset;   display: block;line-height: 62px;text-align: center;">@lang('site.Tbilisi')</div>
                                        @endif
                                    </a>
                                </div>
                                <div class="col-12 mb-3">
                                    <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                        <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                            <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </svg>

                                        <select class="selectpicker"  style="width:280px;"  onchange="this.form.submit()" name="region">

                                           <option value="">  @lang('site.region') </option>
                                            @foreach($regions as $row)
                                                <option value="{{ $row->id }}" {{ isset($request->region) && $request->region!='' && $request->region==$row->id ? "selected" : "" }}> {{ $row->name }} </option>
                                            @endforeach
                                        </select>
                                    </form>
                                </div>
                                <div class="col-12 ">
                                    <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                        <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                            <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </svg>
                                        <input type="text" name="region" value="{{ $request->region }}" hidden>

                                        <select class="selectpicker"  style="width:280px;"  onchange="this.form.submit()" name="city">

                                            <option value="">@lang('site.raioni')</option>
                                            @if(isset($request->region))

                                                @foreach($city as $r)
                                                    <option value="{{ $r->id }}" {{ isset($request->city) && $request->city!='' && $request->city==$r->id ? "selected" : "" }}> {{ $r->name }} </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </form>

                                </div>
                            </div>

                        </div>

                        @foreach($majoritarians as $row)


                        <!-- column -->
                        <div class="col-12 col-md-6 change_cl">
                            <div class="row">
                                <div class="col-7 col-md-7">
                                    <h1 class="mb-3 majorities-title text-wrap">{{ $row->name }}</h1>
                                    <p class="political-desc">{{ $row->location }}</p>
                                    <div class="">
                                        @if($row->majoritarian_percent!='')
                                        <div class="d-flex align-items-center down">
                                            <div class="mr-3 cesko-btn">{{ $row->majoritarian_percent }}%</div>
                                            <a href="{{ $row->cesco_link }}" target="_blank">
                                            <img class="img1" src="{{ url('website') }}/images/ceskoimg.png">
                                            </a>
                                        </div>
                                        @else
                                        <div class="d-flex align-items-center down">
                                            <a href="{{ url('show-candidates') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">
                                                <div class="biography">@lang('site.biography')</div>
                                            </a>
                                            <img class="downarrow" src="{{ url('website') }}/images/icondownarrow.png">
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-5 col-md-5">
                                    <a href="{{ url('show-candidates') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">

                                    <img class="img-fluid" src="{{ asset('uploads/majoritarians/') }}/{{ $row->picture }}" >
                                    </a>


                                </div>
                            </div>
                            <div class="col-12 pt-4 pb-4 p-0 d-block d-md-none">
                                <p class="b-t m-0"></p>
                            </div>
                            <div class="col-12 pt-40 pl-0 pr-0 mb-60 d-none d-md-block">
                                <p class="b-t m-0"></p>
                            </div>
                        </div>
                        <!-- column -->
                        @endforeach



                        <!-- pagination -->
                        <div class="col-12 pt-5 gd-pagination d-none d-md-block">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-around">
                                    <nav class="" aria-label="Page navigation example">
                                        {{  $majoritarians->appends(request()->input())->links("pagination::bootstrap-4") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- pagination -->

                        <!-- pagination -->
                        <div class="col-12 d-block d-md-none">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-around">
                                    {{  $majoritarians->appends(request()->input())->links("pagination::bootstrap-4") }}
                                </div>
                            </div>
                        </div>
                        <!-- pagination -->

                    </div>

                </div>
            </div>

        </div>
    </div>
    </div>

@stop