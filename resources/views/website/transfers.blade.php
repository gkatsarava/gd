@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')

    <div class="transfers">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-12">
                    <div class="row">


                        <!-- column -->
                        <div class="col-12 mb-3">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12 pl-0">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb bg-white pl-0">
                                                <li class="breadcrumb-item_other active" aria-current="page">შემოწირულობები</li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->


                        @foreach($transfers as $row)
                        <!-- column -->
                        <div class="col-12 col-md-6 mb-5">
                            <div class="row">
                                <div class="col-5 col-md-5 transfer-poster">
                                    <div class="d-flex align-items-center transfer-poster-background" style="background-image: url('./images/transfer-poster-background.png');">
                                        <a class="d-none d-md-block" href="{{ url('uploads/transfers') }}/{{ $row->file }}" target="_blank">
                                            @if($row->icon!='')
                                                <img src="{{ url('uploads/transfers_icon') }}/{{$row->icon}}" class="img-fluid">
                                            @else
                                                <svg class="transfer-pdf-icon" xmlns="http://www.w3.org/2000/svg" width="100%" height="111.766" viewBox="0 0 97.795 111.766">
                                                    <g id="pdf" transform="translate(-32)">
                                                        <path id="Path_239" data-name="Path 239" d="M102.985,0A7.006,7.006,0,0,0,96,6.985V104.78a7.006,7.006,0,0,0,6.985,6.985h69.854a7.006,7.006,0,0,0,6.985-6.985V27.941L151.883,0Z" transform="translate(-50.029)" fill="#e2e5e7"/>
                                                        <path id="Path_240" data-name="Path 240" d="M358.985,27.941h20.956L352,0V20.956A7.006,7.006,0,0,0,358.985,27.941Z" transform="translate(-250.146)" fill="#b0b7bd"/>
                                                        <path id="Path_241" data-name="Path 241" d="M404.956,148.956,384,128h20.956Z" transform="translate(-275.161 -100.059)" fill="#cad1d8"/>
                                                        <path id="Path_242" data-name="Path 242" d="M115.824,278.419a3.5,3.5,0,0,1-3.493,3.493H35.493A3.5,3.5,0,0,1,32,278.419V243.493A3.5,3.5,0,0,1,35.493,240h76.839a3.5,3.5,0,0,1,3.493,3.493Z" transform="translate(0 -187.61)" fill="#f15642"/>
                                                        <g id="Group_1163" data-name="Group 1163" transform="translate(47.225 64.248)">
                                                            <path id="Path_243" data-name="Path 243" d="M101.744,296.248a1.922,1.922,0,0,1,1.9-1.928h6.451a6.982,6.982,0,0,1,0,13.964h-4.663v3.688a1.753,1.753,0,0,1-1.788,1.924,1.882,1.882,0,0,1-1.9-1.924Zm3.685,1.589v6.957h4.663a3.481,3.481,0,0,0,0-6.957Z" transform="translate(-101.744 -294.32)" fill="#fff"/>
                                                            <path id="Path_244" data-name="Path 244" d="M189.752,314.8a1.748,1.748,0,0,1-1.928-1.729V297.2a1.9,1.9,0,0,1,1.928-1.732h6.4c12.762,0,12.483,19.325.251,19.325Zm1.76-15.916v12.511h4.635c7.541,0,7.876-12.511,0-12.511h-4.635Z" transform="translate(-169.033 -295.221)" fill="#fff"/>
                                                            <path id="Path_245" data-name="Path 245" d="M290.789,299.092v4.439h7.122a2.16,2.16,0,0,1,2.012,1.98,1.941,1.941,0,0,1-2.012,1.676h-7.122v5.864a1.654,1.654,0,0,1-1.673,1.729,1.774,1.774,0,0,1-1.98-1.729V297.188a1.765,1.765,0,0,1,1.98-1.732h9.8a1.742,1.742,0,0,1,1.956,1.732,1.93,1.93,0,0,1-1.956,1.9h-8.131Z" transform="translate(-246.666 -295.208)" fill="#fff"/>
                                                        </g>
                                                        <path id="Path_246" data-name="Path 246" d="M162.361,419.493H96v3.493h66.361a3.5,3.5,0,0,0,3.493-3.493V416A3.5,3.5,0,0,1,162.361,419.493Z" transform="translate(-50.029 -325.19)" fill="#cad1d8"/>
                                                    </g>
                                                </svg>
                                                @endif

                                           </a>
                                        <a class="d-block d-md-none"  href="{{ url('uploads/transfers') }}/{{ $row->file }}" target="_blank">
                                            @if($row->icon!='')
                                                <img src="{{ url('uploads/transfers_icon') }}/{{$row->icon}}" class="img-fluid">
                                            @else
                                                <svg class="transfer-pdf-icon" xmlns="http://www.w3.org/2000/svg" width="100%" height="111.766" viewBox="0 0 97.795 111.766">
                                                    <g id="pdf" transform="translate(-32)">
                                                        <path id="Path_239" data-name="Path 239" d="M102.985,0A7.006,7.006,0,0,0,96,6.985V104.78a7.006,7.006,0,0,0,6.985,6.985h69.854a7.006,7.006,0,0,0,6.985-6.985V27.941L151.883,0Z" transform="translate(-50.029)" fill="#e2e5e7"/>
                                                        <path id="Path_240" data-name="Path 240" d="M358.985,27.941h20.956L352,0V20.956A7.006,7.006,0,0,0,358.985,27.941Z" transform="translate(-250.146)" fill="#b0b7bd"/>
                                                        <path id="Path_241" data-name="Path 241" d="M404.956,148.956,384,128h20.956Z" transform="translate(-275.161 -100.059)" fill="#cad1d8"/>
                                                        <path id="Path_242" data-name="Path 242" d="M115.824,278.419a3.5,3.5,0,0,1-3.493,3.493H35.493A3.5,3.5,0,0,1,32,278.419V243.493A3.5,3.5,0,0,1,35.493,240h76.839a3.5,3.5,0,0,1,3.493,3.493Z" transform="translate(0 -187.61)" fill="#f15642"/>
                                                        <g id="Group_1163" data-name="Group 1163" transform="translate(47.225 64.248)">
                                                            <path id="Path_243" data-name="Path 243" d="M101.744,296.248a1.922,1.922,0,0,1,1.9-1.928h6.451a6.982,6.982,0,0,1,0,13.964h-4.663v3.688a1.753,1.753,0,0,1-1.788,1.924,1.882,1.882,0,0,1-1.9-1.924Zm3.685,1.589v6.957h4.663a3.481,3.481,0,0,0,0-6.957Z" transform="translate(-101.744 -294.32)" fill="#fff"/>
                                                            <path id="Path_244" data-name="Path 244" d="M189.752,314.8a1.748,1.748,0,0,1-1.928-1.729V297.2a1.9,1.9,0,0,1,1.928-1.732h6.4c12.762,0,12.483,19.325.251,19.325Zm1.76-15.916v12.511h4.635c7.541,0,7.876-12.511,0-12.511h-4.635Z" transform="translate(-169.033 -295.221)" fill="#fff"/>
                                                            <path id="Path_245" data-name="Path 245" d="M290.789,299.092v4.439h7.122a2.16,2.16,0,0,1,2.012,1.98,1.941,1.941,0,0,1-2.012,1.676h-7.122v5.864a1.654,1.654,0,0,1-1.673,1.729,1.774,1.774,0,0,1-1.98-1.729V297.188a1.765,1.765,0,0,1,1.98-1.732h9.8a1.742,1.742,0,0,1,1.956,1.732,1.93,1.93,0,0,1-1.956,1.9h-8.131Z" transform="translate(-246.666 -295.208)" fill="#fff"/>
                                                        </g>
                                                        <path id="Path_246" data-name="Path 246" d="M162.361,419.493H96v3.493h66.361a3.5,3.5,0,0,0,3.493-3.493V416A3.5,3.5,0,0,1,162.361,419.493Z" transform="translate(-50.029 -325.19)" fill="#cad1d8"/>
                                                    </g>
                                                </svg>
                                            @endif
                                        </a>
                                    </div>

                                </div>
                                <div class="col-7 col-md-7">
                                    <p class="transfer-date">
                                        {{ $row->created_at->format('d') }}
                                        {{ $month[$row->created_at->format('m')] }},
                                        {{ $row->created_at->format('Y') }}
                                    </p>
                                    <h1 class="mb-3 transfer-title text-wrap">
                                        {{ $row->title }}
                                    </h1>

                                </div>

                            </div>

                        </div>
                        <!-- column -->
                        @endforeach





                    </div>
                </div>
            </div>

        </div>
    </div>

@stop