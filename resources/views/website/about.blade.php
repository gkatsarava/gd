@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div class="about-party mt-3">
        <div class="container-custom mb-5">
            <div class="row">

                <div class="col-md-12 pt-5 mb-3 mobile-padding-top-none margin-top-31-px margin-bottom-28-px">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white pl-0">
                                        <li class="breadcrumb-item_other active" aria-current="page"> @lang('site.party') <svg class="ml-3 mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg> </li>
                                        <li class="breadcrumb-item_other active" aria-current="page"> @lang('site.about-party') </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 ">
                    <div class="row ">

                        <!-- column -->
                        <div class="col-12 col-lg-6 mb-5 mobile-margin-bottom-none">
                            <div class="row">
                                <div class="col-12">
                                    <div class="structure-detail-about"></div>
                                    <h1 class="about-title">@lang('site.about-party')</h1>
                                    <div class="about-desc">
                                        {!! $about->desc !!}
                                    </div>
                                    <div class="col-12 margin-top-30-px about-regulation d-flex align-items-center mt-5">
                                        <div class="col-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="38" height="47" viewBox="0 0 38 47">
                                                <g id="Icon_feather-file-text" data-name="Icon feather-file-text" transform="translate(-5 -2)">
                                                    <path id="Path_219" data-name="Path 219" d="M31.5,3h-21A4.5,4.5,0,0,0,6,7.5v36A4.5,4.5,0,0,0,10.5,48h27A4.5,4.5,0,0,0,42,43.5v-30Z" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    <path id="Path_220" data-name="Path 220" d="M21,3V13.518H31.518" transform="translate(10.482)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    <path id="Path_221" data-name="Path 221" d="M30,19.5H12" transform="translate(3 4.25)" fill="none" stroke="#fdb714" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    <path id="Path_222" data-name="Path 222" d="M30,25.5H12" transform="translate(3 5.25)" fill="none" stroke="#fdb714" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    <path id="Path_224" data-name="Path 224" d="M30,25.5H12" transform="translate(3 12.25)" fill="none" stroke="#fdb714" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                    <path id="Path_223" data-name="Path 223" d="M16.5,13.5H12" transform="translate(3 3.25)" fill="none" stroke="#fdb714" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                </g>
                                            </svg>

                                        </div>
                                        <div class="col-10 d-flex justify-content-between align-items-center pl-0">
                                            <div>
                                                <p class="more">@lang('site.learn-more')</p>
                                                <a href="{{ url('uploads/about') }}/{{ $about->file }}" target="_blank"><p>@lang('site.party-charter')</p></a>
                                            </div>
                                            <a href="{{ url('uploads/about') }}/{{ $about->file }}" target="_blank">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="12.914" height="23.828" viewBox="0 0 12.914 23.828">
                                                <g id="Icon_feather-arrow-right" data-name="Icon feather-arrow-right" transform="translate(-16.586 -6.086)">
                                                    <path id="Path_218" data-name="Path 218" d="M18,7.5,28.5,18,18,28.5" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                </g>
                                            </svg>
                                            </a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->

                        <!-- column -->
                        <div class="col-12 col-lg-6 margin-top-50-px">
                            <div class="row">
                                <div class="col-12">
                                        <div class="structure-detail-about"></div>
                                        <h1 class="about-title">@lang('site.our-goal'):</h1>
                                       <div class="about-desc">
                                           {!! $about->goal !!}
                                       </div>

                                </div>
                            </div>
                        </div>
                        <!-- column -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop