@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')
    <div class="search-result">
        <div class="container-custom mb-5">
            <div class="row">


                <div class="col-12  pt-5 mb-3">
                    <div class="row">


                    <!-- column -->
                    <div class="col-12 mb-3">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 pl-0 mb-4">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb bg-white pl-0">
                                            <li class="breadcrumb-item_other active" aria-current="page">ძიების შედეგი</li>
                                        </ol>
                                    </nav>
                                </div>
                                <div class="col-12 pl-0">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb bg-white pl-0">
                                            <li class="breadcrumb-item_other active" aria-current="page">მოიძებნა <span class="search-result-number">{{ $cnt }} შედეგი</span> სიტყვისთვის <span class="searched-description">{{ $request->search }}</span></li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->



                    @foreach($news as $row)

                    <!-- column -->
                    <div class="col-12 col-md-12 col-xl-6">
                        <div class="row">
                            <div class="col-5 col-sm-2 col-md-3 col-xl-3 mb-4">

                                    <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">

                                        @if(isset($row->Pictures->where('first_picture','1')->first()->picture))
                                            <img  class="img-fluid" src="{{ asset('uploads/news') }}/{{ $row->Pictures->where('first_picture','1')->first()->picture }}">
                                        @else
                                            <img  class="img-fluid" src="{{ asset('uploads/news') }}/{{ $row->Pictures->first()->picture }}">
                                        @endif
                                    </a>
                            </div>
                            <div class="col-7 col-sm-10 col-md-9 col-xl-9">
                                <p class="date-box-title d-none d-md-block mb-1">
                                    {{ $row->created_at->format('d') }}
                                    {{ $month[$row->created_at->format('m')] }},
                                    {{ $row->created_at->format('Y') }}
                                </p>


                                <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">

                                    <h1 class="search-title">
                                        {!! strip_tags($row->desc) !!}
                                    </h1>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- column -->

                    @endforeach


                        <div class="col-12 mt-5 pt-5 gd-pagination  b-t">
                            <div class="row">
                                <div class="col-12">
                                    <nav class="" aria-label="Page navigation example">
                                        <ul class="pagination d-flex align-items-center justify-content-center">
                                            @if ($news->onFirstPage())
                                                <li class="page-item mr-4">
                                                    <a class="page-link previous" href="#">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(7.414 12.829) rotate(-135)">
                                                                <path id="Path_211" data-name="Path 211" d="M0,7.657,7.657,0" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                                <path id="Path_212" data-name="Path 212" d="M7.657,7.657V0H0" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </li>
                                            @else
                                                <li class="page-item mr-4">
                                                    <a class="page-link previous" href="{{ $news->previousPageUrl() }}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(7.414 12.829) rotate(-135)">
                                                                <path id="Path_211" data-name="Path 211" d="M0,7.657,7.657,0" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                                <path  id="Path_212" data-name="Path 212" d="M7.657,7.657V0H0" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </li>

                                            @endif



                                            <?php
                                            $start = $news->currentPage() - 1; // show 3 pagination links before current
                                            $end = $news->currentPage() + 3; // show 3 pagination links after current
                                            if($start < 1) {
                                                $start = 1; // reset start to 1
                                                $end += 1;
                                            }
                                            if($end >= $news->lastPage() ) $end = $news->lastPage(); // reset end to last page
                                            ?>


                                            @for ($i = $start; $i <= $end; $i++)

                                                @if($i==$request->page)

                                                    <li class="page-item mr-2">
                                                        <a class="page-link number page-active" href="{{ $news->appends(request()->input())->url($i) }}">{{ $i }}</a>
                                                    </li>
                                                @else

                                                    <li class="page-item mr-2">
                                                        <a class="page-link number" href="{{ $news->appends(request()->input())->url($i) }}">{{ $i }}</a>
                                                    </li>
                                                @endif



                                            @endfor





                                            @if ($news->hasMorePages())
                                                <li class="page-item">
                                                    <a class="page-link next" href="{{ $news->nextPageUrl() }}#">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(2 7.415) rotate(-45)">
                                                                <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                                <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </li>
                                            @else
                                                <li class="page-item">
                                                    <a class="page-link next" href="#">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(2 7.415) rotate(-45)">
                                                                <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                                <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </li>

                                            @endif

                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@stop