@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')





    <div class="news mt-3">
        <div class="container-custom" >



            <div class="row">
                <div class="col-md-12 pt-5 mb-3 mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                           {{-- <svg class="mr-3 mt-2" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                                <path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                                            --}}

                                            @lang('site.world-media')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">



            @foreach($news as $row)
                <!-- news -->
                    <div class="col-12 mb-5 p-0 mobile-margin-bottom-none margin-bottom-30-px">
                        <div class="col-12">
                            <div class="col-12 p-0">
                                <div class="row">
                                    <div class="col-5 col-md-7 col-lg-7 col-xl-4 pr-0 pr-md-5">
                                        <a href="{{ url('show-world-media') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">
                                            <img  class="img-fluid" src="{{ asset('uploads/media') }}/{{ $row->Pictures->where('first_picture','1')->first()->picture }}">

                                        </a>
                                    </div>
                                    <div class="col-7 col-md-5 col-lg-5 col-xl-8">
                                        <p class="mb-3 date-box-title">
                                            {{ $row->created_at->format('d') }}
                                            {{ $month[$row->created_at->format('m')] }},
                                            {{ $row->created_at->format('Y') }}
                                        </p>
                                        <a href="{{ url('show-world-media') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">
                                            <h1 class="mb-3 news-title">
                                                {{ $row->title }}
                                            </h1>
                                        </a>
                                        <div class=" d-none d-md-block">
                                        <a href="{{ url('show-world-media') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">

                                            <div class="news-description" style="    -webkit-line-clamp: 2;">
                                                {!! strip_tags($row->desc) !!}
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- news -->
            @endforeach

            <div class="col-12">
                <div class="">
                    <div class="col-12 margin-top-30-px margin-bottom-30-px p-0 d-block d-md-none">
                        <p class="b-t m-0"></p>
                    </div>
                </div>
            </div>

            <!-- pagination -->
            <div class="col-12 pt-5 gd-pagination d-none d-md-block">
                <div class="row">
                    <div class="col-12">
                        {{  $news->appends(request()->input())->links("pagination::bootstrap-4") }}

                    </div>
                </div>
            </div>
            <!-- pagination -->


        </div>

    </div>
    </div>


@stop