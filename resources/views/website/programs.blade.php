@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div class="about-party mt-3">
        <div class="container-custom mb-5">
            <div class="row">

                <div class="col-md-12 pt-5 mb-3 mobile-padding-top-none margin-top-31-px margin-bottom-28-px">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white pl-0">
                                        <li class="breadcrumb-item_other active" aria-current="page">
                                            @lang('site.programe')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 ">
                    @if($programs->file!='')
                    <div class="row ">

                        <div class="col-md-12">
                        <div id="pdf" style="width:100%; height:800px;" class=" pdfobject-container">

                            <embed class="pdfobject" src="{{ asset('uploads/programs') }}/{{ $programs->file }}" type="application/pdf" style="overflow: auto; width: 100%; height: 100%;">
                        </div>
                        <script>
                            var options = {
                                page: 1,
                                pdfOpenParams: {
                                    navpanes: 1,
                                    toolbar: 1,
                                    statusbar: 0,
                                    view: "FitH",
                                    pagemode: "thumbs"
                                }
                            };




                            PDFObject.embed("{{ asset('uploads/programs') }}/{{ $programs->file }}", "#pdf", options);
                        </script>
                        </div>


                      {{--  <!-- column -->
                        <div class="col-12 col-lg-12 mb-5 mobile-margin-bottom-none">
                            <div class="row">
                                <div class="col-12">
                                    <div class="structure-detail-about"></div>
                                    <h1 class="about-title">@lang('site.programs')</h1>

                                    <div class="col-12 col-md-12 margin-top-30-px about-regulation d-flex align-items-center mt-5">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->--}}


                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop