@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')

    @foreach($gallery as $row)
    <div id="video_{{ $row->id }}" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closevideogallery({{ $row->id }})">&times;</a>
        <div class="overlay-content" style="">

            <div class="container">
                <div class="row">
                    <div class="col-md-12 d-none d-md-block">
                        <?php

                        $url = $row->youtube;
                        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                        $id = $matches[1];
                        $width = '60%';
                        $height = '450px'; ?>

                        <div class="player_{{ $row->id }}">
                        <iframe id="ytplayer" type="text/html" width="<?php echo $width ?>" height="<?php echo $height ?>"
                                src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3"
                                frameborder="0" allowfullscreen></iframe>
                        </div>

                    </div>

                    <div class="col-md-12 d-block d-md-none">
                        <?php

                        $url = $row->youtube;
                        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
                        $id = $matches[1];
                        $width = '100%';
                        $height = '300px'; ?>

                        <div class="player_{{ $row->id }}">
                            <iframe id="ytplayer" type="text/html" width="<?php echo $width ?>" height="<?php echo $height ?>"
                                    src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3"
                                    frameborder="0" allowfullscreen></iframe>
                        </div>

                    </div>

                    <div style="width: 58%;" class="d-none d-md-block text-left font-family-regular mx-auto mt-2">
                        <div class="mb-4 bor-one"></div>
                        {{ $row->title }}
                    </div>
                    <div style="width: 88%;" class="d-block d-md-none text-left font-family-regular mx-auto mt-2">
                        <div class="mb-4 bor-one"></div>
                        {{ $row->title }}
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endforeach



    <div class="video-gallery mt-3">
        <div class="container-custom">
            <div class="row">
                <div class="col-md-12 pt-5 mb-3 mobile-margin-bottom-none">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                            @lang('site.video-gallery')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mobile-padding-top-none pt-5 mb-3">

                    <div class="row">

                        <!-- column -->


                        @foreach($gallery as $row)


                        <?php

                        $video_id = explode("?v=", $row->youtube);
                        $video_id = $video_id[1];
                        $thumbnail="http://img.youtube.com/vi/".$video_id."/maxresdefault.jpg";
                        ?>
                        <!-- column -->
                        <div class="col-12 col-md-4 mb-4 d-none d-md-block">
                            <div class="col-12 p-0">
                                <div class="col-12 p-0">
                                    <a href="#">
                                        <div class="img-fluid mb-3 play video_gallery_play_position" onclick="openvideogallery({{ $row->id }})">
                                            <img class="img-fluid" src="{{ $thumbnail }}">
                                            <div class="video_gallery_overlay">

                                                <svg class="video_gallery_overlay_svg" xmlns="http://www.w3.org/2000/svg" width="33" height="38" viewBox="0 0 33 38">
                                                    <path id="Polygon_1" data-name="Polygon 1" d="M17.267,3.01a2,2,0,0,1,3.466,0L36.274,30a2,2,0,0,1-1.733,3H3.459a2,2,0,0,1-1.733-3Z" transform="translate(33) rotate(90)" fill="#fff"/>
                                                </svg>

                                            </div>
                                        </div>

                                    </a>
                                </div>

                                <h1 class="video-title">{{ $row->title }}</h1>
                            </div>
                        </div>

                            <div class="col-12 col-md-4 mb-4 d-block d-md-none">
                                <div class="d-flex">
                                <div class="col-5 p-0">
                                    <div class="col-12 p-0">
                                        <a href="#">
                                            <div class="img-fluid mb-3 play video_gallery_play_position" onclick="openvideogallery({{ $row->id }})">
                                                <img class="img-fluid" src="{{ $thumbnail }}">
                                                <div class="video_gallery_overlay">

                                                    <svg class="video_gallery_overlay_svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 33 38">
                                                        <path id="Polygon_1" data-name="Polygon 1" d="M17.267,3.01a2,2,0,0,1,3.466,0L36.274,30a2,2,0,0,1-1.733,3H3.459a2,2,0,0,1-1.733-3Z" transform="translate(33) rotate(90)" fill="#fff"/>
                                                    </svg>

                                                </div>
                                            </div>

                                        </a>
                                    </div>
                                </div>
                                <div class="col-7 p-0 ml-3">
                                    <h1 class="video-title">{{ $row->title }}</h1>
                                </div>
                                </div>
                            </div>
                        @endforeach
                        <!-- column -->


                        <!-- pagination -->
                        <div class="col-12 pt-5 d-block d-md-none b-t">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-around">
                                    {{  $gallery->appends(request()->input())->links("pagination::bootstrap-4") }}

                                </div>
                            </div>
                        </div>
                        <!-- pagination -->

                    </div>

                </div>
            </div>
        </div>
    </div>



@stop