@inject('request', 'Illuminate\Http\Request')
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="facebook-domain-verification" content="mf9qwa03vlsh619wenlfhlrfys6d8j" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{ asset('website') }}/assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.css">


    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('website') }}/assets/css/theme.min.css">
    <link rel="stylesheet" href="{{ asset('website') }}/assets/css/style.css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('website/bootstrap-select') }}/bootstrap-select.min.css">
    <!-- JAVASCRIPT
    ================================================== -->

    <!-- Libs JS -->
    <script src="{{ asset('website') }}/assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="{{asset('website')}}/assets/js/pdfobject.min.js"></script>

    <link href="{{ asset('website/fav.png') }}" rel="shortcut icon" type="image/x-icon"  />
    <link href="{{ asset('website/fav.png') }}" rel="icon" type="image/x-icon"  />




    @if(Request::segment(1)=='show-bulletin')
            <title>{{ $news->title }}</title>
    @elseif(Request::segment(1)=='show-news')
            <title>{{ $news->title }}</title>
    @else
    <title>„ქართული ოცნება - დემოკრატიული საქართველოს“ ვებგვერდი</title>
    @endif

    @if( Request::segment(1) && Request::segment(1)=='about-party' )
        <meta property="og:title" content="ქართული ოცნება - @lang('site.about-party')">
        <meta property="og:description" content="{!! strip_tags($about->desc) !!}">
        <meta property="og:url" content="{{ url('/about-party') }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ url('gd.png') }}">
        <meta name="description" content="ქართული ოცნება - @lang('site.about-party')">
        <meta name="title" content="{!! strip_tags($about->desc) !!}">
    @endif

    @if( Request::segment(1) && Request::segment(1)=='news' )
        <meta property="og:title" content="ქართული ოცნება - @lang('site.news')">
        <meta property="og:description" content="@lang('site.news')">
        <meta property="og:url" content="{{ url('/news') }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ url('gd.png') }}">
        <meta name="description" content="ქართული ოცნება - @lang('site.news')">
        <meta name="title" content="@lang('site.news')">
    @endif

    @if( Request::segment(1) && Request::segment(1)=='party-structure' )
        <meta property="og:title" content="ქართული ოცნება - @lang('site.structure')">
        <meta property="og:description" content="@lang('site.structure')">
        <meta property="og:url" content="{{ url('/party-structure') }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ url('gd.png') }}">
        <meta name="description" content="ქართული ოცნება - @lang('site.structure')">
        <meta name="title" content="@lang('site.structure')">
    @endif
    @if( Request::segment(1) && Request::segment(1)=='show-structure' )
        <meta property="og:title" content="ქართული ოცნება - {{ $data->name }}">
        <meta property="og:description" content="{{ strip_tags($data->desc) }}">
        <meta property="og:url" content="{{ url('show-bio') }}/{{ $data->id }}/{{ str_replace(' ', '-', $data->name) }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ asset('uploads/political_party') }}/{{ $data->picture }}">
        <meta name="description" content="ქართული ოცნება - {{ $data->name }}">
        <meta name="title" content="{{ strip_tags($data->desc) }}">
    @endif

    @if( Request::segment(1) && Request::segment(1)=='show-bio' )
        <meta property="og:title" content="ქართული ოცნება - {{ $data->name }}">
        <meta property="og:description" content="{{ strip_tags($data->desc) }}">
        <meta property="og:url" content="{{ url('show-bio') }}/{{ $data->id }}/{{ str_replace(' ', '-', $data->name) }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ asset('uploads/political_party') }}/{{ $data->picture }}">
        <meta name="description" content="ქართული ოცნება - {{ $data->name }}">
        <meta name="title" content="{{ strip_tags($data->desc) }}">
    @endif


    @if( Request::segment(1) && Request::segment(1)=='show-news' )

        <meta property="og:title" content="ქართული ოცნება - {{ $news->title }}">
        <meta property="og:description" content="{{ strip_tags($news->desc) }}">
        <meta property="og:url" content="{{ url('show-news') }}/{{ $news->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $news->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ asset('uploads/news') }}/{{ $news->Pictures->first()->picture }}">
        <meta name="description" content="ქართული ოცნება - {{ $news->title }}">
        <meta name="title" content="{{ strip_tags($news->desc) }}">
    @endif




    @if( Request::segment(1) && Request::segment(1)=='party-council' )
        <meta property="og:title" content="ქართული ოცნება - @lang('site.political-council')">
        <meta property="og:description" content="@lang('site.political-council')">
        <meta property="og:url" content="{{ url('/party-council') }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ url('gd.png') }}">
        <meta name="description" content="ქართული ოცნება - @lang('site.political-council')">
        <meta name="title" content="@lang('site.political-council')">
    @endif
    @if( Request::segment(1) && Request::segment(1)=='contact' )
        <meta property="og:title" content="ქართული ოცნება - @lang('site.contact')">
        <meta property="og:description" content="@lang('site.contact')">
        <meta property="og:url" content="{{ url('/contact') }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ url('gd.png') }}">
        <meta name="description" content="ქართული ოცნება - @lang('site.contact')">
        <meta name="title" content="@lang('site.contact')">
    @endif
    @if( Request::segment(1) && Request::segment(1)=='party-regulation' )
        <meta property="og:title" content="ქართული ოცნება - @lang('site.party-charter')">
        <meta property="og:description" content="@lang('site.party-charter')">
        <meta property="og:url" content="{{ url('/party-regulation') }}">
        <meta property="og:type" content="website">
        <meta property="og:image" content="{{ url('gd.png') }}">
        <meta name="description" content="ქართული ოცნება - @lang('site.party-charter')">
        <meta name="title" content="@lang('site.party-charter')">
    @endif




</head>

<body>
@include('website.partials.topbar')

<div class="wrapper-search"></div>
@yield('content')




<!-- FOOTER
================================================== -->
<footer class="footer-section">
    <div class="container">
        <div class="row pt-5">

            <div class="col-12 d-flex mb-5 jutify-content-center align-items-center d-block d-md-none">
                <div class="col-5 pl-0 mr-3">
                    <p><b> @lang('site.join-us') </b></p>
                </div>
                <div class="col-7 d-flex justify-content-between pl-0">
                    <a target="_blank" href="{{ $settings->facebook or '' }}">
                        <svg id="Component_3_1" data-name="Component 3 – 1" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                            <rect id="Rectangle_902" data-name="Rectangle 902" width="50" height="50" rx="6" fill="#0d8cf1"/>
                            <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.672,9.617A9.053,9.053,0,0,0,9.617.563c-2.271,0-6.162,2.419-6.162,2.419S.563,6.886.563,9.617A9.058,9.058,0,0,0,8.2,18.563V12.235H5.9V9.617H8.2V7.622S9.553,4.1,11.622,4.1a13.935,13.935,0,0,1,2.027.177V6.5H12.507a1.309,1.309,0,0,0-1.475,1.414v1.7h2.511l-.4,2.617h-2.11v6.328A9.058,9.058,0,0,0,18.672,9.617Z" transform="translate(15.438 15.438)" fill="#fff"/>
                        </svg>

                    </a>
                    <a target="_blank" href="{{ $settings->youtube or '' }}">
                        <svg id="Component_4_1" data-name="Component 4 – 1" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                            <rect id="Rectangle_900" data-name="Rectangle 900" width="50" height="50" rx="6" fill="red"/>
                            <path id="Icon_awesome-youtube" data-name="Icon awesome-youtube" d="M19.25,6.545a2.336,2.336,0,0,0-1.643-1.654A55.2,55.2,0,0,0,10.344,4.5a55.2,55.2,0,0,0-7.263.391A2.336,2.336,0,0,0,1.438,6.545a24.5,24.5,0,0,0-.388,4.5,24.5,24.5,0,0,0,.388,4.5,2.3,2.3,0,0,0,1.643,1.628,55.2,55.2,0,0,0,7.263.391,55.2,55.2,0,0,0,7.263-.391,2.3,2.3,0,0,0,1.643-1.628,24.5,24.5,0,0,0,.388-4.5,24.5,24.5,0,0,0-.388-4.5ZM8.443,13.812V8.284L13.3,11.048,8.443,13.812Z" transform="translate(14.95 13.5)" fill="#fff"/>
                        </svg>

                    </a>
                    <a target="_blank" href="{{ $settings->twitter or '' }}">
                        <svg id="Component_5_1" data-name="Component 5 – 1" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                            <rect id="Rectangle_901" data-name="Rectangle 901" width="50" height="50" rx="6" fill="#1fa2f2"/>
                            <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M17.621,7.356c.012.174.012.349.012.523A11.374,11.374,0,0,1,6.181,19.332,11.375,11.375,0,0,1,0,17.525a8.327,8.327,0,0,0,.972.05,8.061,8.061,0,0,0,5-1.72,4.032,4.032,0,0,1-3.764-2.792,5.076,5.076,0,0,0,.76.062,4.257,4.257,0,0,0,1.059-.137A4.026,4.026,0,0,1,.8,9.039v-.05A4.054,4.054,0,0,0,2.617,9.5,4.031,4.031,0,0,1,1.371,4.116a11.442,11.442,0,0,0,8.3,4.212,4.544,4.544,0,0,1-.1-.922,4.029,4.029,0,0,1,6.966-2.754,7.925,7.925,0,0,0,2.555-.972A4.014,4.014,0,0,1,17.322,5.9a8.069,8.069,0,0,0,2.318-.623,8.653,8.653,0,0,1-2.019,2.081Z" transform="translate(15 13.619)" fill="#fff"/>
                        </svg>

                    </a>
                </div>
            </div>

            <div class="col-md-5 col-lg-4 mb-3 d-none d-md-block">
                <p class="mb-4"><b> @lang('site.join-us') </b></p>

                <div class="d-flex jutify-content-center" style="max-width: 436px;">
                    <a class="mr-2" href="{{ $settings->facebook or '' }}" target="_blank">
                        <svg class="facebook_hover" xmlns="http://www.w3.org/2000/svg" width="100%" height="50" viewBox="0 0 132 50">
                            <g id="Group_769" data-name="Group 769" transform="translate(-968 -2935)">
                                <rect  id="Rectangle_902" data-name="Rectangle 902" width="132" height="50" rx="6" transform="translate(968 2935)"></rect>
                                <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.672,9.617A9.055,9.055,0,1,0,8.2,18.563V12.235H5.9V9.617H8.2V7.622A3.2,3.2,0,0,1,11.622,4.1a13.935,13.935,0,0,1,2.027.177V6.5H12.507a1.309,1.309,0,0,0-1.475,1.414v1.7h2.511l-.4,2.617h-2.11v6.328A9.058,9.058,0,0,0,18.672,9.617Z" transform="translate(987.438 2950.438)" fill="#fff"></path>
                                <text id="მოწონება" transform="translate(1016 2964)" fill="#fff" font-size="13" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.like')</tspan></text>
                            </g>
                        </svg>
                    </a>
                    <a class="mr-2" href="{{ $settings->youtube or '' }}" target="_blank">
                        <svg class="youtube_hover" xmlns="http://www.w3.org/2000/svg" width="100%" height="50" viewBox="0 0 132 50">
                            <g id="Group_770" data-name="Group 770" transform="translate(-1095 -2935)">
                                <rect id="Rectangle_900" data-name="Rectangle 900" width="132" height="50" rx="6" transform="translate(1095 2935)"></rect>
                                <path id="Icon_awesome-youtube" data-name="Icon awesome-youtube" d="M19.25,6.545a2.336,2.336,0,0,0-1.643-1.654A55.2,55.2,0,0,0,10.344,4.5a55.2,55.2,0,0,0-7.263.391A2.336,2.336,0,0,0,1.438,6.545a24.5,24.5,0,0,0-.388,4.5,24.5,24.5,0,0,0,.388,4.5,2.3,2.3,0,0,0,1.643,1.628,55.2,55.2,0,0,0,7.263.391,55.2,55.2,0,0,0,7.263-.391,2.3,2.3,0,0,0,1.643-1.628,24.5,24.5,0,0,0,.388-4.5,24.5,24.5,0,0,0-.388-4.5ZM8.443,13.812V8.284L13.3,11.048,8.443,13.812Z" transform="translate(1114.695 2948.965)" fill="#fff"></path>
                                <text id="გამოწერა" transform="translate(1144 2964)" fill="#fff" font-size="13" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.subscribe')</tspan></text>
                            </g>
                        </svg>
                    </a>
                    <a class="mr-2" href="{{ $settings->twitter or '' }}" target="_blank">
                        <svg class="twitter_hover" xmlns="http://www.w3.org/2000/svg" width="100%" height="50" viewBox="0 0 132 50">
                            <g id="Group_963" data-name="Group 963" transform="translate(-720 -3297)">
                                <g id="Group_771" data-name="Group 771" transform="translate(-555 362)">
                                    <rect id="Rectangle_901" data-name="Rectangle 901" width="132" height="50" rx="6" transform="translate(1275 2935)"></rect>
                                    <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M17.621,7.356c.012.174.012.349.012.523A11.374,11.374,0,0,1,6.181,19.332,11.375,11.375,0,0,1,0,17.525a8.327,8.327,0,0,0,.972.05,8.061,8.061,0,0,0,5-1.72,4.032,4.032,0,0,1-3.764-2.792,5.076,5.076,0,0,0,.76.062,4.257,4.257,0,0,0,1.059-.137A4.026,4.026,0,0,1,.8,9.039v-.05A4.054,4.054,0,0,0,2.617,9.5,4.031,4.031,0,0,1,1.371,4.116a11.442,11.442,0,0,0,8.3,4.212,4.544,4.544,0,0,1-.1-.922,4.029,4.029,0,0,1,6.966-2.754,7.925,7.925,0,0,0,2.555-.972A4.014,4.014,0,0,1,17.322,5.9a8.069,8.069,0,0,0,2.318-.623,8.653,8.653,0,0,1-2.019,2.081Z" transform="translate(1295.18 2948.643)" fill="#fff"></path>
                                    <text id="გამოწერა" transform="translate(1324 2964)" fill="#fff" font-size="13" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.subscribe')</tspan></text>
                                </g>
                            </g>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-lg-4 mb-3">
                <p class="mb-4 d-block d-md-none "><b> @lang('site.newsletter') </b></p>
                <form class="d-block d-md-none" action="#" method="post">
                    <div class="email-form" style="max-width: 100%;">
                        <input class="input-form" type="text" placeholder="@lang('site.your-email')">
                        <button>
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.519" height="20.787" viewBox="0 0 22.519 20.787"><path id="Icon_ionic-md-send" data-name="Icon ionic-md-send" d="M3.375,25.287,25.894,14.894,3.375,4.5v8.084l16.132,2.31L3.375,17.2Z" transform="translate(-3.375 -4.5)" fill="#1377c6"></path></svg>
                        </button>
                    </div>
                </form>
                <p class="mb-4 pl-2 d-none d-md-block"><b> @lang('site.newsletter') </b></p>
                <form class="pl-2 d-none d-md-block"  action="#" method="post">
                    <div class="email-form">
                        <input class="input-form" type="text" placeholder="@lang('site.your-email')">
                        <button>
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.519" height="20.787" viewBox="0 0 22.519 20.787"><path id="Icon_ionic-md-send" data-name="Icon ionic-md-send" d="M3.375,25.287,25.894,14.894,3.375,4.5v8.084l16.132,2.31L3.375,17.2Z" transform="translate(-3.375 -4.5)" fill="#1377c6"></path></svg>
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-md-4 col-lg-4 mb-3 d-none d-md-block pt-4 pr-0">
                <div class="d-flex justify-content-around">
                    <div class="col-6">
                        <a href="{{ url('transfers') }}"><svg xmlns="http://www.w3.org/2000/svg" width="100%" height="94" viewBox="0 0 260 94">
                                <g id="Group_1160" data-name="Group 1160" transform="translate(-1520 -3141)">
                                    <g id="Rectangle_977" data-name="Rectangle 977" transform="translate(1520 3141)" fill="#fff" stroke="#4d81e2" stroke-width="1">
                                        <rect width="260" height="94" rx="10" stroke="none"></rect>
                                        <rect x="0.5" y="0.5" width="259" height="93" rx="9.5" fill="none"></rect>
                                    </g>
                                    <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(1732.376 3184.377)">
                                        <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#5c5c5c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#5c5c5c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                    </g>
                                    <text id="@lang('site.youth')" transform="translate(1560 3193)" fill="#646464" font-size="20" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.donation')</tspan></text>
                                </g>
                            </svg>

                        </a>
                    </div>
                    <div class="col-6">

                        <a href=" {{ url('young') }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="94" viewBox="0 0 260 94">
                                <g id="Group_1160" data-name="Group 1160" transform="translate(-1520 -3141)">
                                    <g id="Rectangle_977" data-name="Rectangle 977" transform="translate(1520 3141)" fill="#fff" stroke="#4d81e2" stroke-width="1">
                                        <rect width="260" height="94" rx="10" stroke="none"></rect>
                                        <rect x="0.5" y="0.5" width="259" height="93" rx="9.5" fill="none"></rect>
                                    </g>
                                    <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(1732.376 3184.377)">
                                        <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#5c5c5c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#5c5c5c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                    </g>
                                    <text id="ახალგაზრდული" transform="translate(1560 3193)" fill="#646464" font-size="20" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">
                                            @lang('site.youth')</tspan></text>
                                </g>
                            </svg>

                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-4 d-block d-md-none">
                <a href="{{ url('transfers') }}">

                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="77" viewBox="0 0 334 77">
                        <g id="Group_1156" data-name="Group 1156" transform="translate(-21 -3325)">
                            <g id="Rectangle_1094" data-name="Rectangle 1094" transform="translate(21 3325)" fill="#4d81e2" stroke="#4d81e2" stroke-width="1">
                                <rect width="334" height="77" rx="10" stroke="none"></rect>
                                <rect x="0.5" y="0.5" width="333" height="76" rx="9.5" fill="none"></rect>
                            </g>
                            <text id="ახალგაზრდული" transform="translate(61 3369)" fill="#fff" font-size="15" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.donation')</tspan></text>
                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(307.376 3359.377)">
                                <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.658" transform="translate(-10.5 -10.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.658H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="col-12 d-none d-md-block pt-3 mb-3"><div class="b-b"></div></div>
            <div class="col-12 mb-3 d-block d-md-none">

                <a href="{{ url('young') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="77" viewBox="0 0 334 77">
                        <g id="Group_1156" data-name="Group 1156" transform="translate(-21 -3325)">
                            <g id="Rectangle_1094" data-name="Rectangle 1094" transform="translate(21 3325)" fill="#4d81e2" stroke="#4d81e2" stroke-width="1">
                                <rect width="334" height="77" rx="10" stroke="none"></rect>
                                <rect x="0.5" y="0.5" width="333" height="76" rx="9.5" fill="none"></rect>
                            </g>
                            <text id="ახალგაზრდული" transform="translate(61 3369)" fill="#fff" font-size="15" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.youth')</tspan></text>
                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(307.376 3359.377)">
                                <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.658" transform="translate(-10.5 -10.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.658H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                            </g>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="col-12 mt-1 mb-5">
                <div class="row pt-4 m-0 align-items-center">
                    <div class="col-8 pl-0 d-none d-md-block">
                        <div class="d-flex align-items-center">
                            <img style="width: 66.86px;height: 69.19px" class="mr-3" src="{{ url('website') }}/images/footerlogo.png">
                            <p class="gdc">@lang('site.georgian-dream-Democratic')</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 d-block d-md-none">
                        <p class="gdc text-center">@lang('site.all-rights-reserved')</p>
                    </div>
                    <div class="col-12 col-md-4 d-none d-md-block pr-0">
                        <p class="gdc text-right">@lang('site.all-rights-reserved')</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<script src="{{ asset('website') }}/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('website') }}/assets/libs/@fancyapps/fancybox/dist/jquery.fancybox.min.js"></script>

<!-- Theme JS -->
<script src="{{ asset('website') }}/assets/js/theme.min.js"></script>

<script src="{{ asset('website/slack') }}/slick.js"></script>
<link href="{{ asset('website/slack') }}/slick-theme.css" rel="stylesheet"/>
<link href="{{ asset('website/slack') }}/slick.css" rel="stylesheet"/>

<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('website/bootstrap-select') }}/bootstrap-select.min.js"></script>


<!-- (Optional) Latest compiled and minified JavaScript translation files -->




<script>






    function openNav() {
        document.getElementById("myNav").style.display = "block";
    }

    function closeNav() {
        document.getElementById("myNav").style.display = "none";
    }



    function openvideogallery(id) {

        document.getElementById("video_"+id+"").style.display = "block";
    }

    function closevideogallery(id) {
        var player=$('.player_'+id).html();
        $('.player_'+id).html('');
        $('.player_'+id).append(player);
        document.getElementById("video_"+id+"").style.display = "none";
    }


    function openvideo(id) {
        document.getElementById("video_"+id+"").style.display = "block";
    }

    function closevideo(id) {
        var player=$('.player').html();
        $(".player").html('');
        $(".player").append(player);
        document.getElementById("video_"+id+"").style.display = "none";
    }




    function openphotogallery(id) {
        document.getElementById("photo_"+id+"").style.display = "block";
    }

    function closephotogallery(id) {
        document.getElementById("photo_"+id+"").style.display = "none";
    }



    function openmobilephotogallery(id) {
        document.getElementById("photo").style.display = "block";
    }

    function closemobilephotogallery(id) {
        document.getElementById("photo").style.display = "none";
    }






    function o_menu() {
        document.getElementById("mymenu").style.display = "block";

    }

    function c_menu() {
        document.getElementById("mymenu").style.display = "none";

    }




    $(document).ready(function(){


        $( ".nav-link" ).hover(
            function() {
                $(this).find( '.arrow_down' ).hide();
                $(this).find( '.arrow_up' ).show();
            }, function() {
                $(this).find( '.arrow_down' ).show();
                $(this).find( '.arrow_up' ).hide();
            }
        );


        if ($(window).width() > 600) {
            $(function () {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 149) {
                        $('.navbar .navbar-brand .img_default').hide();

                        $('.navbar .navbar-brand .img_scroll').show();
                        $('.navbar').css({"height": "90px"});
                    }
                    if ($(this).scrollTop() < 149) {
                        $('.navbar .navbar-brand .img_scroll').hide();
                        $('.navbar .navbar-brand .img_default').show();

                        $('.navbar').css({"height": "149px"});
                    }
                })
            });

        }




        $( ".search-text" ).focus(function() {
            var height=$( document ).height();
           $('.custom-form').css({"background-color":"#fff","color":"#4D81E2"});
           $('.search-text').css({"color":"#4D81E2"});
           $('#Path_72').css({"stroke":"#27509C"});
           $('#Path_73').css({"stroke":"#27509C"});
           $(".wrapper-search").css({"height": height+'px'});
           $(".wrapper-search").show();
         //  $('html').css({'overflow':"hidden"});
        });

        $( ".search-text" ).focusout(function() {
            $('.custom-form').css({"background-color":"rgba(255, 255, 255, 0.1)","color":"#fff"});
            $('.search-text').css({"color":"#fff"});
            $('#Path_72').css({"stroke":"#fff"});
            $('#Path_73').css({"stroke":"#fff"});
            $(".wrapper-search").hide();
           // $('html').css({'overflow':"unset"});
        });


        $('.carousel').carousel({
            interval: false
        });


        @if(Request::segment(1)!='photo-gallery')

        $('.play').click(function () {
            $('#custCarousel').carousel('cycle');
        });
        $('.pause').click(function () {
            $('#custCarousel').carousel('pause');
        });




        var totalItems = $('#custCarousel .carousel-inner .carousel-item').length;
        var currentIndex = $('div.active').index() + 1;
        $('.count').html('1/'+totalItems+'');

        $('#custCarousel').on('slid.bs.carousel', function() {

            currentIndex = $('div.active').index() + 1;
            $('.count').html(''+currentIndex+'/'+totalItems+'');
        });



        var totalItems = $('#carouselExampleIndicators_mobile .carousel-inner .carousel-item').length;
        var currentIndex = $('#carouselExampleIndicators_mobile div.active').index() + 1;
        $('.counts').html('1/'+totalItems+'');

        $('#carouselExampleIndicators_mobile').on('slid.bs.carousel', function() {

            currentIndex = $('#carouselExampleIndicators_mobile div.active').index() + 1;
            $('.counts').html(''+currentIndex+'/'+totalItems+'');
        });

        @endif


        $('.c-menu').on('click', function() {


            $('.sublist').removeClass('d-block').hide();
            if($(this).children('.sublist').hasClass( "d-none" )){
                $(this).children('.sublist').removeClass('d-none').addClass('d-block');
            }else{
                $(this).children('.sublist').removeClass('d-block').addClass('d-none');
            }

        });








    });


    </script>

@if(Request::segment(1)=='')
<script>
    $(document).ready(function(){





        var time = 5;
    var $bar,
        $slick,
        isPause,
        tick,
        percentTime;

    $slick = $('.slider');
    $slick.slick({
        draggable: true,
        adaptiveHeight: false,
        dots: false,
        mobileFirst: true,
        pauseOnDotsHover: true,
    });

    $bar = $('.slider-progress .progress');
    $barRound = $('.progress');

    $('.slider-wrapper').on({
        mouseenter: function() {
            isPause = true;
        },
        mouseleave: function() {
            isPause = false;
        }
    });

    var progressBarIndex = 0;
    function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        isPause = false;
        tick = setInterval(interval, 5);

    }




    var $rbar = $('.progress circle');
    var rlen = 2 * Math.PI * $rbar.attr('r');
    function interval() {

        if (($('.progress-round__wrap div[data-slick-index="' + progressBarIndex + '"]').attr("aria-hidden")) === "true") {
            progressBarIndex = $('.progress-round__wrap div[aria-hidden="false"]').data("slickIndex");
            startProgressbar();
        } else {
            percentTime += 1 / (time + 0.1);
            $('.progress_' + progressBarIndex + ' circle').css({
                width: percentTime + '%'
            });
            $('.progress_' + progressBarIndex + ' circle').css({
                'stroke-dasharray': rlen,
                'stroke-dashoffset': rlen * (1 - percentTime / 100),
                'stroke':'#4D81E2',

            });
            $('.text_' + progressBarIndex + '').css({
                'fill': '#FDB714',
            });



            if (percentTime >= 100) {
                //alert(progressBarIndex);
                $('.text_' + progressBarIndex + '').css({
                    'fill': '#B2B2B2',

                });
                $('.progress_' + progressBarIndex + ' circle').css({
                    'stroke':'#EBEBEB'

                });

                $slick.slick('slickNext');
                progressBarIndex++;
                if (progressBarIndex > 3) {
                    progressBarIndex = 0;
                }
                startProgressbar();
            }
        }

    }


    function resetProgressbar() {
        $('.progress_' + progressBarIndex + ' circle').css({
            width: 0+'%'
        });

        clearTimeout(tick);
    }



    startProgressbar();


        $( ".progress" ).click(function() {
            var key=$(this).attr('x_id');
            $(".slider").slick("goTo", key);

            $('.progress circle').css({
                width: 0+'%',
                'stroke-dasharray': '220px',
            });


            $('.text').css({
                'fill': '#B2B2B2',

            });
            $('.progress circle').css({
                'stroke':'#EBEBEB'

            });

            //resetProgressbar();
            progressBarIndex=key;
            startProgressbar();


        });


    });


</script>
@else
    <script>
        $(document).ready(function(){
        $('.slider-single').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            adaptiveHeight: true,
            infinite: false,
            useTransform: true,
            speed: 400,
            cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        });

        $('.slider-nav')
            .on('init', function(event, slick) {
                $('.slider-nav .slick-slide.slick-current').addClass('is-active');
            })
            .slick({
                slidesToShow: 7,
                slidesToScroll: 7,
                dots: false,
                focusOnSelect: false,
                infinite: false,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                    }
                }, {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                }, {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }]
            });

        $('.slider-single').on('afterChange', function(event, slick, currentSlide) {
            $('.slider-nav').slick('slickGoTo', currentSlide);
            var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
            $('.slider-nav .slick-slide.is-active').removeClass('is-active');
            $(currrentNavSlideElem).addClass('is-active');
        });

        $('.slider-nav').on('click', '.slick-slide', function(event) {
            event.preventDefault();
            var goToSingleSlide = $(this).data('slick-index');

            $('.slider-single').slick('slickGoTo', goToSingleSlide);
        });
        });
    </script>
@endif

</body>

</html>