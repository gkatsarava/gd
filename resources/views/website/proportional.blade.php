@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div class="proportional-tbilisi  mt-3">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-md-12 pt-5 mb-3 d-none d-md-block">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                            @lang('site.elections2020')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">



                        <!-- column -->
                  {{--      <div class="col-12 col-md-6 mb-100-px d-none d-md-block">
                            <div class="row">
                                <div class="col-12 d-flex">
                                    <a href="{{ url('/mayor') }}"><div class="mr-3 breadcrumb-title pb-15-px">@lang('site.mayor')</div></a>

                                    <a href="{{ url('/candidates') }}"><div class="mr-3 breadcrumb-title pb-15-px">@lang('site.majoritarians')</div></a>
                                    <a href="{{ url('/proportional') }}"><div class="breadcrumb-title active_tab pb-15-px">@lang('site.proportional-list')</div></a>
                                </div>
                            </div>
                        </div>--}}
                        <!-- column -->
                        <div class="col-12 col-md-6 mb-100-px d-none d-md-block">
                        </div>

                        <!-- column -->
                        <!-- column -->
                       {{-- <div class="col-12 d-block d-md-none mb-30">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-3 col-md-6">
                                            <a href="{{ url('/mayor') }}">
                                                <div class="breadcrumb-title text-center ">@lang('site.mayor')</div>
                                            </a>
                                        </div>
                                        <div class="col-4 col-md-6">
                                            <a href="{{ url('/candidates') }}">
                                                <div class="breadcrumb-title text-center ">@lang('site.majoritarians')</div>
                                            </a>
                                        </div>
                                        <div class="col-4 col-md-6 pr-0">
                                            <a href="{{ url('/proportional') }}">
                                                <div class="breadcrumb-title text-center active_tab">@lang('site.proportional-list')</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        <div class="col-12 d-block d-md-none mb-30"></div>
                        <!-- column -->
                        <!-- column -->


                       {{-- <div class="col-12  d-block d-md-none col-md-6 mb-5">
                            <div class="row">
                                <div class="col-6 float-left">

                                    <a href="#" style="width: 146px;float: right;">
                                        <h1 class="mr-3 btn-disabled">თბილისი</h1>
                                    </a>

                                </div>
                                <div style="float:left;width: 170px;">
                                    <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                        <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                            <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </svg>

                                        <select class="selectpicker" disabled name="region">

                                            <option value=""> აირჩიეთ რეგიონი </option>

                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>--}}

                        {{-- d-block d-md-none--}}
                        <div class="col-12 mb-5 d-none">

                            <div class="row">
                                <div class="col-6 pr-0">
                                    <a class="" href="{{ url('/candidates') }}" style="/*width: 146px;float: right;*/">
                                        @if(!isset($request->region))
                                            <div class="btn-disabled">
                                                @lang('site.Tbilisi')
                                            </div>
                                        @else
                                            <div class="btn-major" style="      padding:unset;   display: block;line-height: 62px;text-align: center;">@lang('site.Tbilisi')</div>
                                        @endif
                                    </a>
                                </div>
                                <div class="col-6">
                                    <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                        <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                            <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </svg>

                                        <select disabled class="selectpicker"  style="width:280px;"  onchange="this.form.submit()" name="region">

                                            <option value="">  @lang('site.region') </option>

                                        </select>
                                    </form>
                                </div>
                            </div>

                        </div>

                        {{-- d-none d-md-block--}}
                        <div class="col-12 d-none col-md-6 mb-5">
                            <div class="row">
                                <div class="col-6">

                                    <a href="#" style="width: 146px;float: right;">
                                        <h1 class="mr-3 btn-disabled">@lang('site.Tbilisi')</h1>
                                    </a>

                                </div>
                                <div style="float:left;width:280px;">
                                    <form action="#" class="dropdown-select-box-style" style="position:relative;"  method="GET">
                                        <svg class="dropdown-arrow-class" xmlns="http://www.w3.org/2000/svg" width="14.828" height="8.414" viewBox="0 0 14.828 8.414">
                                            <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(-6.086 14.914) rotate(-90)">
                                                <path id="Path_227" data-name="Path 227" d="M13.5,19.5l-6-6,6-6" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            </g>
                                        </svg>

                                        <select class="selectpicker" disabled name="region">

                                            <option value=""> @lang('site.region') </option>

                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>




                    @foreach($proportional as $key=> $row)




                        <!-- column -->
                        <div class="col-12 col-md-12 change_cl pl-5 ">
                            <div class="align-items-center content_proportional">
                                {!! $row->desc !!}

                              {{--  <div class="col-1">
                                    <h1 class="proportional-candidates-number mt-0 mt-md-2">{{ $key+1 }}</h1>
                                </div>
                                <div class="col-10">
                                    <h1 class="proportional-candidates-title pl-4">{{ $row->name }}</h1>
                                </div>
                                <div class="col-12 pt-4 pb-4 d-block d-md-none">
                                    <p class="b-t m-0"></p>
                                </div>
                                <div class="col-12 pt-40 mb-60 d-none d-md-block">
                                    <p class="b-t m-0"></p>
                                </div>
--}}
                            </div>
                        </div>
                        <!-- column -->


                        <!-- column -->
                        <div class="col-12 col-md-6 change_cl pl-3 ">

                        </div>
                        <!-- column -->
                        @endforeach







                    </div>
                </div>

            </div>
        </div>
    </div>


@stop