@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')





    <div class="news mt-3">
        <div class="container-custom" >



            <div class="row">
                <div class="col-md-12 pt-5 mb-3 mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                            {{--  <svg class="mr-3 mt-2" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                                  <path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                                             --}}


                                            @lang('site.regions_program')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">



            @foreach($news as $row)
                <!-- news -->
                    <div style="" class="col-12 mb-5 p-0 mobile-margin-bottom-none margin-bottom-30-px">
                        <div class="col-12">
                            <div class="col-12 p-0">
                                <div class="row">
                                    <div class="col-5 col-md-7 col-lg-7 col-xl-4 pr-0 pr-md-5">
                                        <a href="{{ url('show-region-program') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">

                                            @if(isset($row->Pictures->where('first_picture','1')->first()->picture))
                                                <img  class="img-fluid" src="{{ asset('uploads/region_programe') }}/{{ $row->Pictures->where('first_picture','1')->first()->picture }}">
                                            @else
                                                <img  class="img-fluid" src="{{ asset('uploads/region_programe') }}/{{ $row->Pictures->first()->picture }}">
                                            @endif

                                        </a>
                                    </div>
                                    <div class="col-7 col-md-5 col-lg-5 col-xl-8">
                                        <p class="mb-3 date-box-title">
                                            {{ $row->created_at->format('d') }}
                                            {{ $month[$row->created_at->format('m')] }},
                                            {{ $row->created_at->format('Y') }}
                                        </p>
                                        <a href="{{ url('show-region-program') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
                                            <h1 class="mb-3 news-title">
                                                {{ $row->title }}
                                            </h1>
                                        </a>
                                        <div class=" d-none d-md-block">
                                            <a href="{{ url('show-region-program') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">

                                                <div class="news-description" style="    -webkit-line-clamp: 2;">
                                                    {!! strip_tags($row->desc) !!}
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- news -->
                @endforeach

                <div class="col-12">
                    <div class="">
                        <div class="col-12 margin-top-30-px margin-bottom-30-px p-0 d-block d-md-none">
                            <p class="b-t m-0"></p>
                        </div>
                    </div>
                </div>


                <div class="col-12 mt-5 pt-5 gd-pagination  b-t">
                    <div class="row">
                        <div class="col-12">
                            <nav class="" aria-label="Page navigation example">
                                <ul class="pagination d-flex align-items-center justify-content-center">
                                    @if ($news->onFirstPage())
                                        <li class="page-item mr-4">
                                            <a class="page-link previous" href="#">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                    <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(7.414 12.829) rotate(-135)">
                                                        <path id="Path_211" data-name="Path 211" d="M0,7.657,7.657,0" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                        <path id="Path_212" data-name="Path 212" d="M7.657,7.657V0H0" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                    @else
                                        <li class="page-item mr-4">
                                            <a class="page-link previous" href="{{ $news->previousPageUrl() }}">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                    <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(7.414 12.829) rotate(-135)">
                                                        <path id="Path_211" data-name="Path 211" d="M0,7.657,7.657,0" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                        <path  id="Path_212" data-name="Path 212" d="M7.657,7.657V0H0" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>

                                    @endif



                                    <?php
                                    $start = $news->currentPage() - 1; // show 3 pagination links before current
                                    $end = $news->currentPage() + 3; // show 3 pagination links after current
                                    if($start < 1) {
                                        $start = 1; // reset start to 1
                                        $end += 1;
                                    }
                                    if($end >= $news->lastPage() ) $end = $news->lastPage(); // reset end to last page
                                    ?>


                                    @for ($i = $start; $i <= $end; $i++)

                                        @if($i==$request->page)

                                            <li class="page-item mr-2">
                                                <a class="page-link number page-active" href="{{ $news->url($i) }}">{{ $i }}</a>
                                            </li>
                                        @else

                                            <li class="page-item mr-2">
                                                <a class="page-link number" href="{{ $news->url($i) }}">{{ $i }}</a>
                                            </li>
                                        @endif



                                    @endfor





                                    @if ($news->hasMorePages())
                                        <li class="page-item">
                                            <a class="page-link next" href="{{ $news->nextPageUrl() }}#">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                    <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(2 7.415) rotate(-45)">
                                                        <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                        <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>
                                    @else
                                        <li class="page-item">
                                            <a class="page-link next" href="#">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="14.829" height="14.829" viewBox="0 0 14.829 14.829">
                                                    <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(2 7.415) rotate(-45)">
                                                        <path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                        <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#4d81e2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </a>
                                        </li>

                                    @endif

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- pagination -->
            {{-- <div class="col-12 pt-5 gd-pagination d-none d-md-block">
                 <div class="row">
                     <div class="col-12">
                         {{  $news->appends(request()->input())->links("pagination::bootstrap-4") }}

                     </div>
                 </div>
             </div>--}}
            <!-- pagination -->


            </div>

        </div>
    </div>


@stop