@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div class="about-party mt-3">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-md-12 pt-5 mb-3 mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white pl-0">
                                        <li class="breadcrumb-item_other active" aria-current="page"> @lang('site.party') <svg class="ml-3 mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg> </li>
                                        <li class="breadcrumb-item_other active" aria-current="page"> @lang('site.party-charter') </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 margin-top-50-px">
                    <div class="row">

                        <!-- column -->
                        <div class="col-12 col-md-8 mb-5 mobile-margin-bottom-none">
                            <div class="row">
                                <div class="col-12">
                                   {{-- <h1 class="about-title">
                                        {{ $regulation->name }}
                                    </h1>--}}
                                    <div>
                                        {!! $regulation->desc !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                        <!-- column -->
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col-12">
                                    <div class="about-regulation-color d-flex align-items-center">
                                        <div class="container">
                                            <div class="row align-items-center">
                                                <div class="col-2">
                                                    <a href="{{ url('uploads/rules') }}/{{ $regulation->file }}">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="38" height="47" viewBox="0 0 38 47">
                                                        <g id="Group_994" data-name="Group 994" transform="translate(-1309.259 -380)">
                                                            <g id="Group_992" data-name="Group 992">
                                                                <path id="Path_219" data-name="Path 219" d="M31.5,3h-21A4.5,4.5,0,0,0,6,7.5v36A4.5,4.5,0,0,0,10.5,48h27A4.5,4.5,0,0,0,42,43.5v-30Z" transform="translate(1304.259 378)" fill="none" stroke="#dc544b" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                                <path id="Path_220" data-name="Path 220" d="M21,3V13.518H31.518" transform="translate(1314.741 378)" fill="none" stroke="#dc544b" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                            </g>
                                                            <g id="Group_993" data-name="Group 993" transform="translate(0 -2)">
                                                                <rect id="Rectangle_1003" data-name="Rectangle 1003" width="22" height="14" rx="3" transform="translate(1317 406)" fill="#dc544b"/>
                                                                <text id="pdf" transform="translate(1328 416)" fill="#fff" font-size="8" font-family="SegoeUI, Segoe UI"><tspan x="-6.998" y="0">PDF</tspan></text>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    </a>

                                                </div>

                                                <div class="col-8">
                                                    <a target="_blank" href="{{ url('uploads/rules') }}/{{ $regulation->file }}">
                                                    <p class="more">@lang('site.download')</p>
                                                    </a>
                                                </div>

                                                <div class="col-2">
                                                    <a target="_blank" href="{{ url('uploads/rules') }}/{{ $regulation->file }}">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="23.828" height="30.242" viewBox="0 0 23.828 30.242">
                                                            <g id="Group_990" data-name="Group 990" transform="translate(-1544.074 -379.508)">
                                                                <path id="Path_218" data-name="Path 218" d="M18,7.5,28.5,18,18,28.5" transform="translate(1573.988 380.25) rotate(90)" fill="none" stroke="#dc544b" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                                <path id="Path_225" data-name="Path 225" d="M-12488.012,1788.833v-28.242" transform="translate(14044 -1380.083)" fill="none" stroke="#dc544b" stroke-linecap="round" stroke-width="2"/>
                                                            </g>
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop