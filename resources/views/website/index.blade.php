@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')



    <div class="gd-slider-background">
</div>
<div class="slider-wrapper mt-252-px" id="slick-1" style="height:650px;overflow:hidden;">
    <div class="slider">
        @foreach($slider as $key=> $row)
        <div class="slide  slide_{{ $key}}" >

            <div class="content" data-id="{{ $key }}">
                <div class="gd-slider">
                    <div class="container">
                        <div class="row mt-0">
                            <div class="col-12 col-lg-7 p-0 p-md-3">


                                <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">

                                @if($row->Pictures->where('first_picture','1')->first())
                                    <img  class="img-fluid" src="{{ asset('uploads/news') }}/{{ $row->Pictures->where('first_picture','1')->first()->picture }}">
                                @else
                                    <img  class="img-fluid" src="{{ asset('uploads/news') }}/{{ $row->Pictures->first()->picture }}">
                                @endif
                                </a>
                                <div class="slider-position">
                                    <div class="slider-news p-5">
                                        <div class="slider-detail mb-2"></div>
                                        <p class="date-box-title">
                                            {{ $row->created_at->format('d') }}
                                            {{ $month[$row->created_at->format('m')] }},
                                            {{ $row->created_at->format('Y') }}
                                        </p>
                                        <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
                                            <h1 class="slider-title text-left">
                                                {{ $row->title }}
                                            </h1>
                                        </a>
                                        <p class="slider-description text-left">
                                            {!! strip_tags($row->desc) !!}
                                        </p>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-lg-5 p-0 d-block d-md-none">
                                <div class="slider-news p-5">
                                    <div class="slider-detail mb-2"></div>
                                    <p class="date-box-title">
                                        {{ $row->created_at->format('d') }}
                                        {{ $month[$row->created_at->format('m')] }},
                                        {{ $row->created_at->format('Y') }}
                                    </p>
                                    <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
                                        <h1 class="slider-title text-left">
                                            {{ $row->title }}
                                        </h1>
                                    </a>
                                    <p class="slider-description text-left">
                                        {!! strip_tags($row->desc) !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        @endforeach

    </div>

    <div class="slider-position slider-pg d-none d-md-block" style="cursor: pointer;">

        @foreach($slider as $key=> $row)
            @if($key<5)
                <svg  x_id="{{ $key }}" class="progress progress_{{ $key }}" style="width: 77px;" data-slick-index="{{ $key }}">
                    <circle r="18" cx="55" cy="80"/>
                    <text class="text text_{{$key}}" x="72%" y="54%" font-size="1em" fill="#B2B2B2" text-anchor="middle" dy=".3em">
                        {{ $key+1 }}
                    </text>
                </svg>
            @endif
        @endforeach



    </div>
    <div class="slider-progress">
        <div class="progress"></div>
    </div>



</div>
{{--<div class="sliderContainer">
    <div class="slider single-item">


        <div>
            <div class="gd-slider">
                <div class="container">
                    <div class="row mt-3">
                        <div class="col-12 col-lg-7 ">
                            <img class="img-fluid" src="{{ url('website') }}/images/MaskGroup27.png">
                        </div>
                        <div class="col-12 col-lg-5 d-flex" style="">
                            <div class="slider-news p-5">
                                <div class="slider-detail mb-2"></div>
                                <p class="date-box-title" style="margin-bottom: 30px">29 ივნისი, 2020</p>
                                <a href="archive-news.html"><h1 class="slider-title" style="margin-bottom: 30px">პოლიტიკური გაერთიანება „ქართული ოცნება - დემოკრატიული საქართველოს“ აღმასრულებელი მდივნის ირაკლი კობახიძის განცხადება</h1></a>
                                <p class="slider-description">ბიძინა ივანიშვილის საერთაშორისო საქველმოქმედო ფონდ „ქართუ“-ს დაფინანსებით, სოფელ ძველ ხიბულაში ახალი საბავშვო ბაღი აშენდა და მუნიციპალიტეტს გადაეცა …</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="gd-slider">
                <div class="container">
                    <div class="row mt-3">
                        <div class="col-12 col-lg-7 ">
                            <img class="img-fluid" src="{{ url('website') }}/images/MaskGroup27.png">
                        </div>
                        <div class="col-12 col-lg-5 d-flex" style="">
                            <div class="slider-news p-5">
                                <div class="slider-detail mb-2"></div>
                                <p class="date-box-title" style="margin-bottom: 30px">29 ივნისი, 2020</p>
                                <a href="archive-news.html"><h1 class="slider-title" style="margin-bottom: 30px">პოლიტიკური გაერთიანება „ქართული ოცნება - დემოკრატიული საქართველოს“ აღმასრულებელი მდივნის ირაკლი კობახიძის განცხადება</h1></a>
                                <p class="slider-description">ბიძინა ივანიშვილის საერთაშორისო საქველმოქმედო ფონდ „ქართუ“-ს დაფინანსებით, სოფელ ძველ ხიბულაში ახალი საბავშვო ბაღი აშენდა და მუნიციპალიტეტს გადაეცა …</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="gd-slider">
                <div class="container">
                    <div class="row mt-3">
                        <div class="col-12 col-lg-7 ">
                            <img class="img-fluid" src="{{ url('website') }}/images/MaskGroup27.png">
                        </div>
                        <div class="col-12 col-lg-5 d-flex" style="">
                            <div class="slider-news p-5">
                                <div class="slider-detail mb-2"></div>
                                <p class="date-box-title" style="margin-bottom: 30px">29 ივნისი, 2020</p>
                                <a href="archive-news.html"><h1 class="slider-title" style="margin-bottom: 30px">პოლიტიკური გაერთიანება „ქართული ოცნება - დემოკრატიული საქართველოს“ აღმასრულებელი მდივნის ირაკლი კობახიძის განცხადება</h1></a>
                                <p class="slider-description">ბიძინა ივანიშვილის საერთაშორისო საქველმოქმედო ფონდ „ქართუ“-ს დაფინანსებით, სოფელ ძველ ხიბულაში ახალი საბავშვო ბაღი აშენდა და მუნიციპალიტეტს გადაეცა …</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <div class="progressBarContainer">
        <div>


            <div class="progress-round__wrap">
                <svg  data-slick-index="0" class="progress">
                    <circle r="50" cx="150" cy="80"/>
                </svg>
            </div>

        </div>

        <div>
            <div  class="progress-round__wrap">
                <svg data-slick-index="1" class="progress">
                    <circle r="50" cx="150" cy="80"/>
                </svg>
            </div>
        </div>

        <div>
            <div class="progress-round__wrap">
                <svg  data-slick-index="2"class="progress">
                    <circle r="50" cx="150" cy="80"/>
                </svg>
            </div>
        </div>
    </div>


</div>--}}

<div class="gd-news">
    <div class="container-custom" style="margin-top:unset!important;">
        <div class="row">



            <div class="col-md-12 mobile-margin-bottom-none mb-26 pt-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-white pl-0 mb-0 p-0">
                        <li class="breadcrumb-item active" aria-current="page">
                            <a class="href-gray" href="{{ url('news') }}">
                            <svg class="mr-3 mt-0" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path></svg>
                            @lang('site.news')
                            </a>
                        </li>
                    </ol>
                </nav>
            </div>


            @foreach($news as $row)

            <div class="col-12 mb-54">
                <div class="row">
                    <div class="col-5 col-md-7 col-lg-7 col-xl-4 pr-0 pr-md-5 mb-2">
                        <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
                            @if($row->Pictures->where('first_picture','1')->first())
                            <img  class="img-fluid" src="{{ asset('uploads/news') }}/{{ $row->Pictures->where('first_picture','1')->first()->picture }}">
                            @else
                            <img  class="img-fluid" src="{{ asset('uploads/news') }}/{{ $row->Pictures->first()->picture }}">
                            @endif
                        </a>
                    </div>
                    <div class="col-7 col-md-5 col-lg-5 col-xl-8">
                        <p class="date-box-title">
                            {{ $row->created_at->format('d') }}
                            {{ $month[$row->created_at->format('m')] }},
                            {{ $row->created_at->format('Y') }}
                        </p>
                        <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
                            <h1 class="gd-news-title">
                                {{ $row->title }}
                            </h1>
                        </a>
                        <div class=" d-none d-md-block">
                            <a href="{{ url('show-news') }}/{{ $row->id }}/{{ str_replace('/', '-', str_replace(' ', '-', $row->title)) }}?lang={{ App::getLocale() == "ge" ? "ge" : "en" }}">
                        <div class="gd-news-description">
                            {!! strip_tags($row->desc) !!}
                        </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</div>


    <div class="container line-mobile" style="">
        <div class="row">
            <div class="col-12 b-b"></div>
        </div>
    </div>

{{--<div class="section-padding">--}}
    {{--<div class="container-custom mb-54" style="margin-top:unset!important;">--}}
        {{--<div class="row">--}}
            {{--<div class="col-12 mb-54">--}}
                {{--<nav aria-label="breadcrumb">--}}
                    {{--<ol class="breadcrumb bg-white pl-0 mb-0 p-0">--}}
                        {{--<li class="breadcrumb-item active" aria-current="page">--}}
                            {{--<a href="{{ url('candidates') }}" class="href-gray">--}}
                            {{--<svg class="mr-3 mt-0" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path></svg>--}}
                            {{--@lang('site.elections2020')--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ol>--}}
                {{--</nav>--}}
            {{--</div>--}}

            {{--<div class="col-12 col-lg-7">--}}
                {{--<div class="col-12 pl-0 pr-0">--}}
                    {{--<div class="row">--}}

                        {{--@foreach($majoritarians as $row)--}}
                        {{--<div class="col-12 col-lg-6">--}}
                            {{--<div class="card">--}}
                                {{--<div class="card-body pt-0 pr-0 pl-0">--}}
                                    {{--<div class="row">--}}
                                        {{--<div class="col-7">--}}
                                            {{--<a class="black-color" href="{{ url('show-candidates') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">--}}

                                            {{--<h5 class="card-title  political-title">--}}
                                                {{--{!!   str_replace(' ','<br>',$row->name) !!}--}}
                                            {{--</h5>--}}
                                            {{--</a>--}}
                                            {{--<a class="blue-color" href="{{ url('show-candidates') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">--}}

                                            {{--<p class="card-text political-desc">--}}
                                                {{--{{ $row->location }}--}}
                                            {{--</p>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-5">--}}
                                            {{--<a class="black-color" href="{{ url('show-candidates') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->name) }}">--}}

                                            {{--<img class="img-fluid" src="{{ asset('uploads/majoritarians/') }}/{{ $row->picture }}">--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}


                            {{--<div class="col-12 col-lg-6 ">--}}
                                {{--<div class="card">--}}
                                    {{--<div class="card-body pt-1 pr-0 pl-0">--}}
                                        {{--<div class="row">--}}

                                            {{--<div class="col-12">--}}
                                                {{--<div class="show-more d-flex justify-content-center">--}}
                                                    {{--<a href="{{ url('/') }}/candidates">--}}
                                                        {{--<h1>--}}
                                                            {{--@lang('site.list-of-majoritarians')--}}
                                                            {{--<svg class="ml-2" xmlns="http://www.w3.org/2000/svg" width="9.072" height="9.071" viewBox="0 0 9.072 9.071"><g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(0.707 0.707)"><path id="Path_211" data-name="Path 211" d="M10.5,10.5l7.657,7.657" transform="translate(-10.5 -10.5)" fill="none" stroke="#5c5c5c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path><path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#5c5c5c" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>--}}
                                                                {{--</g>--}}
                                                            {{--</svg>--}}
                                                        {{--</h1>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}


                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}



            {{--<div class="col-12 col-lg-5">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-12 pr-0 pl-0">--}}
                        {{--<div class="img-majories">--}}
                            {{--<div class="card ">--}}
                                {{--<img src="{{ url('website') }}/images/flagmask.jpg" class="card-img" alt="...">--}}
                                {{--<div class="card-img-overlay">--}}
                                    {{--<h5 class="card-title card-title-box-head mb-0">საქართველოს <br> საპარლამენტო არჩევნები</h5>--}}
                                    {{--<a href="{{ url('/candidates') }}"> <p class="card-text card-title-box-child mb-5">მაჟორიტარული სია</p></a>--}}
                                    {{--<p class="card-text card-title-box">ერთად საქართველოს წარმატებისთვის!</p>--}}
                                    {{--<div class="card-text button-yellow-date">31 ოქტომბერი, 2020</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}




        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="container mb-54 line-mobile">
    <div class="row">
        <div class="col-12 b-b"></div>
    </div>
</div>

{{--


<div class="internation-news">
    <div class="container-custom" style="margin-top:unset!important;">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-12 mobile-margin-bottom-none mb-26">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb bg-white pl-0 mb-0 p-0">
                                <li class="breadcrumb-item active d-none d-md-block" aria-current="page">
                                    <a class="href-gray" href="{{ url('world-media') }}">
                                    <svg class="mr-3 mt-0" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path></svg>
                                    @lang('site.international-media-about-Georgia')
                                    </a>
                                </li>
                            </ol>
                            <li class="breadcrumb-item active d-block d-md-none" aria-current="page">
                                <a href="{{ url('world-media') }}" class="href-gray">
                                <svg class="mr-3 mt-0" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path></svg>
                                @lang('site.international-media-about-Georgia')
                                </a>
                            </li>
                        </nav>
                    </div>

                    @foreach($media as $row)
                    <div class="col-12 mb-54">
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-7 col-xl-4 mb-4 pr-md-5">
                                @if(isset($row->Pictures->where('first_picture','1')->first()->picture))
                                    <a href="{{ url('show-world-media') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">
                                    <img  class="img-fluid" src="{{ asset('uploads/media') }}/{{ $row->Pictures->where('first_picture','1')->first()->picture }}">
                                    </a>
                                @endif
                            </div>
                            <div class="col-12 col-md-5 col-lg-5 col-xl-8">
                                <p class="date-box-title  mb-4">
                                    {{ $row->created_at->format('d') }}
                                    {{ $month[$row->created_at->format('m')] }},
                                    {{ $row->created_at->format('Y') }}
                                </p>
                                <a href="{{ url('show-world-media') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">
                                    <h1 class="internation-news-title">
                                        {{ $row->title }}
                                    </h1>
                                </a>
                                <div class=" d-none d-md-block">
                                    <a href="{{ url('show-world-media') }}/{{ $row->id }}/{{ str_replace(' ', '-', $row->title) }}">
                                <div class="internation-news-description">
                                    {!! strip_tags($row->desc) !!}
                                </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>--}}

@stop