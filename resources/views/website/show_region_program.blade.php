@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')

    <div id="myNav" class="overlay">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="overlay-content">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="custCarousel" class="carousel slide" data-ride="carousel" align="center">
                            <!-- slides -->
                            <div class="carousel-inner">
                                @foreach($news->Pictures as $key=> $row)

                                    @if($key==0)
                                        <div class="carousel-item active">
                                            <img src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}"  style="height: 500px;width: unset;" alt="gd">
                                        </div>
                                    @else
                                        <div class="carousel-item">
                                            <img src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}"  style="height: 500px;width: unset;"  alt="gd">
                                        </div>
                                    @endif
                                @endforeach




                                <div class="count">
                                    1/1
                                </div>
                                {{--  <div class="play">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="12" height="14" viewBox="0 0 12 14">
                                          <path id="Polygon_1" data-name="Polygon 1" d="M7,0l7,12H0Z" transform="translate(12) rotate(90)" fill="#fff"/>
                                      </svg>
                                  </div>--}}
                            </div>
                            <!-- Left right -->

                            <a class="carousel-control-prev" href="#custCarousel" data-slide="prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.932" height="53.865" viewBox="0 0 27.932 53.865">
                                    <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(1 1.414)">
                                        <path id="Path_227" data-name="Path 227" d="M33.018,58.536,7.5,33.018,33.018,7.5" transform="translate(-7.5 -7.5)" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                    </g>
                                </svg>

                            </a>

                            <a class="carousel-control-next" href="#custCarousel" data-slide="next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="27.932" height="53.865" viewBox="0 0 27.932 53.865">
                                    <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(1.414 1.414)">
                                        <path id="Path_227" data-name="Path 227" d="M7.5,58.536,33.018,33.018,7.5,7.5" transform="translate(-7.5 -7.5)" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                    </g>
                                </svg>

                            </a>
                            <!-- Thumbnails -->
                            <ol class="carousel-indicators list-inline">
                                @foreach($news->Pictures as $key=> $row)

                                    @if($key==0)
                                        <li class="list-inline-item active">
                                            <a id="carousel-selector-{{ $key }}" class="selected" data-slide-to="{{ $key }}" data-target="#custCarousel">
                                                <img src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    max-width: 100px;overflow: hidden;height: 70px;" class="img-fluid">
                                            </a>
                                        </li>
                                    @else
                                        <li class="list-inline-item">
                                            <a id="carousel-selector-{{ $key }}" data-slide-to="{{ $key }}" data-target="#custCarousel">
                                                <img src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    max-width: 100px;overflow: hidden;height: 70px;" class="img-fluid">
                                            </a>
                                        </li>

                                    @endif
                                @endforeach


                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @if(isset($news->youtube) && $news->youtube!='')
        <div id="video_{{ $news->id }}" class="overlay">
            <a href="javascript:void(0)" class="closebtn" onclick="closevideo({{ $news->id }})">&times;</a>
            <div class="overlay-content">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php


                            $url = $news->youtube;
                            preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);

                            $id = $matches[1];
                            $width = '60%';
                            $height = '450px'; ?>

                            <div class="player">
                                <iframe id="ytplayer" type="text/html" width="<?php echo $width ?>" height="<?php echo $height ?>"
                                        src="https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0&color=white&iv_load_policy=3"
                                        frameborder="0" allowfullscreen></iframe>
                            </div>

                        </div>


                    </div>
                </div>

            </div>
        </div>
    @endif

    <div class="archive-news mt-3">
        <div class="container-custom mb-5">
            <div class="row">
                <div class="col-12  pt-5 mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
                    <div class="row">


                        <!-- column -->
                        <div class="col-12 mb-3">
                            <div class="col-12 p-0">
                                <div class="col-12 p-0">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12 pl-0">
                                                <nav aria-label="breadcrumb">
                                                    <ol class="breadcrumb bg-white pl-0">
                                                        <a href="{{ url('region-program') }}">
                                                            <li class="breadcrumb-item_other active" aria-current="page">

                                                                <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121"><path id="Path_215" data-name="Path 215" d="M0,7.5,7.5,0,15,7.5" transform="translate(0.75 16.061) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                                                                @lang('site.back')

                                                            </li>
                                                        </a>
                                                    </ol>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->


                        <div class="col-12 col-md-7 mb-5">
                            <div class="col-12 p-0">
                                <div class="col-12 p-0">
                                    <div class="row ">
                                        <div class="col-12 description">
                                            <p class="mb-2 date-box-title b-t pt-3">
                                                {{ $news->created_at->format('d') }}
                                                {{ $month[$news->created_at->format('m')] }},
                                                {{ $news->created_at->format('Y') }}
                                            </p>
                                            <h1 class="about-title">
                                                {{ $news->title }}
                                            </h1>
                                            <div id="carouselExampleIndicators_mobile" class="d-block d-md-none carousel slide" data-ride="carousel">
                                                <ol class="carousel-indicators d-none">
                                                    @foreach($news->Pictures as $key=> $row)
                                                        @if($key==0)
                                                            <li data-target="#carouselExampleIndicators_mobile" data-slide-to="{{ $key }}" class="active"></li>
                                                        @else
                                                            <li data-target="#carouselExampleIndicators_mobile" data-slide-to="{{ $key }}"></li>
                                                        @endif
                                                    @endforeach
                                                </ol>
                                                <div class="carousel-inner">
                                                    @foreach($news->Pictures as $key=> $row)
                                                        @if($key==0)
                                                            <div class="carousel-item active">
                                                                <img class="d-block w-100" src="{{ asset('uploads/region_programe') }}/{{  $row->picture }}" alt="First slide">
                                                            </div>
                                                        @else
                                                            <div class="carousel-item">
                                                                <img class="d-block w-100" src="{{ asset('uploads/region_programe') }}/{{  $row->picture }}" alt="First slide">
                                                            </div>
                                                        @endif
                                                    @endforeach

                                                </div>

                                                @if(count($news->Pictures)>1)
                                                    <a class="carousel-control-prev" href="#carouselExampleIndicators_mobile" role="button" data-slide="prev">
                                                        <div class="show-news-icon-prev">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.414" height="16.828" viewBox="0 0 9.414 16.828">
                                                                <path id="Path_227" data-name="Path 227" d="M14.5,21.5l-7-7,7-7" transform="translate(-6.5 -6.086)" fill="none" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                            </svg>
                                                        </div>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carouselExampleIndicators_mobile" role="button" data-slide="next">
                                                        <div class="show-news-icon-next">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="9.414" height="16.828" viewBox="0 0 9.414 16.828">
                                                                <path id="Path_227" data-name="Path 227" d="M7.5,21.5l7-7-7-7" transform="translate(-6.086 -6.086)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                                            </svg>
                                                        </div>
                                                    </a>
                                                @endif
                                            </div>

                                            <div class="col-12 d-block d-md-none mb-3 p-0">
                                                <div class="col-12 p-0">
                                                    <div class="d-flex justify-content-between align-items-center b-b pt-3 pb-3">
                                                        <div class="d-flex align-items-center">
                                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ $request->fullUrl() }}">
                                                                <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                                    <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                                                        <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                                            <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                                            <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                                        </g>
                                                                        <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"></rect>
                                                                            <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"></path>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="http://twitter.com/share?text={{ $news->title }}&url={{ $request->fullUrl() }}">
                                                                <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                                    <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                                                        <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                                            <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                                            <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                                        </g>
                                                                        <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                                            <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"></rect>
                                                                            <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"></path>
                                                                        </g>
                                                                    </g>
                                                                </svg>
                                                            </a>
                                                            <a href="#"><div class="share-on" style="font-size:13px;">გააზიარე</div></a>


                                                        </div>

                                                        <div class="counts">

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div>
                                                {!! $news->desc !!}
                                            </div>
                                            @if($news->file!='')
                                                <div id="pdf" style="width:100%; height:800px;" class=" pdfobject-container">

                                                    <embed class="pdfobject" src="{{ asset('uploads/region_programe_attachment') }}/{{ $news->file }}" type="application/pdf" style="overflow: auto; width: 100%; height: 100%;">
                                                </div>
                                                <script>
                                                    var options = {
                                                        page: 1,
                                                        pdfOpenParams: {
                                                            navpanes: 1,
                                                            toolbar: 1,
                                                            statusbar: 0,
                                                            view: "FitH",
                                                            pagemode: "thumbs"
                                                        }
                                                    };




                                                    PDFObject.embed("{{ asset('uploads/region_programe_attachment') }}/{{ $news->file }}", "#pdf", options);
                                                </script>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- column -->
                        <!-- column -->
                        <div class="col-12 col-md-5 d-none d-md-block">
                            <div class="row">

                                <div class="col-12  d-flex justify-content-center mb-4" style=" display: flex;
  flex-direction: column;">


                                    @if($news->Pictures->where('first_picture','1')->first())
                                        <img class="img-fluid nav-click" onclick="openNav()" src="{{ asset('uploads/region_programe') }}/{{ $news->Pictures->where('first_picture','1')->first()->picture }}" style="max-width: 471px;">

                                    @else
                                        <img class="img-fluid nav-click" onclick="openNav()" src="{{ asset('uploads/region_programe') }}/{{ $news->Pictures->first()->picture }}" style="max-width: 471px;">

                                    @endif


                                </div>

                                <div class="my-5 text-center container">

                                    @if(count($news->Pictures)>1)
                                        <div class="row d-flex align-items-center">
                                            <div class="col-1 d-flex align-items-center justify-content-center">
                                                <a href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                    <div class="carousel-nav-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                                            <path id="Path_215" data-name="Path 215" d="M0,7.5,7.5,0,15,7.5" transform="translate(0.75 16.061) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-10">
                                                <!--Start carousel-->
                                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                                                    <div class="carousel-inner">
                                                        <div class="carousel-item active">
                                                            <div class="row">

                                                                @foreach($news->Pictures as $key=> $row)

                                                                    @if($key==0)

                                                                        <div class="col-6 ">
                                                                            <img class="img-responsive nav-click" onclick="openNav()" src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    height: 130px;">
                                                                        </div>
                                                                    @endif
                                                                    @if($key==1)
                                                                        <div  class="col-6">
                                                                            <img class="img-responsive nav-click" onclick="openNav()" src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    height: 130px;">
                                                                        </div>
                                                                    @endif

                                                                @endforeach

                                                            </div>
                                                        </div>
                                                        <div class="carousel-item">
                                                            <div class="row">
                                                                @foreach($news->Pictures as $key=> $row)
                                                                    @if($key==2)
                                                                        <div class="col-6 ">
                                                                            <img class="img-responsive nav-click" onclick="openNav()" src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    height: 130px;">
                                                                        </div>
                                                                    @endif
                                                                    @if($key==3)
                                                                        <div class="col-6 ">
                                                                            <img class="img-responsive nav-click" onclick="openNav()" src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    height: 130px;">
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>

                                                        <div class="carousel-item">
                                                            <div class="row">
                                                                @foreach($news->Pictures as $key=> $row)
                                                                    @if($key==4)
                                                                        <div class="col-6 ">
                                                                            <img class="img-responsive nav-click" onclick="openNav()" src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    height: 130px;">
                                                                        </div>
                                                                    @endif
                                                                    @if($key==5)
                                                                        <div class="col-6 ">
                                                                            <img class="img-responsive nav-click" onclick="openNav()" src="{{    asset('uploads/region_programe') }}/{{ $row->picture }}" style="    height: 130px;">
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>




                                                    </div>
                                                </div>
                                                <!--End carousel-->
                                            </div>
                                            <div class="col-1 d-flex align-items-center justify-content-center"><a  href="#carouselExampleIndicators" data-slide="next">
                                                    <div class="carousel-nav-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                                            <path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                                        </svg>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endif

                                </div>

                                {{--
                                                                <div class="col-12 d-flex justify-content-center align-items-center mb-5">
                                --}}





                                {{-- <a class="mr-4" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                <path id="Path_215" data-name="Path 215" d="M0,7.5,7.5,0,15,7.5" transform="translate(0.75 16.061) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                </svg>
                                </a>
                                 <a class="mr-4" href="#">
                                     <img class="img-responsive nav-click" onclick="openNav()" src="{{ url('website') }}/images/MaskGroup62.png" style="max-width: 192px;">
                                 </a>
                                 <a class="mr-4" href="#">
                                     <img class="img-responsive nav-click" onclick="openNav()" src="{{ url('website') }}/images/MaskGroup63.png" style="max-width: 192px;">
                                 </a>
                                 <a class="" href="#">
                                     <svg xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                         <path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                     </svg>
                                 </a>--}}
                                {{--
                                                                </div>
                                --}}


                                <div class="col-12 mb-4 d-flex justify-content-center">

                                    @if(isset($news->youtube) && $news->youtube!='')
                                        <?php

                                        $video_id = explode("?v=", $news->youtube);
                                        $video_id = $video_id[1];
                                        $thumbnail="http://img.youtube.com/vi/".$video_id."/hqdefault.jpg";

                                        ?>
                                        <div class="col-12 col-md-12 mb-4">
                                            <div class="col-12 p-0">
                                                <div class="col-12 p-0">
                                                    <a href="#">
                                                        <div class="img-fluid mb-3 play video_gallery_play_position" onclick="openvideo({{ $news->id }})">
                                                            <img class="img-fluid" src="{{ $thumbnail }}">
                                                            <div class="video_gallery_overlay">

                                                                <svg class="video_gallery_overlay_svg" xmlns="http://www.w3.org/2000/svg" width="33" height="38" viewBox="0 0 33 38">
                                                                    <path id="Polygon_1" data-name="Polygon 1" d="M17.267,3.01a2,2,0,0,1,3.466,0L36.274,30a2,2,0,0,1-1.733,3H3.459a2,2,0,0,1-1.733-3Z" transform="translate(33) rotate(90)" fill="#fff"/>
                                                                </svg>

                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    @endif
                                </div>

                                <div class="col-12 d-flex justify-content-center">
                                    <div class="d-flex align-items-center b-t pt-4" style="width:471px;">
                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ $request->fullUrl() }}">
                                            <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                                    <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                        <rect width="50" height="50" rx="6" stroke="none"/>
                                                        <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"/>
                                                    </g>
                                                    <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"/>
                                                        <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                        <a target="_blank" href="http://twitter.com/share?text={{ $news->title }}&url={{ $request->fullUrl() }}">
                                            <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                                <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                                    <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                        <rect width="50" height="50" rx="6" stroke="none"/>
                                                        <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"/>
                                                    </g>
                                                    <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"/>
                                                        <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"/>
                                                    </g>
                                                </g>
                                            </svg>
                                        </a>
                                        <a href="#"><div class="share-on">გააზიარე</div></a>
                                    </div>
                                </div>
                            </div>
                            <!-- column -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop