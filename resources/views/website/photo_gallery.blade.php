@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')




    @foreach($gallery as $row)
        <script>


            $( document ).ready(function() {
                $('.custCarousel_{{ $row->id }}').carousel({
                    interval: false
                });

                var totalItems = $('.custCarousel_{{ $row->id }} .carousel-inner .carousel-item').length;
                var currentIndex = $('.custCarousel_{{ $row->id }} .active').index() + 1;
                    $('.counts_{{ $row->id }}').html('1/'+totalItems+'');

                $('.custCarousel_{{ $row->id }}').on('slid.bs.carousel', function() {

                    currentIndex = $('.custCarousel_{{ $row->id }} .active').index() + 1;
                    $('.counts_{{ $row->id }}').html(''+currentIndex+'/'+totalItems+'');
                });
            });

        </script>
    <div id="photo_{{ $row->id }}" class="overlay" style="background-color:#fff;">
        <div class="overlay-content" style="top: 0px;">

            <div class="container-fluid p-0">
                <div class="row">
                    <div class="col-md-4" style="background-color: #F8F8F8;height: 100vh;top: 0px;">
                        <a style="        right: 20px;left:unset;" href="javascript:void(0)" class="closebtn" onclick="closephotogallery({{ $row->id }})" style="float:right;">&times;</a>
                        <h4 style="    margin-top: 20%;
    text-align: left;
    padding-left: 2rem;font-family:FiraGO-Regular;color:#27509C;">
                            {{ $row->created_at->format('d') }}
                            {{ $month[$row->created_at->format('m')] }},
                            {{ $row->created_at->format('Y') }}
                        </h4>
                        <h3 style="text-align: left;
    padding-left: 2rem;font-family:FiraGO-Regular">
                          {{ $row->title }}
                        </h3>
                        <p style="text-align: left;
    padding-left: 2rem;font-family:FiraGO-Regular;color:#646464;">
                          {!! strip_tags($row->desc) !!}
                        </p>

                       {{-- <div style="position: absolute; bottom: 130px;">
                            <div class="col-12 d-none d-md-block share" >
                                <div class="d-flex justify-content-start align-items-center">
                                    <a href="#">
                                        <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                                <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                    <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                    <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                </g>
                                                <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"></rect>
                                                    <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                    <a href="#">
                                        <svg class="mr-3" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                                <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                    <rect width="50" height="50" rx="6" stroke="none"></rect>
                                                    <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"></rect>
                                                </g>
                                                <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"></rect>
                                                    <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"></path>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                    <a href="#"><div class="share-on" style="font-size:15px;">გააზიარე</div></a>
                                </div>
                            </div>
                        </div>--}}


                    </div>
                    <div class="col-md-8" style="margin-top:8%;">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="custCarousel" class="carousel slide custCarousel_{{ $row->id }}" data-ride="carousel" align="center">
                                        <!-- slides -->
                                        <div class="carousel-inner">
                                            @foreach($row->Pictures as $key=> $r)
                                                @if($key==0)
                                                <div class="carousel-item active">
                                                    <img src="{{ url('uploads/photo_gallery') }}/{{ $r->picture }}"  alt="">
                                                </div>
                                                @else
                                                    <div class="carousel-item ">
                                                        <img src="{{ url('uploads/photo_gallery') }}/{{ $r->picture }}"  alt="">
                                                    </div>
                                                @endif
                                            @endforeach

                                            <div class="count counts_{{ $row->id }}">
                                                1/1
                                            </div>
                                            {{--  <div class="play">
                                                  <svg xmlns="http://www.w3.org/2000/svg" width="12" height="14" viewBox="0 0 12 14">
                                                      <path id="Polygon_1" data-name="Polygon 1" d="M7,0l7,12H0Z" transform="translate(12) rotate(90)" fill="#fff"/>
                                                  </svg>
                                              </div>--}}
                                        </div>
                                        <!-- Left right -->

                                        <a class="carousel-control-prev" href=".custCarousel_{{ $row->id }}" data-slide="prev">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="27.932" height="53.865" viewBox="0 0 27.932 53.865">
                                                <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(1 1.414)">
                                                    <path id="Path_227" data-name="Path 227" d="M33.018,58.536,7.5,33.018,33.018,7.5" transform="translate(-7.5 -7.5)" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                </g>
                                            </svg>

                                        </a>

                                        <a class="carousel-control-next" href=".custCarousel_{{ $row->id }}" data-slide="next">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="27.932" height="53.865" viewBox="0 0 27.932 53.865">
                                                <g id="Icon_feather-arrow-left" data-name="Icon feather-arrow-left" transform="translate(1.414 1.414)">
                                                    <path id="Path_227" data-name="Path 227" d="M7.5,58.536,33.018,33.018,7.5,7.5" transform="translate(-7.5 -7.5)" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                                </g>
                                            </svg>

                                        </a>
                                        <!-- Thumbnails -->
                                        <ol class="carousel-indicators list-inline">
                                            @foreach($row->Pictures as $key=> $r)
                                                @if($key==0)
                                                    <li class="list-inline-item active">
                                                        <a id="carousel-selector-0" class="selected" data-slide-to="{{ $key }}" data-target=".custCarousel_{{ $row->id }}">
                                                            <img src="{{ url('uploads/photo_gallery') }}/{{ $r->picture }}" class="img-fluid">
                                                        </a>
                                                    </li>
                                                    @else
                                                    <li class="list-inline-item active">
                                                        <a id="carousel-selector-0" class="selected" data-slide-to="{{ $key }}" data-target=".custCarousel_{{ $row->id }}">
                                                            <img src="{{ url('uploads/photo_gallery') }}/{{ $r->picture }}" class="img-fluid">
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @endforeach
    <div class="photo-gallery mt-3">
        <div class="container-custom">
            <div class="row">
                <div class="col-md-12 pt-5  mobile-margin-bottom-none mb-3">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                            @lang('site.photo-gallery')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 pt-5 mobile-padding-top-none  mb-3">

                    <div class="row">


                        @foreach($gallery as $row)

                        <!-- column -->
                        <div class="col-12 col-md-4 mb-4">


                            <div class="d-block d-md-none">
                                <div class="d-flex">
                                <div class="col-md-12 col-5 p-0">
                                    <a href="{{ url('show-photo-gallery') }}/{{ $row->id }}">
                                        <img class="img-fluid mb-3" src="{{ asset('uploads/photo_gallery') }}/{{ $row->Pictures->first()->picture }}">

                                    </a>
                                </div>
                                <div class="col-md-12 col-7 p-0 ml-3">
                                    <a href="{{ url('show-photo-gallery') }}/{{ $row->id }}">
                                    <h1 class="photo-title">{{ $row->title }}</h1>
                                    </a>
                                </div>
                            </div>
                            </div>

                            <div class="d-none d-md-block" onclick="openphotogallery({{ $row->id }})">
                                <div class="">
                                    <div class="col-md-12 col-5 p-0">
                                        <a href="#">
                                            <img class="img-fluid mb-3" src="{{ asset('uploads/photo_gallery') }}/{{ $row->Pictures->first()->picture }}">

                                        </a>
                                    </div>
                                    <div class="col-md-12 col-7 p-0 ml-2">
                                        <h1 class="photo-title">{{ $row->title }}</h1>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- column -->
                        @endforeach









                    </div>
                </div>
            </div>
        </div>

    </div>




@stop