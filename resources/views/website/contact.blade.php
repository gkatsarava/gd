@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')

    <div class="contact mt-3 mobile-margin-bottom-none mobile-padding-top-none margin-top-31-px">
        <div class="container-custom" >
            <div class="row">
                <div class="col-md-12 pt-5 mb-3 mobile-margin-bottom-none">
                    <div class="col-12 p-0">
                        <div class="col-12">
                            <div class="row">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb bg-white">
                                        <li class="breadcrumb-item active" aria-current="page">
                                      {{--      <svg class="mr-3 mt-2 d-none d-md-block" xmlns="http://www.w3.org/2000/svg" width="9.311" height="17.121" viewBox="0 0 9.311 17.121">
                                                <path id="Path_215" data-name="Path 215" d="M7.5,7.5,15,15l7.5-7.5" transform="translate(-6.439 23.561) rotate(-90)" fill="none" stroke="#d0d0d0" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>--}}
                                            @lang('site.contact')
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">


                    <div class="row">


                        <!-- column -->
                        <div class="col-12 col-md-6 info mb-5 ">
                            <div class="pb-5 pt-5 mobile-padding-top-none class_border">
                                <h5 class="mb-3">@lang('site.address')</h5>
                                <div class="contact-title">{{ $settings->address }}</div>
                            </div>
                            <div class="pb-5">
                                <h5 class="mb-3">@lang('site.phone')</h5>
                                <div class="contact-title">{{ $settings->phone }}</div>
                            </div>
                            <div class="mb-5 pb-5">
                                <h5 class="mb-3">@lang('site.e-mail')</h5>
                                <div class="contact-title">{{ $settings->email }}</div>
                            </div>
                            <div class="margin-bottom-30-px d-block d-md-none">
                                <p>@lang('site.georgian-dream-in-socmedia')</p>
                            </div>
                            <div class="margin-bottom-40-px share d-block d-md-none">
                                <div class="share d-flex justify-content-start">
                                    <a class="mr-3" href="{{ $settings->facebook or '' }}" target="_blank" >
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_808" data-name="Group 808" transform="translate(-356 -793)">
                                                <rect id="Rectangle_893" data-name="Rectangle 893" width="50" height="50" rx="25" transform="translate(356 793)" fill="#0d8cf1"/>
                                                <g id="Group_762" data-name="Group 762" transform="translate(-628 -2038)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                    <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#fff"/>
                                                </g>
                                            </g>
                                        </svg>



                                    </a>
                                    <a class="mr-3" href="{{ $settings->twitter or '' }}" target="_blank">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_805" data-name="Group 805" transform="translate(-566 -793)">
                                                <rect id="Rectangle_947" data-name="Rectangle 947" width="50" height="50" rx="25" transform="translate(566 793)" fill="#1fa2f2"/>
                                                <g id="Group_764" data-name="Group 764" transform="translate(-418 -2038)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                    <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#fff"/>
                                                </g>
                                            </g>
                                        </svg>



                                    </a>
                                    <a href="{{ $settings->youtube or '' }}" target="_blank" >
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                                            <g id="Group_806" data-name="Group 806" transform="translate(-496 -793)">
                                                <rect id="Rectangle_946" data-name="Rectangle 946" width="50" height="50" rx="25" transform="translate(496 793)" fill="red"/>
                                                <g id="Group_763" data-name="Group 763" transform="translate(-488 -2038)">
                                                    <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                    <path id="Icon_awesome-youtube" data-name="Icon awesome-youtube" d="M19.25,6.545a2.336,2.336,0,0,0-1.643-1.654A55.2,55.2,0,0,0,10.344,4.5a55.2,55.2,0,0,0-7.263.391A2.336,2.336,0,0,0,1.438,6.545a24.5,24.5,0,0,0-.388,4.5,24.5,24.5,0,0,0,.388,4.5,2.3,2.3,0,0,0,1.643,1.628,55.2,55.2,0,0,0,7.263.391,55.2,55.2,0,0,0,7.263-.391,2.3,2.3,0,0,0,1.643-1.628,24.5,24.5,0,0,0,.388-4.5,24.5,24.5,0,0,0-.388-4.5ZM8.443,13.812V8.284L13.3,11.048,8.443,13.812Z" transform="translate(998.695 2844.965)" fill="#fff"/>
                                                </g>
                                            </g>
                                        </svg>



                                    </a>
                                </div>
                            </div>
                            <div class="d-none d-md-block">
                            <div class="mb-5 pb-5  d-flex justify-content-start">
                                <a href="{{ url('contact_send') }}" >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="93" viewBox="0 0 327 93">
                                        <g id="Group_1051" data-name="Group 1051" transform="translate(-280 -702)">
                                            <rect id="Rectangle_1059" data-name="Rectangle 1059" width="327" height="93" rx="10" transform="translate(280 702)" fill="#4d81e2"/>
                                            <text id="წერილის_გაგზავნა" data-name="წერილის გაგზავნა" transform="translate(320 756)" fill="#fff" font-size="20" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.send-massage')</tspan></text>
                                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(565.79 749.205) rotate(-45)">
                                                <path id="Path_211" data-name="Path 211" d="M10.5,10.5,21.614,21.614" transform="translate(-13.957 -13.957)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                                <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                            </div>


                            <div class="d-block d-md-none">
                            <div class="mb-5 pb-5  d-flex justify-content-start">
                                <a href="{{ url('contact_send') }}"  class="w-100">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="93" viewBox="0 0 327 93">
                                        <g id="Group_1051" data-name="Group 1051" transform="translate(-280 -702)">
                                            <rect id="Rectangle_1059" data-name="Rectangle 1059" width="327" height="93" rx="10" transform="translate(280 702)" fill="#4d81e2"/>
                                            <text id="წერილის_გაგზავნა" data-name="წერილის გაგზავნა" transform="translate(320 756)" fill="#fff" font-size="20" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">@lang('site.send-massage')</tspan></text>
                                            <g id="Icon_feather-arrow-down-right" data-name="Icon feather-arrow-down-right" transform="translate(565.79 749.205) rotate(-45)">
                                                <path id="Path_211" data-name="Path 211" d="M10.5,10.5,21.614,21.614" transform="translate(-13.957 -13.957)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                                <path id="Path_212" data-name="Path 212" d="M18.157,10.5v7.657H10.5" transform="translate(-10.5 -10.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                            </div>



                            <div class="mb-3 d-none d-md-block">
                                <p>@lang('site.georgian-dream-in-socmedia')</p>
                            </div>
                            <div class="share d-none d-md-block">
                                <div class="d-flex justify-content-start">
                                    <a href="{{ $settings->facebook or '' }}" target="_blank" class="mr-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="134" height="50" viewBox="0 0 134 50">
                                            <g id="Group_1055" data-name="Group 1055" transform="translate(-280 -849)">
                                                <text id="Facebook" transform="translate(345 879)" fill="#b2b2b2" font-size="16" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">Facebook</tspan></text>
                                                <g id="Group_808" data-name="Group 808" transform="translate(-76 56)">
                                                    <rect id="Rectangle_893" data-name="Rectangle 893" width="50" height="50" rx="25" transform="translate(356 793)" fill="#0d8cf1"/>
                                                    <g id="Group_762" data-name="Group 762" transform="translate(-628 -2038)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                        <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#fff"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>

                                    </a>
                                    <a href="{{ $settings->twitter or '' }}" target="_blank" class="mr-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="115" height="50" viewBox="0 0 115 50">
                                            <g id="Group_1052" data-name="Group 1052" transform="translate(-443 -848)">
                                                <g id="Group_805" data-name="Group 805" transform="translate(-123 55)">
                                                    <rect id="Rectangle_947" data-name="Rectangle 947" width="50" height="50" rx="25" transform="translate(566 793)" fill="#1fa2f2"/>
                                                    <g id="Group_764" data-name="Group 764" transform="translate(-418 -2038)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                        <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#fff"/>
                                                    </g>
                                                </g>
                                                <text id="Twitter" transform="translate(508 879)" fill="#b2b2b2" font-size="16" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">Twitter</tspan></text>
                                            </g>
                                        </svg>

                                    </a>
                                    <a href="{{ $settings->youtube or '' }}" target="_blank" >
                                        <svg xmlns="http://www.w3.org/2000/svg" width="125" height="50" viewBox="0 0 125 50">
                                            <g id="Group_1053" data-name="Group 1053" transform="translate(-588 -849)">
                                                <g id="Group_806" data-name="Group 806" transform="translate(92 56)">
                                                    <rect id="Rectangle_946" data-name="Rectangle 946" width="50" height="50" rx="25" transform="translate(496 793)" fill="red"/>
                                                    <g id="Group_763" data-name="Group 763" transform="translate(-488 -2038)">
                                                        <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#d1d1d1" opacity="0"/>
                                                        <path id="Icon_awesome-youtube" data-name="Icon awesome-youtube" d="M19.25,6.545a2.336,2.336,0,0,0-1.643-1.654A55.2,55.2,0,0,0,10.344,4.5a55.2,55.2,0,0,0-7.263.391A2.336,2.336,0,0,0,1.438,6.545a24.5,24.5,0,0,0-.388,4.5,24.5,24.5,0,0,0,.388,4.5,2.3,2.3,0,0,0,1.643,1.628,55.2,55.2,0,0,0,7.263.391,55.2,55.2,0,0,0,7.263-.391,2.3,2.3,0,0,0,1.643-1.628,24.5,24.5,0,0,0,.388-4.5,24.5,24.5,0,0,0-.388-4.5ZM8.443,13.812V8.284L13.3,11.048,8.443,13.812Z" transform="translate(998.695 2844.965)" fill="#fff"/>
                                                    </g>
                                                </g>
                                                <text id="Youtube" transform="translate(653 880)" fill="#b2b2b2" font-size="16" font-family="FiraGO-Regular, FiraGO"><tspan x="0" y="0">Youtube</tspan></text>
                                            </g>
                                        </svg>

                                    </a>
                                </div>
                            </div>

                        </div>
                        <!-- column -->

                        <!-- column -->
                        <div class="col-12 col-md-6">
                            <div class="map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d591.1587030235119!2d44.80668977907066!3d41.693470825538064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40440cfaac03352d%3A0xbc964438f25fc521!2sGeorgian%20Dream!5e0!3m2!1sen!2sge!4v1603094185739!5m2!1sen!2sge" width="100%" height="555" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>                            </div>
                        </div>
                        <!-- column -->


                    </div>

                </div>
            </div>
        </div>
    </div>


@stop