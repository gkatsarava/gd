@inject('request', 'Illuminate\Http\Request')
@extends('website.layouts.app')

@section('content')


    <div id="photo" class="overlay" style="    background-color: rgb(255 255 255 / 95%);">
        <a style="right: 20px;left:unset;top: 18px;" href="javascript:void(0)" class="closebtn" onclick="closemobilephotogallery()">×</a>
        <div class="overlay-content" style="    top: 7.9%;">


            <div class="container-fluid p-0 gallery" style="    margin-top: 48px;">


                <div id="carouselExampleIndicators_mobile" class="d-block d-md-none carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators d-none">
                        @foreach($gallery->Pictures as $key=> $row)
                            @if($key==0)
                                <li data-target="#carouselExampleIndicators_mobile" data-slide-to="{{ $key }}" class="active"></li>
                            @else
                                <li data-target="#carouselExampleIndicators_mobile" data-slide-to="{{ $key }}"></li>
                            @endif
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach($gallery->Pictures as $key=> $row)
                            @if($key==0)
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="{{ asset('uploads/photo_gallery') }}/{{  $row->picture }}" alt="First slide">
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{ asset('uploads/photo_gallery') }}/{{  $row->picture }}" alt="First slide">
                                </div>
                            @endif
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators_mobile" role="button" data-slide="prev">
                        <div class="show-news-icon-prev">
                            <svg style="    margin-top: -22px;" xmlns="http://www.w3.org/2000/svg" width="9.414" height="16.828" viewBox="0 0 9.414 16.828">
                                <path id="Path_227" data-name="Path 227" d="M14.5,21.5l-7-7,7-7" transform="translate(-6.5 -6.086)" fill="none" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                            </svg>
                        </div>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators_mobile" role="button" data-slide="next">
                        <div class="show-news-icon-next">
                            <svg style="    margin-top: -22px;" xmlns="http://www.w3.org/2000/svg" width="9.414" height="16.828" viewBox="0 0 9.414 16.828">
                                <path id="Path_227" data-name="Path 227" d="M7.5,21.5l7-7-7-7" transform="translate(-6.086 -6.086)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                            </svg>
                        </div>
                    </a>
                </div>


                <div class="col-12 text-left mt-3 font-family-firago-regular margin-top-30-px" style="font-size:16px;">
                    {!! strip_tags($gallery->desc) !!}
                </div>


            </div>

        </div>
    </div>




    <div class="show-photo-gallery">
        <div class="container-custom" style="max-width: 1470px;">
            <div class="row">
                <div class="col-12">

                    <div class="row">
                        <!-- column -->
                        <div class="col-12 pb-30">
                            <div class="row">
                                <div class="col-12">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb bg-white pl-0">
                                            <li class="breadcrumb-item_other active" aria-current="page">
                                                <a class="href-gray" href="{{ url('photo-gallery') }}">
                                                <svg class="mr-2" xmlns="http://www.w3.org/2000/svg" width="6.231" height="11.461" viewBox="0 0 6.231 11.461">
                                                    <g id="Icon_feather-arrow-up" data-name="Icon feather-arrow-up" transform="translate(0.5 10.754) rotate(-90)">
                                                        <path id="Path_71" data-name="Path 71" d="M0,5.023,5.023,0l5.023,5.023" fill="none" stroke="#b2b2b2" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                                                    </g>
                                                </svg>
                                                უკან
                                                </a>
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <!-- column -->

                        <!-- column -->
                        <div class="col-12 mb-4">
                            <h1 class="photo-title">{{ $gallery->title }}</h1>
                        </div>
                        <!-- column -->

                        @foreach($gallery->Pictures as $row)
                        <!-- column -->
                        <div class="col-6 mb-4">
                            <img class="img-fluid mb-3" onclick="openmobilephotogallery()" src="{{ asset('uploads/photo_gallery') }}/{{ $row->picture }}">
                        </div>
                        <!-- column -->
                        @endforeach



                        <!-- share zone -->
                        <div class="col-12 pt-4 b-t">
                            <div class="d-flex justify-content-center align-items-center">
                                <a href="#">
                                    <svg class="mr-28" xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 50 50">
                                        <g id="Group_988" data-name="Group 988" transform="translate(-705 -934.999)">
                                            <g id="Rectangle_897" data-name="Rectangle 897" transform="translate(705 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                <rect width="50" height="50" rx="6" stroke="none"/>
                                                <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"/>
                                            </g>
                                            <g id="Group_985" data-name="Group 985" transform="translate(-279 -1896)">
                                                <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#0d8cf1" opacity="0"/>
                                                <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M18.057,9.31A8.747,8.747,0,1,0,7.943,17.951V11.838H5.721V9.31H7.943V7.382a3.086,3.086,0,0,1,3.3-3.4A13.462,13.462,0,0,1,13.2,4.15V6.3H12.1a1.264,1.264,0,0,0-1.425,1.366V9.31H13.1l-.388,2.529H10.676v6.113A8.75,8.75,0,0,0,18.057,9.31Z" transform="translate(999.662 2846.752)" fill="#0d8cf1"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 50 50">
                                        <g id="Group_987" data-name="Group 987" transform="translate(-775 -934.999)">
                                            <g id="Rectangle_899" data-name="Rectangle 899" transform="translate(775 934.999)" fill="#fff" stroke="#ebebeb" stroke-width="1">
                                                <rect width="50" height="50" rx="6" stroke="none"/>
                                                <rect x="0.5" y="0.5" width="49" height="49" rx="5.5" fill="none"/>
                                            </g>
                                            <g id="Group_986" data-name="Group 986" transform="translate(-209 -1896)">
                                                <rect id="Rectangle_892" data-name="Rectangle 892" width="22" height="22" transform="translate(998 2845)" fill="#1fa2f2" opacity="0"/>
                                                <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M15.733,6.93c.011.156.011.312.011.467A10.155,10.155,0,0,1,5.519,17.623,10.156,10.156,0,0,1,0,16.009a7.435,7.435,0,0,0,.868.044A7.2,7.2,0,0,0,5.33,14.519a3.6,3.6,0,0,1-3.36-2.492,4.532,4.532,0,0,0,.679.056,3.8,3.8,0,0,0,.946-.122A3.594,3.594,0,0,1,.712,8.432V8.388a3.619,3.619,0,0,0,1.624.456A3.6,3.6,0,0,1,1.224,4.037,10.216,10.216,0,0,0,8.634,7.8a4.057,4.057,0,0,1-.089-.823,3.6,3.6,0,0,1,6.22-2.459,7.076,7.076,0,0,0,2.281-.868,3.584,3.584,0,0,1-1.58,1.981,7.2,7.2,0,0,0,2.07-.556,7.725,7.725,0,0,1-1.8,1.858Z" transform="translate(1000.232 2845.498)" fill="#1fa2f2"/>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <!-- share zone -->


                    </div>
                </div>
            </div>
        </div>
    </div>




@stop