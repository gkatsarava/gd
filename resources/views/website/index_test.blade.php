
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.1/assets/img/favicons/favicon.ico">

    <title>Fixed top navbar example for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/navbar-fixed/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.1/examples/navbar-fixed/navbar-top-fixed.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('website') }}/assets/css/style.css">

</head>

<body>

<!-- NAVBAR
================================================== -->
<nav class="navbar navbar-expand-lg navbar-light bg-blue">
    <div class="container">

        <!-- Brand -->
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ url('website') }}/images/Logo%20Home.png" class="navbar-brand-img" alt="logo">
        </a>

        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbarCollapse">

            <!-- Toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fe fe-x"></i>
            </button>

            <!-- Navigation -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item mr-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarLandings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        პარტია
                        <svg class="ml-1" xmlns="http://www.w3.org/2000/svg" width="8.004" height="4.502" viewBox="0 0 8.004 4.502">
                            <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(-6.793 -6.793)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarAccount">
                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                პარტიის შესახებ
                            </a>
                        </li>

                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                პარტიის სტრუქტურა
                            </a>
                        </li>


                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                პოლიტიკური საბჭო
                            </a>
                        </li>


                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1 mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                წესდება
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item mr-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarLandings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        ახალი ამბები
                    </a>
                </li>

                <li class="nav-item mr-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarLandings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        არჩევნები 2020
                        <svg class="ml-1" xmlns="http://www.w3.org/2000/svg" width="8.004" height="4.502" viewBox="0 0 8.004 4.502">
                            <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(-6.793 -6.793)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarAccount">
                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                კანდიდატები
                            </a>
                        </li>

                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                პროგრამა
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item mr-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarLandings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        საერთაშორისო მედია
                    </a>
                </li>

                <li class="nav-item mr-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarLandings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        მედია
                        <svg class="ml-1" xmlns="http://www.w3.org/2000/svg" width="8.004" height="4.502" viewBox="0 0 8.004 4.502">
                            <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(-6.793 -6.793)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarAccount">
                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                ვიდეო გალერეა
                            </a>
                        </li>

                        <li class="dropdown-item dropright">
                            <a class="dropdown-link dropdown-toggle" data-toggle="dropdown" href="#">
                                <svg class="mb-1  mt-2 mr-1" xmlns="http://www.w3.org/2000/svg" width="8.224" height="8.004" viewBox="0 0 8.224 8.004">
                                    <g id="Group_748" data-name="Group 748" transform="translate(-397.219 -169.851)">
                                        <path id="Path_71" data-name="Path 71" d="M7.5,7.5l3.295,3.295L14.09,7.5" transform="translate(394.147 184.647) rotate(-90)" fill="none" stroke="#626262" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                                        <path id="Path_74" data-name="Path 74" d="M-18143.281-21656.146h6.963" transform="translate(18541 21830)" fill="none" stroke="#626262" stroke-linecap="round" stroke-width="1"></path>
                                    </g>
                                </svg>
                                ფოტო გალერეა
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item mr-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarLandings" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                        კონტაქტი
                    </a>
                </li>

            </ul>

            <!-- Button -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item d-none d-lg-block mr-76">
                    <form class="custom-form d-flex">
                        <input class="search-text" type="search" placeholder="ძებნა">
                        <a class="search-btn mt-3" href="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="17.958" height="17.958" viewBox="0 0 17.958 17.958">
                                <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(-4.219 -4.219)">
                                    <path id="Path_72" data-name="Path 72" d="M17.892,11.2a6.7,6.7,0,1,1-6.7-6.7,6.7,6.7,0,0,1,6.7,6.7Z" transform="translate(0.719 0.719)" fill="none" stroke="#fff" stroke-linecap="square" stroke-linejoin="bevel" stroke-width="2"></path>
                                    <path id="Path_73" data-name="Path 73" d="M28.346,28.346l-3.371-3.371" transform="translate(-7.583 -7.583)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></path>
                                </g></svg>
                        </a>
                    </form>
                </li>
                <li class="nav-item mr-item" style="margin-top: 9px;"> <a class="nav-link" href="#" style="color: #4F70D5"> EN </a>
                </li>
            </ul>

        </div>

    </div>
</nav>




<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
<script src="https://getbootstrap.com/docs/4.1/dist/js/bootstrap.min.js"></script>
</body>
</html>
