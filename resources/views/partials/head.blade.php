<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="{{ url('admin') }}/assets/images/favicon.png">
<title>GD - ADMIN PANEL</title>
<!-- Bootstrap Core CSS -->
<link href="{{ url('admin') }}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Morries chart CSS -->
<link href="{{ url('admin') }}/assets/plugins/morrisjs/morris.css" rel="stylesheet">


<!-- Custom CSS -->
<link href="{{ url('admin') }}/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="{{ url('admin') }}/css/colors/blue.css" id="theme" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->