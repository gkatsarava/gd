<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{ url('admin') }}/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="{{ url('admin') }}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ url('admin') }}/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="{{ url('admin') }}/js/waves.js"></script>
<!--Menu sidebar -->
<script src="{{ url('admin') }}/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="{{ url('admin') }}/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="{{ url('admin') }}/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!--Custom JavaScript -->
<script src="{{ url('admin') }}/js/custom.min.js"></script>



<script src="{{ url('admin') }}/assets/plugins/tinymce/tinymce.min.js"></script>


<script src="{{ url('admin') }}/assets/plugins/dropzone-master/dist/dropzone.js"></script>
<link rel="stylesheet" href="{{ url('admin') }}/assets/plugins/dropzone-master/dist/dropzone.css">
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{{ url('admin') }}/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>





<script>





    $(document).ready(function() {


        $( ".table-sortable tbody" ).sortable({
            axis: "y",
            placeholder: "ui-state-highlight",
            helper: 'clone',
            update: function(event, ui) {
                /// console.log($("#accordion > div").sortable('toArray'));
                //var sorted = $( this ).sortable( "serialize" );
                /* console.log($(this).sortable('serialize'));
                 console.log($(this).sortable( "toArray" ));*/

                console.log($(this).sortable('serialize'));

                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/political_party/Sort') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: $(this).sortable('serialize'),
                    success: function(data) {
                        //var jsondata=data;
                        //createJSTree(data);
                    },
                });


                //console.log(sorted);


                //  console.log('update: '+ui.item.index()+'data id'+ui.item.attr('data-id'));
                //console.log($('#accordion').sortable('toArray'));
            },
        });

        $( function() {
            $( "#datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
        } );


        Dropzone.autoDiscover = false;
        $("#my-news-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/news/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image[]" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name[]" value="' + response.real_name + '">');
                    $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');

                });
            }

        });

        $("#my-news-attachment").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/news/uploadNewFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file" value="' + response.success + '">');
/*
                    $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
*/

                });
            }

        });

        $("#my-region-programe-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/region_programe/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image[]" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name[]" value="' + response.real_name + '">');
                    $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');

                });
            }

        });

        $("#my-region-programe-attachment").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/region_programe/uploadNewFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file" value="' + response.success + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });



        $("#my-bulletin-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/bulletin/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image[]" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name[]" value="' + response.real_name + '">');
                    $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');

                });
            }

        });

        $("#my-bulletin-attachment").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/bulletin/uploadNewFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file" value="' + response.success + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });




        $("#my-programs-file-ge-up").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/programs/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file_ge" value="' + response.success + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });

        $("#my-programs-file-en-up").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/programs/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file_en" value="' + response.success + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });




        $("#my-transfers-pdf-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/transfers/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');
/*
                    $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
*/

                });
            }

        });


        $("#my-young-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/young/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });


        $("#my-gallery-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 500000, // MB
            maxFiles: 50,
            url: "{{ url('admin/photo_gallery/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image[]" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name[]" value="' + response.real_name + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });

        $("#my-transfers-pdf-icon-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/transfers/uploadIconFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');
                    /*
                                        $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');
                    */

                });
            }

        });





        $("#my-political-party-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/political_party/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');

                });
            }

        });


        $("#my-majoritarian-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/majoritarians/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');

                });
            }

        });

        $("#my-mayor-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/mayor/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');

                });
            }

        });

        $("#my-proportional-list-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/proportional_list/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="' + response.real_name + '">');

                });
            }

        });






        $("#my-media-files").dropzone({
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/media/uploadFiles') }}",
            init: function () {
                this.on("success", function (file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image[]" value="' + response.success + '">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name[]" value="' + response.real_name + '">');
                    $(file.previewTemplate).append('<label>მთავარი სურათი </label> <input type="radio" style="position:relative;opacity:1;left:0px;" name="checkedfirst" value="'+response.success+'">');

                });
            }

        });


        $("#my-slider-files").dropzone({
            addRemoveLinks : true,
            headers: {
                'X-CSRF-TOKEN':  $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/slider/uploadFiles') }}",
            init: function() {
                this.on("success", function(file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="image" value="'+response.success+'">');
                    $(file.previewTemplate).append('<input type="hidden" name="image_real_name" value="'+response.real_name+'">');
                });
            }


        });


        $("#my-about-files").dropzone({
            addRemoveLinks : true,
            headers: {
                'X-CSRF-TOKEN':  $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/about/uploadFiles') }}",
            init: function() {
                this.on("success", function(file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file" value="'+response.success+'">');
                    $(file.previewTemplate).append('<input type="hidden" name="file_real_name" value="'+response.real_name+'">');
                });
            }


        });



        $("#my-rules-files").dropzone({
            addRemoveLinks : true,
            headers: {
                'X-CSRF-TOKEN':  $("meta[name='csrf-token']").attr('content')
            },
            maxFilesize: 50000, // MB
            maxFiles: 50,
            url: "{{ url('admin/rules/uploadFiles') }}",
            init: function() {
                this.on("success", function(file, response) {

                    // $(file.previewTemplate).append('<a href="#">Default image</a>');
                    $(file.previewTemplate).append('<input type="hidden" name="file" value="'+response.success+'">');
                    $(file.previewTemplate).append('<input type="hidden" name="file_real_name" value="'+response.real_name+'">');
                });
            }


        });



        $( ".pin_slider" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ აპინვა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var status=$(this).attr('y_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/news/pin_to_slide') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,
                        status:status


                    },
                    success: function(data) {
                       // self.parent().remove();
                         location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });
        $( ".delete-political-party-picture" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/political_party/delete_politican_party_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".delete-file-photo-gallery" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/photo_gallery/delete_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".delete-majoritarians-picture" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/majoritarians/delete_majoritarians_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });
        $( ".delete-mayor-picture" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/mayor/delete_mayor_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });



        $( ".pause-news" ).click(function() {
            var r = confirm("დარწმუნებული ხართ?");
            if (r == true) {


                var self=$(this);
                var id=$(this).attr('x_id');
                var status=$(this).attr('y_id');
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/news/pause-news') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,
                        status:status


                    },
                    success: function(data) {
                        //self.parent().remove();
                         location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".pause-region-programe" ).click(function() {
            var r = confirm("დარწმუნებული ხართ?");
            if (r == true) {


                var self=$(this);
                var id=$(this).attr('x_id');
                var status=$(this).attr('y_id');
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/region_programe/pause-news') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,
                        status:status


                    },
                    success: function(data) {
                        //self.parent().remove();
                        location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".pause-bulletin" ).click(function() {
            var r = confirm("დარწმუნებული ხართ?");
            if (r == true) {


                var self=$(this);
                var id=$(this).attr('x_id');
                var status=$(this).attr('y_id');
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/bulletin/pause-news') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,
                        status:status


                    },
                    success: function(data) {
                        //self.parent().remove();
                        location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".delete-proportional-list-picture" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/proportional_list/delete_proportional_list_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });




        $( ".delete-file-news" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

            var id=$(this).attr('x_id');
            var self=$(this);
            $.ajax({
                type: "POST",
                url: '{{ url('admin/news/delete_picture') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,


                },
                success: function(data) {
                    self.parent().remove();
                    // location.reload();
                }
            }).done(function (response) {




            });
            } else {

            }
        });
        $( ".delete-region-programe-file-news" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/region_programe/delete_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });



        $( ".delete-file-bulletin" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/bulletin/delete_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".delete-attachment-news" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/news/delete_attachment') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".delete-attachment-region-programe" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/region_programe/delete_attachment') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

     



        $( ".delete-file-transfers" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/transfers/delete_file') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".delete-file-transfers-icon" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/transfers/delete_icon') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });





        $( ".delete-file-media" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/media/delete_picture') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        self.parent().remove();
                        // location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });



/*
        $( ".btn-add-position" ).click(function() {

            var other_position_ge=$('.other_position_ge').val();
            var other_position_en=$('.other_position_en').val();

            if(other_position_ge.length>0){
            $('.append_positions').append('<tr>' +
                '<td>' +
                '<input type="text" class="form-control"  name="other_position[][ge][]" value="'+other_position_ge+'">' +
                '</td>' +
                '<td>' +
                '<input type="text" class="form-control"  name="other_position[][en][]" value="'+other_position_en+'">' +
                '</td>' +
                '<td>' +
                '<div class="btn btn-danger delete-other-position">წაშლა</div>' +
                '</td>' +
                '</tr>');

            $(".other_position_ge").val('');
            $(".other_position_en").val('');
            }
        });*/



       /* $(document).on('click','.delete-other-position',function(){

            $(this).parent().parent().remove();
        });
*/

        $( ".save-other-position" ).click(function() {

            var self=$(this);
            var id=$(self).attr('x_id');
            var position_ge=$('.position_ge').val();
            var position_en=$('.position_en').val();

            $.ajax({
                type: "POST",
                url: '{{ url('admin/political_party/save_other_position') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,
                    name_ge:position_ge,
                    name_en:position_en


                },
                success: function(data) {
                    //self.parent().remove();
                    location.reload();
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function( index, value ) {

                        $("."+index+"_label").css({'color':'red'});
                        //$(".error").append('<div class="alert alert-danger ">'+value+'</div>');

                    });

                }
            }).done(function (response) {




            });


        });



        $( ".save-district" ).click(function() {

            var self=$(this);
            var id=$(self).attr('x_id');
            var name_ge=$('.name_ge').val();
            var name_en=$('.name_en').val();

            $.ajax({
                type: "POST",
                url: '{{ url('admin/region/save_district') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,
                    name_ge:name_ge,
                    name_en:name_en


                },
                success: function(data) {
                    //self.parent().remove();
                    location.reload();
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function( index, value ) {

                        $("."+index+"_label").css({'color':'red'});
                        //$(".error").append('<div class="alert alert-danger ">'+value+'</div>');

                    });

                }
            }).done(function (response) {




            });


        });


        $( ".edit-other-position" ).click(function() {

            var self=$(this);
            var id=$(self).attr('x_id');


            $.ajax({
                type: "POST",
                url: '{{ url('admin/political_party/edit_other_position') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,


                },
                success: function(data) {
                    //self.parent().remove();

                    document.documentElement.scrollTop = 0;
                    $('.position_ge').val(data.name_ge);
                    $('.position_en').val(data.name_en);
                    $('.save-other-position').hide();
                    $('.update-other-position').removeClass('d-none').addClass('d-block');
                    $('.update-other-position').attr('x_id',id);

                }
            }).done(function (response) {




            });


        });

        $( ".edit-district" ).click(function() {

            var self=$(this);
            var id=$(self).attr('x_id');


            $.ajax({
                type: "POST",
                url: '{{ url('admin/region/edit_district') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,


                },
                success: function(data) {
                    //self.parent().remove();

                    document.documentElement.scrollTop = 0;
                    $('.name_ge').val(data.name_ge);
                    $('.name_en').val(data.name_en);
                    $('.save-district').hide();
                    $('.update-district').removeClass('d-none').addClass('d-block');
                    $('.update-district').attr('x_id',id);

                }
            }).done(function (response) {




            });


        });


        $(document).on('click','.update-other-position',function(){



            var self=$(this);
            var id=$(self).attr('x_id');
            var position_ge=$('.position_ge').val();
            var position_en=$('.position_en').val();

            $.ajax({
                type: "POST",
                url: '{{ url('admin/political_party/update_other_position') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,
                    name_ge:position_ge,
                    name_en:position_en


                },
                success: function(data) {
                    //self.parent().remove();
                    location.reload();
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function( index, value ) {

                        $("."+index+"_label").css({'color':'red'});
                        //$(".error").append('<div class="alert alert-danger ">'+value+'</div>');

                    });

                }
            }).done(function (response) {




            });


        });

        $(document).on('click','.update-district',function(){



            var self=$(this);
            var id=$(self).attr('x_id');
            var name_ge=$('.name_ge').val();
            var name_en=$('.name_en').val();

            $.ajax({
                type: "POST",
                url: '{{ url('admin/region/update_district') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,
                    name_ge:name_ge,
                    name_en:name_en


                },
                success: function(data) {
                    //self.parent().remove();
                    location.reload();
                },
                error: function (data) {
                    $.each(data.responseJSON.errors, function( index, value ) {

                        $("."+index+"_label").css({'color':'red'});
                        //$(".error").append('<div class="alert alert-danger ">'+value+'</div>');

                    });

                }
            }).done(function (response) {




            });


        });


        $(document).on('click','.delete-other-position',function(){
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/political_party/delete_other_position') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                        $(self).parent().parent().remove();
                    }
                }).done(function (response) {




                });

            }else{

            }
        });


        $(document).on('click','.delete-district',function(){
            var r = confirm("დარწმუნებული ხართ რომ გსურთ წაშლა?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/region/delete_district') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                        $(self).parent().parent().remove();
                    }
                }).done(function (response) {




                });

            }else{

            }
        });





        $( ".pin_home" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ მთავარზე ფოტოზე განთავსება?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/news/pin_home') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                         location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".pin_home_region_programe" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ მთავარზე ფოტოზე განთავსება?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/region_programe/pin_home') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                        location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".pin_home_media" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ მთავარზე ფოტოზე განთავსება?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/media/pin_home') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                        location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });


        $( ".pin_bulletin_home" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ მთავარზე ფოტოზე განთავსება?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/bulletin/pin_home') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                        location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });

        $( ".pin_bulletin_media" ).click(function() {
            var r = confirm("დარწმუნებული ხართ რომ გსურთ მთავარზე ფოტოზე განთავსება?");
            if (r == true) {

                var id=$(this).attr('x_id');
                var self=$(this);
                $.ajax({
                    type: "POST",
                    url: '{{ url('admin/bulletin/pin_home') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    },
                    data: {
                        id:id,


                    },
                    success: function(data) {
                        //self.parent().remove();
                        location.reload();
                    }
                }).done(function (response) {




                });
            } else {

            }
        });



        $( ".region" ).on( "change", function() {
            var id=$(this).val();
            $.ajax({
                type: "POST",
                url: '{{ url('admin/region/get_districts') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },
                data: {
                    id:id,


                },
                success: function(data) {
                    //self.parent().remove();
                    if(data){
                        $(".district").html('');
                        $(".district").append('<option value="">აირჩიეთ რაიონი</option>');
                        $('.district_block').removeClass('d-none').addClass('d-block');
                        $.each(data, function( index, value ) {


                           $(".district").append('<option value='+value.id+'>'+value.name_ge+'</option>');

                        });
                    }
                }
            }).done(function (response) {




            });

        });




        if ($("#mymce").length > 0) {
            tinymce.init({
                selector: "textarea#mymce",
                theme: "modern",
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

            });
        }

    });

</script>
</body>

</html>



@yield('javascript')