<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/admin/home') }}">
                <!-- Logo icon --><b>

                </b>
                <!--End Logo icon -->
                <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="{{ url('/') }}/logo_home.png" alt="homepage" style="height: 55px;" class="dark-logo" />
                    <!-- Light Logo text -->
                         <img src="{{ url('/') }}/logo_home.png" class="light-logo" style="height: 55px;" alt="homepage" /></span> </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->



                <!-- ============================================================== -->
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ url('admin') }}/assets/images/users/1.jpg" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{ url('admin') }}/assets/images/users/1.jpg" alt="user"></div>
                                    <div class="u-text">
                                        <h4>{{ Auth::user()->name }}</h4>
                                        <p class="text-muted">{{ Auth::user()->email }}</p>
                                    </div>
                                </div>
                            </li>

                            <li role="separator" class="divider"></li>
                            <li>

                                <a href="#logout" onclick="$('#logout').submit();">
                                    <i class="fa fa-power-off"></i>
                                    <span class="title">Logout</span>
                                </a>
                               
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">PERSONAL</li>

                @can('users_manage')
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="mdi mdi-bullseye"></i>
                        <span class="hide-menu">
                            მომხმარებლები
                        </span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.permissions.index') }}">უფლებები</a></li>
                        <li><a href="{{ route('admin.roles.index') }}">როლები</a></li>
                        <li><a href="{{ route('admin.users.index') }}">მომხმარებლები</a></li>
                    </ul>
                </li>
                @endcan

                @can('news_manage')

                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">
                            სიახლებიის მართვა
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ url('/admin/news') }}">სიახლეები</a></li>
                            <li><a href="{{ url('/admin/region_programe') }}"> რეგიონების პროგრამა</a></li>
                            <li><a href="{{ url('/admin/bulletin') }}">ბლოგი</a></li>
                        </ul>
                    </li>
                    
                @endcan


                @can('media_manage')
                <li>

                    <a class="has-arrow waves-effect waves-dark" href="{{ url('/admin/media') }}" aria-expanded="false">
                        <i class="mdi mdi-bullseye"></i>
                        <span class="hide-menu">
                            საერთაშორისო მედია
                        </span>
                    </a>

                </li>
                @endcan

                @can('political_party')
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">
                            პარტია
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ url('admin/rules') }}">წესდება</a></li>
                            <li><a href="{{ url('admin/about') }}">პარტიის შესახებ</a></li>
                            <li><a href="{{ url('admin/political_party') }}">პარტიის სტრუქტურა / საბჭო</a></li>
                        </ul>
                    </li>
                @endcan
                @can('gallery_manage')
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">
                            მედია
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ url('admin/photo_gallery') }}">ფოტო გალერეა</a></li>
                            <li><a href="{{ url('admin/video_gallery') }}">ვიდეო გალერეა</a></li>
                        </ul>
                    </li>
                @endcan


                @can('vote_manage')
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">
                            არჩევნები
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ url('admin/region') }}">რეგიონები</a></li>
                            <li><a href="{{ url('admin/majoritarians') }}">მაჟორიტარები</a></li>
                            <li><a href="{{ url('admin/proportional_list') }}">პროპორციული სია</a></li>
                            <li><a href="{{ url('admin/programs') }}">პროგრამა</a></li>
                            <li><a href="{{ url('admin/mayor') }}">მერები</a></li>
                        </ul>
                    </li>
                @endcan


                @can('vote_manage')
                    <li>
                        <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">
                            გვერდები
                        </span>
                        </a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="{{ url('admin/transfers') }}">შემოწირულობა</a></li>
                            <li><a href="{{ url('admin/young') }}">ახალგაზრდული</a></li>
                        </ul>
                    </li>
                @endcan

                @can('contact_manage')
                    <li>

                        <a class="has-arrow waves-effect waves-dark" href="{{ url('/admin/default_settings') }}" aria-expanded="false">
                            <i class="mdi mdi-bullseye"></i>
                            <span class="hide-menu">
                            Settings
                        </span>
                        </a>

                    </li>
                @endcan

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->