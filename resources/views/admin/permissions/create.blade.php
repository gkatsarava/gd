@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">@lang('global.permissions.title')</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">



                        {!! Form::open(['method' => 'POST', 'route' => ['admin.permissions.store']]) !!}



                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 form-group">
                                        {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('name'))
                                            <p class="help-block">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                        {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}
                                    </div>
                                </div>

                            </div>



                        {!! Form::close() !!}



                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection