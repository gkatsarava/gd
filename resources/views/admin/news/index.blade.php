@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">სიახლეები</h3>
            <div class="row mb-4">
                <div class="col-md-12">

                        <a href="{{ url('admin/news/create') }}" class="btn btn-info">სიახლეების დამატება</a>


                </div>
                <div class="col-md-4 mt-3">
                    <a href="{{ url('admin/news') }}" class="btn btn-primary">სიახლეები</a>
                    <a href="{{ url('admin/news?slider=1') }}" class="btn btn-danger">სლაიდერი</a>
                </div>

            </div>




            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <table class="table table-bordered">
                                <tr>
                                    <td>#</td>
                                    <td>სტატუსი</td>
                                    <td>დასახელება</td>
                                    <td>ქმედება</td>
                                    <td>სლაიდერი</td>
                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                                @foreach($partner as $row)

                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td align="center" valign="middle">
                                            @if($row->status==0)
                                                <div class="mt-2" style="background-color:limegreen;border-radius:10px;width:20px;height:20px;"></div>
                                            @else
                                                <div class="mt-2" style="background-color:red;border-radius:10px;width:20px;height:20px;"></div>
                                            @endif
                                        </td>
                                        <td>
                                            @if($row->title_ge=='')
                                                {{ $row->title_en }}
                                                @else
                                                {{ $row->title_ge }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($row->status==0)
                                            <div class="btn btn-danger btn-sm pause-news" x_id="{{ $row->id }}" y_id="1">გათიშვა</div>
                                            @else
                                            <div class="btn btn-primary btn-sm pause-news" x_id="{{ $row->id }}" y_id="0">ჩართვა</div>
                                            @endif
                                        </td>
                                        <td align="center">
                                            @if($row->slider==0)
                                            <button class="btn btn-info btn-sm pin_slider" y_id="1" x_id="{{ $row->id }}"> აპინვა</button>
                                            @else
                                                <button class="btn btn-success btn-sm pin_slider" y_id="0" x_id="{{ $row->id }}"> აპინვის მოხსნა</button>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/news') }}/{{ $row->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ url('admin/news/') }}/{{ $row->id }}"  method="POST" onsubmit="return confirm('დარწმუნებული ხარ რომ გსურს წაშლა?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger btn-sm" >წაშლა</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>


                            {{  $partner->appends(request()->input())->links("pagination::bootstrap-4") }}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection