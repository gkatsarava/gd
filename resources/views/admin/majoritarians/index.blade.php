@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">მაჟორიტარები</h3>
            <p>
                <a href="{{ url('admin/majoritarians/create') }}" class="btn btn-info">დამატება</a>
            </p>

            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form action="{{ url('admin/majoritarians') }}" method="GET">
                                <table class="table table-striped table-bordered">

                                    <tr>
                                        <td>სახელი გვარი</td>
                                        <td>რეგიონი</td>
                                        <td>ძებნა</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" value="{{ $request->fname or '' }}" name="fname">
                                        </td>
                                        <td>
                                            <select class="form-control" name="region">
                                                <option value="">აირჩიეთ რეგიონი</option>

                                                @foreach($region as $row)
                                                    <option value="{{ $row->id }}" {{ $request->region && $request->region==$row->id  ? 'selected' : '' }}>
                                                        {{ $row->name_ge }}
                                                    </option>

                                                @endforeach
                                            </select>
                                        </td>

                                        <td align="center">
                                            <button class="btn btn-info">ძებნა</button>
                                        </td>
                                    </tr>

                                </table>
                            </form>

                            <table class="table table-bordered">
                                <tr>
                                    <td>#</td>

                                    <td>სახელი გვარი</td>
                                    <td>რეგიონი</td>

                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                                @foreach($political_party as $row)

                                    <tr>
                                        <td>{{ $row->id }}</td>

                                        <td>{{ $row->name_ge or '' }}</td>
                                        <td>
                                           {{ $row->Region->name_ge or '' }}
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/majoritarians') }}/{{ $row->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ url('admin/majoritarians/') }}/{{ $row->id }}"  method="POST" onsubmit="return confirm('დარწმუნებული ხარ რომ გსურს წაშლა?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger btn-sm" >წაშლა</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>



                            {{  $political_party->appends(request()->input())->links("pagination::bootstrap-4") }}


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection