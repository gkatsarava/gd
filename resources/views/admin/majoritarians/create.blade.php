@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">მაჟორიტარების დამატება</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">


                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif


                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('name_ge'))
                                            <label style="color:red;">სახელი გვარი ქართულად</label>
                                        @else
                                            <label>სახელი გვარი ქართულად</label>
                                        @endif
                                        <input type="text" value="{{ $news->name_ge or '' }} {{ old('name_ge') ? old('name_ge') : '' }}" name="name_ge"  class="form-control">

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>სახელი გვარი ინგლისურად</label>
                                        <input type="text" value="{{ $news->name_en or '' }} {{ old('name_en') ? old('name_en') : '' }}" name="name_en"  class="form-control">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('name_ge'))
                                            <label style="color:red;">რაიონი ქართულად</label>
                                        @else
                                            <label>რაიონი ქართულად</label>
                                        @endif
                                        <input type="text" value="{{ $news->location_ge or '' }} {{ old('location_ge') ? old('location_ge') : '' }}" name="location_ge"  class="form-control">

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>რაიონი ინგლისურად</label>
                                        <input type="text" value="{{ $news->location_en or '' }} {{ old('location_en') ? old('location_en') : '' }}" name="location_en"  class="form-control">

                                    </div>





                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('position_ge'))
                                            <label style="color:red;">მაჟორიტარის პროცენტი</label>
                                        @else
                                            <label>მაჟორიტარის პროცენტი</label>
                                        @endif
                                        <input type="text" value="{{ $news->majoritarian_percent or '' }} {{ old('majoritarian_percent') ? old('majoritarian_percent') : '' }}" name="majoritarian_percent"  class="form-control">

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>ცესკო-ს ლინკი</label>
                                        <input type="text" value="{{ $news->cesco_link or '' }} {{ old('cesco_link') ? old('cesco_link') : '' }}" name="cesco_link"  class="form-control">

                                    </div>

                                </div>


                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <label>რეგიონი</label>
                                        <select class="form-control region" name="region">
                                            <option value="">აირჩიეთ რეგიონი</option>
                                            @foreach($region as $row)
                                                <option value="{{ $row->id }}" {{ isset($news->region_id) && $row->id == $news->region_id ? "selected" : "" }}>{{ $row->name_ge }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-6 {{ isset($news->district_id) ? "d-block" : "d-none" }} district_block">
                                        <label>რაიონი</label>
                                        <select class="form-control  district" name="district">
                                            <option value="">აირჩიეთ რაიონი</option>
                                            @if(isset($news->district_id))
                                            @foreach($news->Region->District as $row)

                                                <option value="{{ $row->id }}" {{ isset($news->district_id) && $row->id == $news->district_id ? "selected" : "" }}>{{ $row->name_ge }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>






                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('work_ge'))
                                            <label style="color:red;">სამუშაო გამოცდილება ქართულად</label>
                                        @else
                                            <label>სამუშაო გამოცდილება  ქართულად</label>
                                        @endif
                                        <textarea name="work_ge" id="mymce" class="form-control">{{ old('work_ge') ? old('work_ge') : '' }}{{ $news->work_ge or '' }}</textarea>

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>სამუშაო გამოცდილება ინგლისურად</label>
                                        <textarea name="work_en" id="mymce" class="form-control">{{ old('work_en') ? old('work_en') : '' }}{{ $news->work_en or '' }}</textarea>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('work_ge'))
                                            <label style="color:red;">განათლება ქართულად</label>
                                        @else
                                            <label>განათლება  ქართულად</label>
                                        @endif
                                        <textarea name="education_ge" id="mymce" class="form-control">{{ old('education_ge') ? old('education_ge') : '' }}{{ $news->education_ge or '' }}</textarea>

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>განათლება ინგლისურად</label>
                                        <textarea name="education_en" id="mymce" class="form-control">{{ old('education_en') ? old('education_en') : '' }}{{ $news->education_en or '' }}</textarea>

                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        <label>Facebook პროფილის ლინკი</label>
                                        <input name="facebook" class="form-control" value="{{ old('facebook') ? old('facebook') : '' }}{{ $news->facebook or '' }}">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        <label>Instagram პროფილის ლინკი</label>
                                        <input name="instagram"  class="form-control" value="{{ old('instagram') ? old('instagram') : '' }}{{ $news->instagram or '' }}">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        <label>Twitter პროფილის ლინკი</label>
                                        <input name="twitter"  class="form-control" value="{{ old('twitter') ? old('twitter') : '' }}{{ $news->twitter or '' }}">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        <label>Youtube პროფილის ლინკი</label>
                                        <input name="youtube"  class="form-control" value="{{ old('youtube') ? old('youtube') : '' }}{{ $news->youtube or '' }}">

                                    </div>

                                </div>




                                <div class="row">
                                    @if(isset($form['id']))
                                        @if($news->picture!='')
                                            <div class="col-md-12 mt-5 mb-5">
                                                <label>სურათი</label>



                                                <div class="row">


                                                    <div class="col-md-4 p-2 mb-2" style="border:1px solid #efefef;border-radius:10px;">

                                                        <div class="btn btn-sm btn-danger mb-2 delete-majoritarians-picture"   x_id="{{ $news->id }}">წაშლა</div>
                                                        <img src="{{ asset('uploads/majoritarians') }}/{{ $news->picture }}" class="img-fluid">
                                                        <input type="text" name="image" hidden value="{{ $news->picture or '' }}">

                                                    </div>


                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                    <div class="col-md-12 mt-2">

                                        <div id="my-majoritarian-files"  class="dropzone">



                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12 mt-3">
                                        <button class="btn btn-success">შენახვა</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection