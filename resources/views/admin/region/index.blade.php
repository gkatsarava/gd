@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">რეგიონები</h3>
            <p>
                <a href="{{ url('admin/region/create') }}" class="btn btn-info">რეგიონის დამატება</a>
            </p>

            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <table class="table table-bordered">
                                <tr>
                                    <td>#</td>
                                    <td>დასახელება</td>
                                    <td>რაიონი</td>
                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                                @foreach($region as $row)

                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->name_ge }}</td>
                                        <td>
                                            <a href="{{ url('admin/region/add_district') }}/{{ $row->id }}">
                                                <button class="btn btn-sm btn-info">რაიონის დამატება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/region') }}/{{ $row->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ url('admin/region/') }}/{{ $row->id }}"  method="POST" onsubmit="return confirm('დარწმუნებული ხარ რომ გსურს წაშლა?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger btn-sm" >წაშლა</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>


                            {{  $region->appends(request()->input())->links("pagination::bootstrap-4") }}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection