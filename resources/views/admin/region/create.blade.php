@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">რეგიონის დამატება</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif

                                <div class="row">

                                    <div class="col-md-6">
                                        <label>დასახელება ქართულად</label>
                                        <input type="text" class="form-control" value="{{ $region->name_ge or '' }}" name="name_ge">
                                    </div>

                                    <div class="col-md-6">
                                        <label>დასახელება ინგლისურად</label>
                                        <input type="text" class="form-control" value="{{ $region->name_en or '' }}" name="name_en">
                                    </div>


                                    <div class="col-md-12 mt-3">
                                        <button class="btn btn-success">შენახვა</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection