@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">რაიონის დამატება</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">


                                <div class="row">

                                    <div class="col-md-12">
                                    <h3 class="mb-3">რეგიონის დასახელება: {{ $region->name_ge }}</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="name_ge_label">დასახელება ქართულად</label>
                                        <input type="text" class="form-control name_ge" value="" name="name_ge">
                                    </div>

                                    <div class="col-md-6">
                                        <label class="name_en_label">დასახელება ინგლისურად</label>
                                        <input type="text" class="form-control name_en" value="" name="name_en">
                                    </div>


                                    <div class="col-md-12 mt-3">
                                        <button class="btn btn-success save-district" x_id="{{ $request->id }}">შენახვა</button>
                                        <button class="btn btn-success d-none update-district">შენახვა</button>

                                    </div>
                                </div>


                            <table class="table table-striped table-bordered mt-3">
                                <tr>
                                    <td>დასახელება ქართულად</td>
                                    <td>დასახელება ინგლისურად</td>
                                    <td>მომხმარებელი</td>
                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                                @foreach($district as $row)
                                    <tr>
                                        <td>{{ $row->name_ge }}</td>
                                        <td>{{ $row->name_en }}</td>
                                        <td>{{ $row->User->name }}</td>
                                        <td>
                                            <div class="btn btn-sm btn-success edit-district" x_id="{{ $row->id }}">რედაქტირება</div>
                                        </td>
                                        <td>
                                            <div class="btn btn-sm btn-danger delete-district" x_id="{{ $row->id }}">წაშლა</div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection