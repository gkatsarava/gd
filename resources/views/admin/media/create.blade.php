@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">სიახლეები</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                            {{ csrf_field() }}

                            @if(isset($form['id']))
                                {{ method_field('PUT') }}
                            @endif




                            <div class="col-md-12 mt-2">

                                @if($errors->has('title_ge'))
                                    <label style="color:red;">დასახელება ქართულად</label>
                                @else
                                    <label>დასახელება ქართულად</label>
                                @endif
                                <input type="text" value="{{ $news->title_ge or '' }}" name="title_ge"  class="form-control">

                            </div>

                                <div class="col-md-12 mt-2">

                                    <label>დასახელება ინგლისურად</label>
                                    <input type="text" value="{{ $news->title_en or '' }}" name="title_en"  class="form-control">

                                </div>




                            <div class="col-md-12 mt-2">

                                @if($errors->has('desc_ge'))
                                    <label style="color:red;">აღწერილობა ქართულად</label>
                                @else
                                    <label>აღწერილობა ქართულად</label>
                                @endif
                                <textarea name="desc_ge" id="mymce" class="form-control">{{ $news->desc_ge or '' }}</textarea>

                            </div>

                                <div class="col-md-12 mt-2">

                                    <label>აღწერილობა ინგლისურად</label>
                                    <textarea name="desc_en" id="mymce" class="form-control">{{ $news->desc_en or '' }}</textarea>

                                </div>

                                <div class="col-md-12 mt-2">
                                    <label>Youtube Embed</label>
                                    <textarea name="youtube"  class="form-control">{{ $news->youtube or '' }}</textarea>

                                </div>


                                <div class="col-md-12 mt-2">
                                    <label>წყარო</label>
                                    <input type="text" name="source" class="form-control" value="{{ $news->source or '' }}">

                                </div>





                            @if(isset($form['id']))
                                <div class="col-md-12 mt-5 mb-5">
                                    <label>სიახლის სურათი</label>



                                    <div class="row">
                                        @if(isset($news->Pictures))
                                             @foreach($news->Pictures as $row)


                                        <div class="col-md-4 p-2 mb-2" style="border:1px solid #efefef;border-radius:10px;">

                                            <div class="btn btn-sm btn-danger mb-2 delete-file-media"   x_id="{{ $row->id }}">წაშლა</div>
                                            <div class="btn btn-sm  mb-2 pin_home_media  {!! $row->first_picture == "1" ? "btn-warning" : "btn-info" !!}" {!! $row->first_picture == "1" ? "" : "style='opacity:0.7'" !!} x_id="{{ $row->id }}">მთავარი ფოტო</div>
                                            <img src="{{ asset('uploads/media') }}/{{ $row->picture }}" class="img-fluid">
                                            <input type="text" name="image[]" hidden value="{{ $row->picture or '' }}">

                                        </div>


                                    @endforeach
                                        @endif
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-12 mt-2">
                                @if($errors->has('checkedfirst'))
                                <label style="color:red;">სიახლის სურათის ატვირთვა</label>
                                    @else
                                <label>სიახლის სურათის ატვირთვა</label>
                                @endif


                                <div id="my-media-files"  class="dropzone">



                                    <div class="fallback">
                                        <input name="file" type="file" multiple/>
                                    </div>
                            </div>

                             </div>

                            <div class="col-md-12 mt-3">
                                <button class="btn btn-success">შენახვა</button>
                            </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection