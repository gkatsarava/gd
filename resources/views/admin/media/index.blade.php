@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">საერთაშორისო მედია</h3>
            <p>
                <a href="{{ url('admin/media/create') }}" class="btn btn-info">საერთაშორისო მედიის დამატება</a>
            </p>

            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <table class="table table-bordered">
                                <tr>
                                    <td>#</td>
                                    <td>დასახელება</td>
                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                                @foreach($partner as $row)

                                    <tr>
                                        <td>{{ $row->id }}</td>
                                        <td>{{ $row->title_ge }}</td>
                                        <td>
                                            <a href="{{ url('admin/media') }}/{{ $row->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ url('admin/media/') }}/{{ $row->id }}"  method="POST" onsubmit="return confirm('დარწმუნებული ხარ რომ გსურს წაშლა?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger btn-sm" >წაშლა</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>


                            {{  $partner->appends(request()->input())->links("pagination::bootstrap-4") }}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection