@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">ვიდეო გალერეა</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                            {{ csrf_field() }}

                            @if(isset($form['id']))
                                {{ method_field('PUT') }}
                            @endif


                            <div class="col-md-12 mt-2">

                                @if($errors->has('title_ge'))
                                    <label style="color:red;">დასახელება ქართულად</label>
                                @else
                                    <label>დასახელება ქართულად</label>
                                @endif
                                <input type="text" value="{{ $news->title_ge or '' }}" name="title_ge"  class="form-control">

                            </div>

                                <div class="col-md-12 mt-2">

                                    <label>დასახელება ინგლისურად</label>
                                    <input type="text" value="{{ $news->title_en or '' }}" name="title_en"  class="form-control">

                                </div>






                                <div class="col-md-12 mt-2">
                                    <label>Youtube ლინკი</label>
                                    <input type="text" value="{{ $news->youtube or '' }}" name="youtube"  class="form-control">

                                </div>




                            <div class="col-md-12 mt-3">
                                <button class="btn btn-success">შენახვა</button>
                            </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection