@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">@lang('global.roles.title')</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->

                    <div class="card">

                        {!! Form::model($role, ['method' => 'PUT', 'route' => ['admin.roles.update', $role->id]]) !!}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 form-group">
                                    {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 form-group">
                                    {!! Form::label('permission', 'Permissions', ['class' => 'control-label']) !!}
                                    {!! Form::select('permission[]', $permissions, old('permission') ? old('permission') : $role->permissions()->pluck('name', 'name'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('permission'))
                                        <p class="help-block">
                                            {{ $errors->first('permission') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-danger']) !!}

                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection