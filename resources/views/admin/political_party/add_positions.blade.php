@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">პარტიის სტრუქტურა</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">



                            <div class="row">
                                <div class="col-md-12">
                                    <h3>სახელი გვარი: {{ $politican->name_ge }}</h3>
                                </div>
                            </div>


                            <div class="row mt-3">

                                <div class="col-md-6">
                                    <label class="name_ge_label">თანამდებობა ქართულად</label>
                                    <input type="text" class="form-control position_ge" name="position_ge">
                                </div>

                                <div class="col-md-6">
                                    <label>თანამდებობა ინგლისურად</label>
                                    <input type="text" class="form-control position_en" name="position_en">
                                </div>
                                    <div class="col-md-12 mt-3">
                                        <button class="btn btn-success save-other-position" x_id="{{ $request->id }}">შენახვა</button>
                                        <button class="btn btn-success d-none update-other-position">განახლება</button>
                                    </div>

                            </div>



                                <table class="table table-striped table-bordered mt-3">
                                    <tr>
                                        <td>
                                            თანამდებობა ქართულად
                                        </td>
                                        <td>
                                            თანამდებობა ინგლისურად
                                        </td>
                                        <td>მოხმარებელი</td>
                                        <td>
                                           რედაქტირება
                                        </td>

                                        <td>
                                            წაშლა
                                        </td>

                                    </tr>
                                    @foreach($Positions as $row)
                                        <tr>
                                            <td>
                                                {{ $row->name_ge }}
                                            </td>
                                            <td>
                                                {{ $row->name_en }}
                                            </td>
                                            <td>{{ $row->User->name or '' }}</td>
                                            <td>
                                                <div class="btn btn-sm btn-success edit-other-position" x_id="{{ $row->id }}">რედაქტირება</div>
                                            </td>
                                            <td>
                                                <div class="btn btn-sm btn-danger delete-other-position" x_id="{{ $row->id }}">წაშლა</div>
                                            </td>

                                        </tr>
                                    @endforeach

                                </table>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection