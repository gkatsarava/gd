@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">პარტიის სტრუქტურა</h3>
            <p>
                <a href="{{ url('admin/political_party/create') }}" class="btn btn-info">დამატება</a>
            </p>

            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form action="{{ url('admin/political_party') }}" method="GET">
                            <table class="table table-striped table-bordered">

                                <tr>
                                    <td>სახელი გვარი</td>
                                    <td>სტრუქტურა</td>
                                    <td>პარტიაში თანამდებობა</td>
                                    <td>ძებნა</td>
                                </tr>

                                <tr>
                                    <td>
                                        <input type="text" class="form-control" value="{{ $request->fname or '' }}" name="fname">
                                    </td>
                                    <td>
                                        <select class="form-control" name="structure">
                                            <option value="">აირჩიეთ სტრუქტურა</option>

                                            <option value="1" {{ $request->structure && $request->structure==1  ? 'selected' : '' }}>პოლიტიკური საბჭო</option>
                                            <option value="2" {{ $request->structure && $request->structure==2  ? 'selected' : '' }}>პარტიის სტრუქტურა</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" value="{{ $request->position or '' }}" name="position">
                                    </td>
                                    <td align="center">
                                        <button class="btn btn-info">ძებნა</button>
                                    </td>
                                </tr>

                            </table>
                            </form>

                            <table class="table table-sortable table-bordered">
                                <tr>
                                    <td>#</td>

                                    <td>სახელი გვარი</td>
                                    <td>პარტიაში თანამდებობა</td>
                                    <td>სტრუქტურა</td>

                                    <td>თანამდებობები</td>

                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                            @foreach($political_party as $key=> $row)

                                    <tr id="sort_{{ $row->id }}">
                                        <td>{{ $row->id }}</td>

                                        <td>{{ $row->name_ge }}</td>
                                        <td>{{ $row->position_ge }}</td>
                                        <td>
                                            @if($row->structure==1)
                                                პოლიტიკური საბჭო
                                                @else
                                                პარტიის სტრუქტურა
                                            @endif
                                        </td>

                                        <td>
                                            <a href="{{ url('admin/political_party/add_positions') }}/{{ $row->id }}">
                                            <button class="btn btn-sm btn-success">თან. დამატება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/political_party') }}/{{ $row->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ url('admin/political_party/') }}/{{ $row->id }}"  method="POST" onsubmit="return confirm('დარწმუნებული ხარ რომ გსურს წაშლა?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger btn-sm" >წაშლა</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>



                            {{  $political_party->appends(request()->input())->links("pagination::bootstrap-4") }}


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection