@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">პარტიის სტრუქტურა</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">


                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif


                                <div class="row">
                                <div class="col-md-6 mt-2">

                                    @if($errors->has('name_ge'))
                                        <label style="color:red;">სახელი გვარი ქართულად</label>
                                    @else
                                        <label>სახელი გვარი ქართულად</label>
                                    @endif
                                    <input type="text" value="{{ $news->name_ge or '' }} {{ old('name_ge') ? old('name_ge') : '' }}" name="name_ge"  class="form-control">

                                </div>

                                <div class="col-md-6 mt-2">

                                    <label>სახელი გვარი ინგლისურად</label>
                                    <input type="text" value="{{ $news->name_en or '' }} {{ old('name_en') ? old('name_en') : '' }}" name="name_en"  class="form-control">

                                </div>






                                <div class="col-md-6 mt-2">

                                    @if($errors->has('position_ge'))
                                        <label style="color:red;">პარტიაში თანამდებობა ქართულად</label>
                                    @else
                                        <label>პარტიაში თანამდებობა ქართულად</label>
                                    @endif
                                    <input type="text" value="{{ $news->position_ge or '' }} {{ old('position_ge') ? old('position_ge') : '' }}" name="position_ge"  class="form-control">

                                </div>

                                <div class="col-md-6 mt-2">

                                    <label>პარტიაში თანამდებობა ინგლისურად</label>
                                    <input type="text" value="{{ $news->position_en or '' }} {{ old('position_en') ? old('position_en') : '' }}" name="position_en"  class="form-control">

                                </div>

                                </div>


                               {{-- <hr>

                                <div class="row mb-5">
                                    <div class="col-md-12 mt-2">


                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>თანამდებობება ქართულად</label>
                                                <input type="text" class="form-control other_position_ge" placeholder="თანამდებობა">
                                            </div>

                                            <div class="col-md-6">
                                                <label>თანამდებობება ინგლისურად</label>
                                                <input type="text" class="form-control other_position_en" placeholder="თანამდებობა">
                                            </div>

                                        </div>

                                        <div class="btn btn-info mt-2 float-right btn-add-position">დამატება</div>

                                    </div>

                                    <div class="col-md-7">
                                        <table class="mt-2 table table-bordered table-striped append_positions">
                                            <tr>
                                                <td>თანამდებობა ქართულად</td>
                                                <td>თანამდებობა ინგლისურად</td>
                                                <td>ქმედება</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <hr>--}}



                                <div class="row">
                                    <div class="col-md-6  mt-2">

                                        @if($errors->has('structure'))
                                            <label style="color:red;">სტრუქტურა</label>
                                        @else
                                            <label>სტრუქტურა</label>
                                        @endif
                                        <select class="form-control" name="structure">
                                            <option value="">აირჩიეთ სტრუქტურა</option>
                                            <option value="1" {{ old('structure_id') && old('structure_id')==1  ? 'selected' : '' }} {{ isset($news->structure) && $news->structure==1  ? 'selected' : '' }}>პოლიტიკური საბჭო</option>
                                            <option value="2" {{ old('structure_id') && old('structure_id')==2  ? 'selected' : '' }} {{ isset($news->structure) && $news->structure==2  ? 'selected' : '' }}>პარტიის სტრუქტურა</option>
                                        </select>
                                    </div>
                                </div>



                                <div class="row">
                                <div class="col-md-6 mt-2">

                                    @if($errors->has('desc'))
                                        <label style="color:red;">აღწერილობა</label>
                                    @else
                                        <label>აღწერილობა</label>
                                    @endif
                                        <textarea name="desc_ge" id="mymce" class="form-control">{{ old('desc_ge') ? old('desc_ge') : '' }}{{ $news->desc_ge or '' }}</textarea>

                                </div>

                                <div class="col-md-6 mt-2">

                                    <label>აღწერილობა</label>
                                    <textarea name="desc_en" id="mymce" class="form-control">{{ old('desc_en') ? old('desc_en') : '' }}{{ $news->desc_en or '' }}</textarea>

                                </div>
                                </div>


                               {{-- <div class="row">
                                <div class="col-md-6 mt-2">

                                    @if($errors->has('work_ge'))
                                        <label style="color:red;">განათლება ქართულად</label>
                                    @else
                                        <label>განათლება  ქართულად</label>
                                    @endif
                                    <textarea name="education_ge" id="mymce" class="form-control">{{ old('education_ge') ? old('education_ge') : '' }}{{ $news->education_ge or '' }}</textarea>

                                </div>

                                <div class="col-md-6 mt-2">

                                    <label>განათლება ინგლისურად</label>
                                    <textarea name="education_en" id="mymce" class="form-control">{{ old('education_en') ? old('education_en') : '' }}{{ $news->education_en or '' }}</textarea>

                                </div>
                                </div>

--}}


                                <div class="row">
                                <div class="col-md-6 mt-2">

                                    <label>Facebook პროფილის ლინკი</label>
                                    <input name="facebook" class="form-control" value="{{ old('facebook') ? old('facebook') : '' }}{{ $news->facebook or '' }}">

                                </div>


                                <div class="col-md-6 mt-2">

                                    <label>Instagram პროფილის ლინკი</label>
                                    <input name="instagram"  class="form-control" value="{{ old('instagram') ? old('instagram') : '' }}{{ $news->instagram or '' }}">

                                </div>


                                <div class="col-md-6 mt-2">

                                    <label>Twitter პროფილის ლინკი</label>
                                    <input name="twitter"  class="form-control" value="{{ old('twitter') ? old('twitter') : '' }}{{ $news->twitter or '' }}">

                                </div>


                                <div class="col-md-6 mt-2">

                                    <label>Youtube პროფილის ლინკი</label>
                                    <input name="youtube"  class="form-control" value="{{ old('youtube') ? old('youtube') : '' }}{{ $news->youtube or '' }}">

                                </div>

                                </div>




                                <div class="row">
                                 @if(isset($form['id']))
                                     @if($news->picture!='')
                                        <div class="col-md-12 mt-5 mb-5">
                                        <label>სურათი</label>



                                        <div class="row">


                                                <div class="col-md-4 p-2 mb-2" style="border:1px solid #efefef;border-radius:10px;">

                                                    <div class="btn btn-sm btn-danger mb-2 delete-political-party-picture"   x_id="{{ $news->id }}">წაშლა</div>
                                                    <img src="{{ asset('uploads/political_party') }}/{{ $news->picture }}" class="img-fluid">
                                                    <input type="text" name="image" hidden value="{{ $news->picture or '' }}">

                                                </div>


                                        </div>
                                    </div>
                                     @endif
                                @endif

                                <div class="col-md-12 mt-2">

                                    <div id="my-political-party-files"  class="dropzone">



                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 mt-3">
                                    <button class="btn btn-success">შენახვა</button>
                                </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection