@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">ჩვენს შესახებ</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif



                                <div class="row">
                                    <div class="col-md-12 mt-2">
                                        <label>ელ.ფოსტა</label>
                                        <input type="text" class="form-control" value="{{ $about->email  or ''}}" name="email">
                                    </div>
                                    <div class="col-md-12 mt-2">
                                        <label>ტელეფონის ნომერი</label>
                                        <input type="text" class="form-control" value="{{ $about->phone  or ''}}" name="phone">
                                    </div>

                                    <div class="col-md-6 mt-2">
                                        <label>მისამართი ქართულად</label>
                                        <input type="text" class="form-control" value="{{ $about->address_ge  or ''}}"  name="address_ge">
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <label>მისამართი ინგლისურად</label>
                                        <input type="text" class="form-control" value="{{ $about->address_en  or ''}}" name="address_en">
                                    </div>


                                    <div class="col-md-12 mt-2">
                                        <label>საკონტაქტო ელ.ფოსტა ფორმისთვის</label>
                                        <input type="text" class="form-control" value="{{ $about->contact_email_for_form  or ''}}" name="contact_email_for_form">
                                    </div>


                                    <div class="col-md-12 mt-2">
                                        <label>Facebook</label>
                                        <input type="text" class="form-control" value="{{ $about->facebook  or ''}}" name="facebook">
                                    </div>


                                    <div class="col-md-12 mt-2">
                                        <label>Twitter</label>
                                        <input type="text" class="form-control" value="{{ $about->twitter  or ''}}" name="twitter">
                                    </div>


                                    <div class="col-md-12 mt-2">
                                        <label>Youtube</label>
                                        <input type="text" class="form-control" value="{{ $about->youtube  or ''}}" name="youtube">
                                    </div>



                                    <div class="col-md-12 mt-3">
                                        <button class="btn btn-success">შენახვა</button>
                                    </div>
                                </div>




                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection