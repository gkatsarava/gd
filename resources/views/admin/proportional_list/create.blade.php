@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">მაჟორიტარების დამატება</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">


                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif

{{--
                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('name_ge'))
                                            <label style="color:red;">სახელი გვარი ქართულად</label>
                                        @else
                                            <label>სახელი გვარი ქართულად</label>
                                        @endif
                                        <input type="text" value="{{ $news->name_ge or '' }} {{ old('name_ge') ? old('name_ge') : '' }}" name="name_ge"  class="form-control">

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>სახელი გვარი ინგლისურად</label>
                                        <input type="text" value="{{ $news->name_en or '' }} {{ old('name_en') ? old('name_en') : '' }}" name="name_en"  class="form-control">

                                    </div>








                                </div>


                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <label>სიაში რომელ ნომრად არის წარმოდგენილი?</label>
                                        <input type="text" class="form-control" value="{{ $news->number or '' }} {{ old('number') ? old('number') : '' }}" name="number">
                                    </div>
                                </div>





                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('work_ge'))
                                            <label style="color:red;">სამუშაო გამოცდილება ქართულად</label>
                                        @else
                                            <label>სამუშაო გამოცდილება  ქართულად</label>
                                        @endif
                                        <textarea name="work_ge" id="mymce" class="form-control">{{ old('work_ge') ? old('work_ge') : '' }}{{ $news->work_ge or '' }}</textarea>

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>სამუშაო გამოცდილება ინგლისურად</label>
                                        <textarea name="work_en" id="mymce" class="form-control">{{ old('work_en') ? old('work_en') : '' }}{{ $news->work_en or '' }}</textarea>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('work_ge'))
                                            <label style="color:red;">განათლება ქართულად</label>
                                        @else
                                            <label>განათლება  ქართულად</label>
                                        @endif
                                        <textarea name="education_ge" id="mymce" class="form-control">{{ old('education_ge') ? old('education_ge') : '' }}{{ $news->education_ge or '' }}</textarea>

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>განათლება ინგლისურად</label>
                                        <textarea name="education_en" id="mymce" class="form-control">{{ old('education_en') ? old('education_en') : '' }}{{ $news->education_en or '' }}</textarea>

                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        <label>Facebook პროფილის ლინკი</label>
                                        <input name="facebook" class="form-control" value="{{ old('facebook') ? old('facebook') : '' }}{{ $news->facebook or '' }}">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        <label>Instagram პროფილის ლინკი</label>
                                        <input name="instagram"  class="form-control" value="{{ old('instagram') ? old('instagram') : '' }}{{ $news->instagram or '' }}">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        <label>Twitter პროფილის ლინკი</label>
                                        <input name="twitter"  class="form-control" value="{{ old('twitter') ? old('twitter') : '' }}{{ $news->twitter or '' }}">

                                    </div>


                                    <div class="col-md-6 mt-2">

                                        <label>Youtube პროფილის ლინკი</label>
                                        <input name="youtube"  class="form-control" value="{{ old('youtube') ? old('youtube') : '' }}{{ $news->youtube or '' }}">

                                    </div>

                                </div>




                                <div class="row">
                                    @if(isset($form['id']))
                                        @if($news->picture!='')
                                            <div class="col-md-12 mt-5 mb-5">
                                                <label>სურათი</label>



                                                <div class="row">


                                                    <div class="col-md-4 p-2 mb-2" style="border:1px solid #efefef;border-radius:10px;">

                                                        <div class="btn btn-sm btn-danger mb-2 delete-proportional-list-picture"   x_id="{{ $news->id }}">წაშლა</div>
                                                        <img src="{{ asset('uploads/proportional_list') }}/{{ $news->picture }}" class="img-fluid">
                                                        <input type="text" name="image" hidden value="{{ $news->picture or '' }}">

                                                    </div>


                                                </div>
                                            </div>
                                        @endif
                                    @endif

                                    <div class="col-md-12 mt-2">

                                        <div id="my-proportional-list-files"  class="dropzone">



                                            <div class="fallback">
                                                <input name="file" type="file" multiple/>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12 mt-3">
                                        <button class="btn btn-success">შენახვა</button>
                                    </div>
                                </div>--}}


                                <div class="row">
                                    <div class="col-md-6 mt-2">

                                        @if($errors->has('proportional_list'))
                                            <label style="color:red;">პროპორციული სია ქართულად</label>
                                        @else
                                            <label>პროპორციული სია ქართულად</label>
                                        @endif
                                        <textarea name="proportional_list_ge" id="mymce" class="form-control">{{ old('proportional_list_ge') ? old('proportional_list_ge') : '' }}{{ $news->desc_ge or '' }}</textarea>

                                    </div>

                                    <div class="col-md-6 mt-2">

                                        <label>პროპორციული სია ინგლისურად</label>
                                        <textarea name="proportional_list_en" id="mymce" class="form-control">{{ old('proportional_list_en') ? old('proportional_list_en') : '' }}{{ $news->desc_en or '' }}</textarea>

                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-12 mt-3">
                                    <button class="btn btn-success">შენახვა</button>
                                </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection