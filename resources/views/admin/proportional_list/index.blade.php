@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">პროპორციული სია</h3>
            <p>
                <a href="{{ url('admin/proportional_list/create') }}" class="btn btn-info">დამატება</a>
            </p>

            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                         {{--   <form action="{{ url('admin/proportional_list') }}" method="GET">
                                <table class="table table-striped table-bordered">

                                    <tr>
                                        <td>სახელი გვარი</td>
                                        <td>ნომერი</td>
                                        <td>ძებნა</td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" value="{{ $request->fname or '' }}" name="fname">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" value="{{ $request->number or '' }}" name="number">
                                        </td>

                                        <td align="center">
                                            <button class="btn btn-info">ძებნა</button>
                                        </td>
                                    </tr>

                                </table>
                            </form>--}}

                            <table class="table table-bordered">
                                <tr>
                                    <td>#</td>

                                    <td>პროპორციული სია</td>
                                    <td>რედაქტირება</td>
                                    <td>წაშლა</td>
                                </tr>
                                @foreach($political_party as $row)

                                    <tr>
                                        <td>{{ $row->id }}</td>

                                        <td>პროპორციული სია</td>

                                        <td>
                                            <a href="{{ url('admin/proportional_list') }}/{{ $row->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>
                                        <td>
                                            <form action="{{ url('admin/proportional_list/') }}/{{ $row->id }}"  method="POST" onsubmit="return confirm('დარწმუნებული ხარ რომ გსურს წაშლა?')">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button class="btn btn-danger btn-sm" >წაშლა</button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>



                            {{  $political_party->appends(request()->input())->links("pagination::bootstrap-4") }}


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection