@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">ახალგაზრდულის შესახებ</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif






                                <div class="col-md-12 mt-2">

                                    <label>ახალგაზრდულის შესახებ ქართულად</label>
                                    <textarea name="desc_ge" id="mymce" class="form-control">{{ $about->desc_ge or '' }}</textarea>

                                </div>

                                <div class="col-md-12 mt-2">

                                    <label>ახალგაზრდულის შესახებ ინგლისურად</label>
                                    <textarea name="desc_en" id="mymce" class="form-control">{{ $about->desc_en or '' }}</textarea>

                                </div>



                                @if(isset($form['id']))
                                    <div class="col-md-12 mt-5 mb-5">
                                        <label>სურათი</label>
                                        <br>
                                        <img src="{{ asset('uploads/young') }}/{{ $about->picture }}">
                                        <input type="text" name="image" hidden value="{{ $about->picture or '' }}">
                                    </div>
                                @endif

                                <div class="col-md-12 mt-2">
                                    <label>სურათის ატვირთვა</label>
                                    <div id="my-young-files"  class="dropzone">



                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12 mt-3">
                                    <button class="btn btn-success">შენახვა</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection