@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">ახალგაზრდულის შესახებ</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <table class="table table-bordered">
                                <tr>
                                    <td>#</td>
                                    <td>დასახელება</td>
                                    <td>ქმედება</td>
                                </tr>

                                    <tr>
                                        <td>{{ $about->id }}</td>
                                        <td>ახალგაზრდულის შესახებ</td>
                                        <td>
                                            <a href="{{ url('admin/young') }}/{{ $about->id }}/edit">
                                                <button class="btn btn-primary btn-sm">რედაქტირება</button>
                                            </a>
                                        </td>

                                    </tr>

                            </table>




                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection