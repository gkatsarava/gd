@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">@lang('global.roles.title')</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        {!! Form::open(['method' => 'POST', 'route' => ['admin.users.store']]) !!}




                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 form-group">
                                        {!! Form::label('name', 'Name*', ['class' => 'control-label']) !!}
                                        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('name'))
                                            <p class="help-block">
                                                {{ $errors->first('name') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        {!! Form::label('email', 'Email*', ['class' => 'control-label']) !!}
                                        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('email'))
                                            <p class="help-block">
                                                {{ $errors->first('email') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        {!! Form::label('password', 'Password*', ['class' => 'control-label']) !!}
                                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('password'))
                                            <p class="help-block">
                                                {{ $errors->first('password') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 form-group">
                                        {!! Form::label('roles', 'Roles*', ['class' => 'control-label']) !!}
                                        {!! Form::select('roles[]', $roles, old('roles'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('roles'))
                                            <p class="help-block">
                                                {{ $errors->first('roles') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>

                                {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-danger']) !!}

                            </div>



                        {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection