@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">სლაიდერი</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                            {{ csrf_field() }}

                            @if(isset($form['id']))
                                {{ method_field('PUT') }}
                            @endif


                            <div class="col-md-12 mt-2">

                                <label>დასახელება ქართულად</label>
                                <input type="text" value="{{ $slider->title_ge or '' }}" name="title_ge"  class="form-control">

                            </div>

                            <div class="col-md-12 mt-2">

                                <label>დასახელება ინგლისურად</label>
                                <input type="text" value="{{ $slider->title_en or '' }}" name="title_en"  class="form-control">

                            </div>

                            <div class="col-md-12 mt-2">

                                <label>დასახელება რუსულად</label>
                                <input type="text" value="{{ $slider->title_ru or '' }}" name="title_ru"  class="form-control">

                            </div>



                                <div class="col-md-12 mt-2">

                                    <label>ლინკი (თუ ლინკს შეიყვანთ გამოჩნდება ღიალკი სლაიდერზე)</label>
                                    <input type="text" value="{{ $slider->link or '' }}" name="link"  class="form-control">

                                </div>


                            @if(isset($form['id']))
                                <div class="col-md-6 mt-5 mb-5">
                                    <label>სლაიდერის სურათი </label>

                                    <img src="{{ asset('uploads/Slider') }}/{{ $slider->picture }}" class="img-fluid">
                                    <input type="text" name="image" hidden value="{{ $slider->picture or '' }}">
                                </div>
                            @endif

                            <div class="col-md-12 mt-2">
                                <label>სლაიდერის სურათის ატვირთვა</label>
                                <div id="my-slider-files"  class="dropzone">



                                    <div class="fallback">
                                        <input name="file" type="file" multiple/>
                                    </div>
                            </div>

                             </div>

                            <div class="col-md-12 mt-3">
                                <button class="btn btn-success">შენახვა</button>
                            </div>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
    </script>
@endsection