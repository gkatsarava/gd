@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">პროგრამა</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif






                                @if(isset($form['id']))
                                    <div class="col-md-12 mt-5 mb-5">
                                        <label>ფაილი</label>
                                        <br>
                                        @if( $about->file_ge!='')
                                        <a href="{{ url('admin/programs/delete') }}?id={{ $about->id }}&lang=ge" class="btn btn-danger">წაშლა</a>
                                        <br>
                                        @endif
                                        <a href="{{ url('uploads/programs/') }}/{{ $about->file_ge }}" target="_blank">
                                            {{ $about->file_ge }}
                                        </a>
                                        <input type="text" name="file_ge" hidden value="{{ $about->file_ge or '' }}">
                                    </div>
                                @endif

                                <div class="col-md-12 mt-2">
                                    <label>ფაილის ატვირთვა (ქართული)</label>
                                    <div id="my-programs-file-ge-up"  class="dropzone">



                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                    </div>
                                </div>



                                @if(isset($form['id']))
                                    <div class="col-md-12 mt-5 mb-5">
                                        <label>ფაილი</label>
                                        <br>
                                        @if( $about->file_en!='')
                                            <a href="{{ url('admin/programs/delete') }}?id={{ $about->id }}&lang=en" class="btn btn-danger">წაშლა</a>
                                            <br>
                                        @endif
                                        <a href="{{ url('uploads/programs/') }}/{{ $about->file_en }}" target="_blank">
                                            {{ $about->file_en }}
                                        </a>
                                        <input type="text" name="file_en" hidden value="{{ $about->file_en or '' }}">
                                    </div>
                                @endif

                                <div class="col-md-12 mt-2">
                                    <label>ფაილის ატვირთვა (ინგლისური)</label>
                                    <div id="my-programs-file-en-up"  class="dropzone">



                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12 mt-3">
                                    <button class="btn btn-success">შენახვა</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection