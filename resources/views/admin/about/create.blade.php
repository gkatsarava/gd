@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="page-wrapper">


        <div class="container-fluid">
            <h3 class="page-title">ჩვენს შესახებ</h3>


            <div class="row">
                <div class="col-12">
                    <!-- Column -->
                    <div class="card">

                        <div class="card-body table-responsive">

                            <form method="{{ $form['method'] }}" action="{{ $form['action'] }}">

                                {{ csrf_field() }}

                                @if(isset($form['id']))
                                    {{ method_field('PUT') }}
                                @endif






                                <div class="col-md-12 mt-2">

                                    <label>პარტიის შესახებ ქართულად</label>
                                    <textarea name="desc_ge" id="mymce" class="form-control">{{ $about->desc_ge or '' }}</textarea>

                                </div>

                                <div class="col-md-12 mt-2">

                                    <label>პარტიის შესახებ ინგლისურად</label>
                                    <textarea name="desc_en" id="mymce" class="form-control">{{ $about->desc_en or '' }}</textarea>

                                </div>



                                <div class="col-md-12 mt-2">

                                    <label>პარტიის მიზანი ქართულად</label>
                                    <textarea name="goal_desc_ge" id="mymce" class="form-control">{{ $about->goal_desc_ge or '' }}</textarea>

                                </div>

                                <div class="col-md-12 mt-2">

                                    <label>პარტიის მიზანი ინგლისურად</label>
                                    <textarea name="goal_desc_en" id="mymce" class="form-control">{{ $about->goal_desc_en or '' }}</textarea>

                                </div>



                                @if(isset($form['id']))
                                    <div class="col-md-12 mt-5 mb-5">
                                        <label>ფაილი</label>
                                        <br>
                                        <a href="{{ url('uploads/about/') }}/{{ $about->file }}" target="_blank">
                                            {{ $about->file }}
                                        </a>
                                        <input type="text" name="file" hidden value="{{ $about->file or '' }}">
                                    </div>
                                @endif

                                <div class="col-md-12 mt-2">
                                    <label>ფაილის ატვირთვა</label>
                                    <div id="my-about-files"  class="dropzone">



                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                        </div>
                                    </div>
                                </div>



                                <div class="col-md-12 mt-3">
                                    <button class="btn btn-success">შენახვა</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @stop

        @section('javascript')
            <script>
                window.route_mass_crud_entries_destroy = '{{ route('admin.permissions.mass_destroy') }}';
            </script>
@endsection