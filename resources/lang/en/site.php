<?php

return [


    "home"=>'Home',
    "party"=>"Party",
    "about-party"=>"About party",
    "party-structure"=>"Party structure",
    "structure"=>"Structure",
    "political-council"=>"Political council",
    "biography"=>"Biography",
    "charter"=>"Charter",
    "news"=>"News",
    "elections2020"=>"Elections 2021",
    "candidates"=>"Candidates",
    "programe"=>'Tbilisi Programs',
    "majoritarians"=>"Majoritarians",
    "mayor"=>"Mayor",
    "proportional-list"=>"Proportional list",
    "world-media"=>"International Media",
    "media"=>"Media",
    "photo-gallery"=>"Photo gallery",
    "video-gallery"=>"Video gallery",
    "contact"=>"Contact",
    "search"=>"Search",
    "phone"=>"Phone",
    "e-mail"=>"E-mail",
    "address"=>"Address",
    "send-massage"=>"send letter",
    "georgian-dream-in-socmedia"=>"Georgian Dream party in social networks",
    "join-us"=>"Join us",
    "newsletter"=>"Subscribe to GD Chronicles",
    "your-email"=>"Your E-mail",
    "georgian-dream-Democratic"=>"Georgian Dream - Democratic Georgia",
    "all-rights-reserved"=>"All rights reserved ©2020",
    "share"=>"Share",
    "donation"=>"Donation",
    "youth"=>"Youth",
    "list-of-majoritarians"=>"Complete list of Majoritarians",
    "international-media-about-Georgia"=>"International media about Georgia",
    "like"=>"Like",
    "subscribe"=>"Subscribe",
    "learn-more"=>"Learn more",

    "our-goal"=>"Our goal",

    "party-charter"=>"Party Charter",
    "back"=>'Back',

    "education"=>"Education",

    "work-experience"=>"Work Experience",

    "download"=>"Download",
    "Tbilisi"=>'Tbilisi',

    "region"=>"Region",

    "head-office"=>"Head Office",
    "your-name"=>"Your name",

    "subject"=>"Subject",

    "message"=>"Message",
    "programs"=>"Programs",
    "regions_program"=>"Region Programs",
    "raioni"=>'District'
];