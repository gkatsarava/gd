<?php

if (session()->get('locale') === null)
    session()->put('locale', App::getLocale());

Route::get('locale/{locale}', function ($locale) {
    \Session::put('locale', $locale);

   // return redirect()->back()->with('lang', App::getLocale());
    $previousUrl = app('url')->previous();
    $exp=explode('?',$previousUrl);

    return redirect()->to($exp[0].'?'. http_build_query(['lang'=> $locale]));
});

Route::GET('transfer_data','ClientController@transfer_data');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');


Route::GET('/','ClientController@index');
Route::GET('/about-party','ClientController@about');
Route::GET('/party-structure','ClientController@party_structure');
Route::GET('/party-council','ClientController@party_council');
Route::GET('/party-regulation','ClientController@party_regulation');
Route::GET('/bulletin','ClientController@bulletin');
Route::GET('/news','ClientController@news');
Route::GET('/candidates','ClientController@candidates');
Route::GET('/mayor','ClientController@mayor');
Route::GET('/show-mayor/{id}/{param}','ClientController@show_mayor');

Route::GET('/region-program','ClientController@region_program');
Route::GET('/show-region-program/{id}/{param}','ClientController@show_region_program');



Route::GET('/program','ClientController@program');
Route::GET('/world-media','ClientController@world_media');
Route::GET('/show-world-media/{id}/{param}','ClientController@show_world_media');
Route::GET('/show-candidates/{id}/{param}','ClientController@show_candidates');
Route::GET('/show-structure/{id}/{param}','ClientController@show_structure');

Route::GET('/video-gallery','ClientController@video_gallery');
Route::get('/show-photo-gallery/{id}','ClientController@show_photo_gallery');
Route::GET('/photo-gallery','ClientController@photo_gallery');
Route::GET('/contact','ClientController@contact');
Route::GET('/contact_send','ClientController@contact_send');

Route::GET('/young','ClientController@young');
Route::GET('/transfers','ClientController@transfers');
Route::GET('/search','ClientController@search');
Route::GET('/show-bulletin/{id}/{param}','ClientController@show_bulletin');

Route::GET('/show-news/{id}/{param}','ClientController@show_news');
Route::GET('/proportional','ClientController@proportional');
Route::GET('/show-bio/{id}/{param}','ClientController@show_bio');


Route::GET('/programs','ClientController@programs');


View::composer('website.layouts.app', function($view){
    $settings=\App\Models\DefaultSettingsModel::first();
    $view->with('settings',$settings);
});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);


    Route::POST('proportional_list/delete_proportional_list_picture','Admin\ProportionalController@delete_proportional_list_picture');
    Route::post('proportional_list/uploadFiles','Admin\ProportionalController@uploadFiles');
    Route::resource('proportional_list','Admin\ProportionalController');

    Route::POST('majoritarians/delete_majoritarians_picture','Admin\MajoritariansController@delete_majoritarians_picture');
    Route::post('majoritarians/uploadFiles','Admin\MajoritariansController@uploadFiles');
    Route::resource('majoritarians','Admin\MajoritariansController');


    Route::POST('mayor/delete_mayor_picture','Admin\mayorController@delete_mayor_picture');
    Route::post('mayor/uploadFiles','Admin\mayorController@uploadFiles');
    Route::resource('mayor','Admin\mayorController');


    Route::POST('region/get_districts','Admin\RegionController@get_districts');
    Route::POST('region/delete_district','Admin\RegionController@delete_district');
    Route::POST('region/update_district','Admin\RegionController@update_district');
    Route::POST('region/edit_district','Admin\RegionController@edit_district');
    Route::POST('region/save_district','Admin\RegionController@save_district');
    Route::get('region/add_district/{id}','Admin\RegionController@add_district');
    Route::resource('region','Admin\RegionController');

    Route::resource('default_settings','Admin\DefaultSettingsController');

    Route::POST('political_party/Sort','Admin\PoliticalPartyController@Sort');
    Route::POST('political_party/delete_politican_party_picture','Admin\PoliticalPartyController@delete_politican_party_picture');
    Route::POST('political_party/delete_other_position','Admin\PoliticalPartyController@delete_other_position');
    Route::POST('political_party/update_other_position','Admin\PoliticalPartyController@update_other_position');
    Route::POST('political_party/edit_other_position','Admin\PoliticalPartyController@edit_other_position');
    Route::POST('political_party/save_other_position','Admin\PoliticalPartyController@save_other_position');
    Route::GET('political_party/add_positions/{id}','Admin\PoliticalPartyController@add_positions');
    Route::post('political_party/uploadFiles','Admin\PoliticalPartyController@uploadFiles');
    Route::resource('political_party','Admin\PoliticalPartyController');

    Route::post('rules/uploadFiles','Admin\RulesController@uploadFiles');
    Route::resource('rules','Admin\RulesController');

    Route::post('about/uploadFiles','Admin\AboutController@uploadFiles');
    Route::resource('about','Admin\AboutController');


    Route::POST('media/delete_picture','Admin\MediaController@delete_picture');
    Route::POST('media/pin_home','Admin\MediaController@pin_home');
    Route::post('media/uploadFiles','Admin\MediaController@uploadFiles');
    Route::resource('media','Admin\MediaController');


    Route::POST('bulletin/pause-news','Admin\BulletinController@pause_news');
    Route::POST('bulletin/pin_to_slide','Admin\BulletinController@pin_to_slide');
    Route::POST('bulletin/delete_picture','Admin\BulletinController@delete_picture');
    Route::POST('bulletin/delete_attachment','Admin\BulletinController@delete_attachment');
    Route::POST('bulletin/pin_home','Admin\BulletinController@pin_home');
    Route::post('bulletin/uploadFiles','Admin\BulletinController@uploadFiles');
    Route::post('bulletin/uploadNewFiles','Admin\BulletinController@uploadNewFiles');
    Route::resource('bulletin','Admin\BulletinController');




    Route::POST('news/pause-news','Admin\NewsController@pause_news');
    Route::POST('news/pin_to_slide','Admin\NewsController@pin_to_slide');
    Route::POST('news/delete_picture','Admin\NewsController@delete_picture');
    Route::POST('news/delete_attachment','Admin\NewsController@delete_attachment');
    Route::POST('news/pin_home','Admin\NewsController@pin_home');
    Route::post('news/uploadFiles','Admin\NewsController@uploadFiles');
    Route::post('news/uploadNewFiles','Admin\NewsController@uploadNewFiles');
    Route::resource('news','Admin\NewsController');



    Route::POST('region_programe/pause-news','Admin\RegionPrograme@pause_news');
    Route::POST('region_programe/pin_home','Admin\RegionPrograme@pin_home');

    Route::POST('region_programe/delete_picture','Admin\RegionPrograme@delete_picture');
    Route::POST('region_programe/delete_attachment','Admin\RegionPrograme@delete_attachment');
    Route::post('region_programe/uploadFiles','Admin\RegionPrograme@uploadFiles');
    Route::post('region_programe/uploadNewFiles','Admin\RegionPrograme@uploadNewFiles');
    Route::resource('region_programe','Admin\RegionPrograme');


    Route::post('about/uploadFiles','Admin\AboutController@uploadFiles');
    Route::post('about/uploadHeaderFiles','Admin\AboutController@UploadHeaderFile');
    Route::resource('about','Admin\AboutController');

    Route::post('slider/uploadFiles','Admin\SliderController@uploadFiles');
    Route::resource('slider','Admin\SliderController');


    Route::post('transfers/uploadFiles','Admin\TransfersController@uploadFiles');
    Route::POST('transfers/uploadIconFiles','Admin\TransfersController@uploadIconFiles');
    Route::POST('transfers/delete_file','Admin\TransfersController@delete_file');
    Route::POST('transfers/delete_icon','Admin\TransfersController@delete_icon');
    Route::resource('transfers','Admin\TransfersController');


    Route::post('young/uploadFiles','Admin\YoungController@uploadFiles');
    Route::resource('young','Admin\YoungController');

    Route::POST('photo_gallery/delete_picture','Admin\PhotoGalleryController@delete_picture');
    Route::post('photo_gallery/uploadFiles','Admin\PhotoGalleryController@uploadFiles');
    Route::resource('photo_gallery','Admin\PhotoGalleryController');



    Route::resource('video_gallery','Admin\VideoGalleryController');

    Route::get('programs/delete','Admin\ProgramsController@delete');
    Route::post('programs/uploadFiles','Admin\ProgramsController@uploadFiles');
    Route::resource('programs','Admin\ProgramsController');

});
