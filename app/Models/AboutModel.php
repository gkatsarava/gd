<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\App;


class AboutModel extends Model
{
    protected $appends = [
        'desc',
        'goal'
    ];




    protected $table='about';

    public function getDescAttribute(){

        $langName = 'desc_'.App::getLocale();

        return $this->$langName;
    }
    public function getGoalAttribute(){

        $langName = 'goal_desc_'.App::getLocale();

        return $this->$langName;
    }

}
