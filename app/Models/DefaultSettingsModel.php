<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class DefaultSettingsModel extends Model
{
    protected $appends = [
        'desc',
        'address'
    ];




    protected $table='default_settings';

    public function getDescAttribute(){

        $langName = 'desc_'.App::getLocale();

        return $this->$langName;
    }
    public function getAddressAttribute(){

        $langName = 'address_'.App::getLocale();

        return $this->$langName;
    }
}
