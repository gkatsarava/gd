<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class RegionModel extends Model
{
    protected $appends = [
        'name'
    ];




    protected $table='region';

    public function getNameAttribute(){

        $langName = 'name_'.App::getLocale();

        return $this->$langName;
    }
    public function District(){
        return $this->hasMany('App\Models\DistrictModel','region_id','id');
    }
}
