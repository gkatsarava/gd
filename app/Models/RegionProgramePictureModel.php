<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\SoftDeletes;


class RegionProgramePictureModel extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='region_pictures';
}
