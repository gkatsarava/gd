<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class RulesModel extends Model
{
    protected $table='rules';
    protected $appends = [
        'desc',
        'title',
        'name'
    ];

    public function getDescAttribute(){

        $langName = 'desc_'.App::getLocale();

        return $this->$langName;
    }

    public function getTitleAttribute(){

        $langName = 'title_'.App::getLocale();

        return $this->$langName;
    }
    public function getNameAttribute(){

        $langName = 'name_'.App::getLocale();

        return $this->$langName;
    }
}
