<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class ProgramsModel extends Model
{
    protected $table='programs';
    protected $appends = [
        'file',
    ];
    public function getDescAttribute(){

        $langName = 'file_'.App::getLocale();

        return $this->$langName;
    }
}
