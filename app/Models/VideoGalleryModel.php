<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class VideoGalleryModel extends Model
{
    protected $appends = [
        'title'
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='video_gallery';

    public function getTitleAttribute(){

        $langName = 'title_'.App::getLocale();

        return $this->$langName;
    }

    public function User(){
        return $this->hasOne('App\User','id','user_id');
    }
}
