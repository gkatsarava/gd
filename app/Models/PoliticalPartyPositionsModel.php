<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\App;

class PoliticalPartyPositionsModel extends Model
{
    protected $appends = [
        'name',
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='political_party_positions';

    public function User(){
        return $this->hasOne('App\User','id','user_id');
    }
    public function PoliticalParty(){
        return $this->hasOne('App\PoliticalPartyModel','id','political_party_id');
    }

    public function getNameAttribute(){

        $langName = 'name_'.App::getLocale();

        return $this->$langName;
    }
}
