<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\App;

class NewsPicturesModel extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='news_pictures';
}
