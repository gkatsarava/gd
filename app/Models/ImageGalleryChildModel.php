<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\App;

class ImageGalleryChildModel extends Model
{

    protected $appends = [
        'desc',
        'title',
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='picture_gallery_childs';
    public function gallery(){
        return $this->hasOne('App\Models\ImageGalleryModel','id','gallery_id');
    }

}
