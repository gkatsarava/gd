<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\App;

class ImageGalleryModel extends Model
{
    protected $appends = [
        'desc',
        'title',
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='picture_gallery';

    public function User(){
        return $this->hasOne('App\User','id','user_id');
    }
    public function getDescAttribute(){

        $langName = 'desc_'.App::getLocale();

        return $this->$langName;
    }

    public function getTitleAttribute(){

        $langName = 'title_'.App::getLocale();

        return $this->$langName;
    }
    public function Pictures(){
        return $this->hasMany('App\Models\ImageGalleryChildModel','gallery_id','id');
    }
}
