<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MediaPicturesModel extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='media_pictures';
}
