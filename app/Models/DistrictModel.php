<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\App;

class DistrictModel extends Model
{

    protected $appends = [
        'name'
    ];


    use SoftDeletes;
    protected $dates = ['deleted_at'];




    protected $table='district';

    public function User(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function getNameAttribute(){

        $langName = 'name_'.App::getLocale();

        return $this->$langName;
    }
}
