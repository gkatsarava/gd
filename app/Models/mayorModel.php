<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class mayorModel extends Model
{
    protected $appends = [
        'desc',
        'title',
        'name',
        'location',
        'work'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='mayor';


    public function User(){
        return $this->hasOne('App\User','id','user_id');
    }
    public function getDescAttribute(){

        $langName = 'desc_'.App::getLocale();

        return $this->$langName;
    }

    public function getNameAttribute(){

        $langName = 'name_'.App::getLocale();

        return $this->$langName;
    }

    public function getTitleAttribute(){

        $langName = 'title_'.App::getLocale();

        return $this->$langName;
    }

    public function getLocationAttribute(){

        $langName = 'location_'.App::getLocale();

        return $this->$langName;
    }
    public function getWorkAttribute(){

        $langName = 'work_'.App::getLocale();

        return $this->$langName;
    }
    public function District(){
        return $this->hasMany('App\Models\DistrictModel','id','district_id');
    }
    public function Region(){
        return $this->hasOne('App\Models\RegionModel','id','region_id');
    }

}
