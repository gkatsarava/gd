<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\App;

class PoliticalPartyModel extends Model
{
    protected $appends = [
        'desc',
        'title',
        'name',
        'position',
        'education',
        'work'
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='political_party';


    public function User(){
        return $this->hasOne('App\User','id','user_id');
    }
    public function getDescAttribute(){

        $langName = 'desc_'.App::getLocale();

        return $this->$langName;
    }

    public function getTitleAttribute(){

        $langName = 'title_'.App::getLocale();

        return $this->$langName;
    }
    public function getNameAttribute(){

        $langName = 'name_'.App::getLocale();

        return $this->$langName;
    }
    public function getPositionAttribute(){

        $langName = 'position_'.App::getLocale();

        return $this->$langName;
    }
    public function getEducationAttribute(){

        $langName = 'education_'.App::getLocale();

        return $this->$langName;
    }
    public function getWorkAttribute(){

        $langName = 'work_'.App::getLocale();

        return $this->$langName;
    }
    public function PoliticalPartyPositions(){
        return $this->hasMany('App\Models\PoliticalPartyPositionsModel','political_party_id','id');
    }
}
