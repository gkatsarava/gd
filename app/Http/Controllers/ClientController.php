<?php

namespace App\Http\Controllers;

use App\Models\AboutModel;
use App\Models\BulletinModel;
use App\Models\BulletinPicturesModel;
use App\Models\CityModel;
use App\Models\DefaultSettingsModel;
use App\Models\ImageGalleryChildModel;
use App\Models\ImageGalleryModel;
use App\Models\MajoritariansModel;
use App\Models\mayorModel;

use App\Models\MediaModel;
use App\Models\NewsModel;
use App\Models\NewsPicturesModel;
use App\Models\RegionProgrameModel;



use App\Models\PoliticalPartyModel;
use App\Models\ProgramsModel;
use App\Models\ProportionalModel;
use App\Models\RegionModel;
use App\Models\DistrictModel;

use App\Models\RulesModel;
use App\Models\TransferModel;
use App\Models\VideoGalleryModel;
use App\Models\YoungModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    public function __construct(){
        App::setLocale(session()->get('locale'));
    }
    public function index(){

        if(App::getLocale()=='ge') {
            $data['news']=NewsModel::orderBy('created_at','DESC')->where('title_ge','<>','')->orderBy('id','DESC')->where('status','0')->limit(2)->get();
        }
        elseif(App::getLocale()=='en') {
            $data['news']=NewsModel::orderBy('created_at','DESC')->where('title_en','<>','')->orderBy('id','DESC')->where('status','0')->limit(2)->get();
        }else{
            $data['news']=NewsModel::orderBy('created_at','DESC')->orderBy('id','DESC')->where('status','0')->limit(2)->get();
        }

        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }



        if(App::getLocale()=='ge') {
            $data['slider']=NewsModel::where('slider','1')
                ->orderBy('created_at','DESC')
                ->orderBy('id','DESC')
                ->where('status','0')
                ->where('title_ge','<>','')
                ->get();
        }
        elseif(App::getLocale()=='en') {
            $data['slider']=NewsModel::where('slider','1')
                ->orderBy('created_at','DESC')
                ->orderBy('id','DESC')
                ->where('status','0')
                ->where('title_en','<>','')
                ->get();
        }

        $data['media']=MediaModel::orderBy('created_at','DESC')->limit(1)->get();
        $data['majoritarians']=MajoritariansModel::limit(3)->get();
        return view('website.index',$data);
    }

    public function about(){
        $data['about']=AboutModel::first();
        return view('website.about',$data);
    }
    public function party_structure(){
        $data['party_structure']=PoliticalPartyModel::where('structure','2')->orderBy('sort','ASC')->paginate(50);
        return view('website.party_structure',$data);
    }
    public function party_council(){
        $data['party_structure']=PoliticalPartyModel::where('structure','1')->orderBy('sort','ASC')->paginate(50);
        return view('website.party_council',$data);
    }
    public function party_regulation(){
        $data['regulation']=RulesModel::first();
        return view('website.party_regulation',$data);
    }
    public function news(){
        $news=NewsModel::where('status','0')->orderBy('created_at','DESC');
        $data['cnt']=$news->count();
        if(App::getLocale()=='ge') {
            $data['news'] = $news->where('title_ge','<>','')->paginate(20);
        }
        elseif(App::getLocale()=='en') {
            $data['news'] = $news->where('title_en','<>','')->paginate(20);
        }else{
            $data['news'] = $news->paginate(20);
        }

        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        return view('website.news',$data);
    }
    public function bulletin(){
        $news=BulletinModel::where('status','0')->orderBy('created_at','DESC');
        $data['cnt']=$news->count();
        $data['news']=$news->paginate(20);
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        return view('website.bulletin',$data);
    }
    public function candidates(Request $request){
        if(!isset($request->region)){
            $data['majoritarians']=MajoritariansModel::where('region_id','3')->paginate(50);
        }else{

            $data['majoritarians']=MajoritariansModel::where('region_id',$request->region)->paginate(50);
            $data['city']=DistrictModel::where('region_id',$request->region)->get();
        }
        $data['city']=DistrictModel::where('region_id',$request->region)->get();


        $data['regions']=RegionModel::where('name_ge','<>','თბილისი')->get();
        return view('website.candidates',$data);
    }

    public function mayor(Request $request){
        if(!isset($request->region)){
            $data['majoritarians']=mayorModel::where('region_id','3')->paginate(50);
        }else{
            $data['majoritarians']=mayorModel::where('region_id',$request->region)->paginate(50);
        }

        $data['regions']=RegionModel::where('name_ge','<>','თბილისი')->get();
        return view('website.mayor',$data);
    }

    public function show_candidates(Request $request){
        $data['data']=MajoritariansModel::where('id',$request->id)->first();
        return view('website.show_candidates',$data);
    }

    public function show_mayor(Request $request){
        $data['data']=mayorModel::where('id',$request->id)->first();
        return view('website.show_mayor',$data);
    }

    public function programs(){
        $data['programs']=ProgramsModel::first();
        return view('website.programs',$data);
    }
    public function world_media(){
        return;
        $data['news']=MediaModel::paginate(20);
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        return view('website.world_media',$data);
    }
    public function show_world_media(Request $request){
        return;
        $data['news']=MediaModel::where('id',$request->id)->first();
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        return view('website.show_world_media',$data);
    }
    public function video_gallery(){
        $data['gallery']=VideoGalleryModel::paginate(20);
        return view('website.video_gallery',$data);
    }

    public function photo_gallery(){
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        $data['gallery']=ImageGalleryModel::paginate(10);
        return view('website.photo_gallery',$data);
    }

    public function show_photo_gallery(Request $request){
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        $data['gallery']=ImageGalleryModel::where('id',$request->id)->first();
        return view('website.show_photo_gallery',$data);
    }
    public function contact(){
        $data['settings']=DefaultSettingsModel::first();
        return view('website.contact',$data);
    }
    public function contact_send(){
        $data['settings']=DefaultSettingsModel::first();
        return view('website.contact_send_box',$data);
    }

    public function young(){
        $data['data']=YoungModel::first();
        return view('website.young',$data);
    }
    public function transfers(){
        $data['transfers']=TransferModel::paginate(20);
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        return view('website.transfers',$data);
    }
    public function search(Request $request){

        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        $data['news']=array();
        $desc='desc_'.App::getLocale();
        $title='title_'.App::getLocale();

        if(App::getLocale()=='ge') {
            $news=NewsModel::where('status','0')
                ->where(function($query) use ($desc,$request) {
                    $query->where('title_ge','like','%'.$request->search.'%')
                        ->orwhere($desc,'like','%'.$request->search.'%')
                        ->where('title_ge','<>','');
                });

        }
        elseif(App::getLocale()=='en') {
            $news=NewsModel::where('status','0')
                ->where(function($query) use ($desc,$request) {
                    $query->where('title_en','like','%'.$request->search.'%')
                        ->orwhere($desc,'like','%'.$request->search.'%')
                        ->where('title_en','<>','');
                });
            //$news=NewsModel::where('status','0')->where('title_en','<>','')->where('title_en','like','%'.$request->search.'%')->where($desc,'like','%'.$request->search.'%');

        }
       // $media=MediaModel::where($desc,'like','%'.$request->search.'%');

        //array_push($data['news'],$news->get());
       // array_push($data['news'],$media->get());

        $data['cnt']=$news->count();

        $data['news']=$news->paginate(20);


        return view('website.search',$data);
    }

    public function show_news(Request $request){
       if(isset($request->lang)){
          \Session::put('locale', $request->lang);

        }
       if(App::getLocale()!=$request->lang){
            return redirect(url('locale/').'/'.$request->lang);
        }



        $data['news']=NewsModel::where('status','0')->where('id',$request->id)->first();

        if($data['news']->title==''){
            return redirect(url('news'));
        }
        if($data['news']->desc==''){
            return redirect(url('news'));
        }
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }



        return view('website.show_news',$data);
    }

    public function region_program(){
        $news=RegionProgrameModel::where('status','0')->orderBy('created_at','DESC');
        $data['cnt']=$news->count();
        if(App::getLocale()=='ge') {
            $data['news'] = $news->where('title_ge','<>','')->paginate(20);
        }
        elseif(App::getLocale()=='en') {
            $data['news'] = $news->where('title_en','<>','')->paginate(20);
        }else{
            $data['news'] = $news->paginate(20);
        }

        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }
        return view('website.region_program',$data);
    }
    public function show_region_program(Request $request){
        if(isset($request->lang)){
            \Session::put('locale', $request->lang);

        }
        if(App::getLocale()!=$request->lang){
            return redirect(url('locale/').'/'.$request->lang);
        }



        $data['news']=RegionProgrameModel::where('status','0')->where('id',$request->id)->first();

        if($data['news']->title==''){
            return redirect(url('news'));
        }
        if($data['news']->desc==''){
            return redirect(url('news'));
        }
        if(App::getLocale()=='ge'){
            $data['month']=array(
                "01"=>'იანვარი',
                "02"=>'თებერვალი',
                "03"=>'მარტი',
                "04"=>'აპრილი',
                "05"=>'მაისი',
                "06"=>'ივნისი',
                "07"=>'ივლისი',
                "08"=>'აგვისტო',
                "09"=>'სექტემბერი',
                "10"=>'ოქტომბერი',
                "11"=>'ნოემბერი',
                "12"=>'დეკემბერი'
            );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }



        return view('website.show_region_program',$data);
    }
 

    public function show_bulletin(Request $request){
        if(isset($request->lang)){
            \Session::put('locale', $request->lang);

        }
        if(App::getLocale()!=$request->lang){
            return redirect(url('locale/').'/'.$request->lang);
        }



        $data['news']=BulletinModel::where('status','0')->where('id',$request->id)->first();
        if(App::getLocale()=='ge'){
        $data['month']=array(
            "01"=>'იანვარი',
            "02"=>'თებერვალი',
            "03"=>'მარტი',
            "04"=>'აპრილი',
            "05"=>'მაისი',
            "06"=>'ივნისი',
            "07"=>'ივლისი',
            "08"=>'აგვისტო',
            "09"=>'სექტემბერი',
            "10"=>'ოქტომბერი',
            "11"=>'ნოემბერი',
            "12"=>'დეკემბერი'
        );
        }
        else{
            $data['month']=array(
                "01"=>'Jan',
                "02"=>'Feb',
                "03"=>'Mar',
                "04"=>'Apr',
                "05"=>'May',
                "06"=>'Jun',
                "07"=>'Jul',
                "08"=>'Aug',
                "09"=>'Sep',
                "10"=>'Oct',
                "11"=>'Nov',
                "12"=>'Dec'
            );
        }



        return view('website.show_bulletin',$data);
    }
    public function proportional(){
        $data['proportional']=ProportionalModel::get();
        return view('website.proportional',$data);
    }
    public function show_structure(Request $request){
        $data['data']=PoliticalPartyModel::where('id',$request->id)->first();
        return view('website.show_structure',$data);
    }
    public function show_bio(Request $request){
        $data['data']=PoliticalPartyModel::where('id',$request->id)->first();
        return view('website.show_bio',$data);
    }
    public function program(Request $request){
        $data['programs']=ProgramsModel::first();
        return view('website.programs',$data);
    }
    public function transfer_data(){

        $news=DB::connection('MYSQL_DB_PRODUCTION_DATA')
            ->table('blog')
            ->where('status','1')
            //->where('news_id','>','1217')
            ->get();

        foreach($news as $r){

            /*$old_news = DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('blog_description')
                ->where('blog_id', $r->blog_id)
                ->where('language_id', '2')
                ->first();



            $n = BulletinModel::where('old_web_id', $old_news->blog_id)->first();
            $n->title_en = @$old_news->title;
            $n->desc_en = @htmlspecialchars_decode($old_news->description);
            $n->save();*/

            $bullet=BulletinModel::where('old_web_id',$r->blog_id)->get();
            foreach($bullet as $row){
                $news = new BulletinPicturesModel();
                    $news->picture = str_replace('news/','',str_replace('photo/','',str_replace('blog/','',str_replace('data/','',$r->image))));
                    $news->news_id=$row->id;
                $news->save();
            }


        }



      // die();
       // $news=NewsModel::where('id','>=','1182')->get();

       /* foreach($news as $row){
            $old=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news')
                ->where('news_id',$row->id)
                ->first();
            if(@$old->video==''){
                $video='';
            }else{
                $video='https://www.youtube.com/watch?v='.$old->video;
            }
            $n=NewsModel::where('id',$row->id)->first();
            $n->youtube=$video;
            $n->save();

        }*/


      /*  foreach($news as $row) {
              $old_news=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                   ->table('news')
                   ->where('news_id',$row->old_web_id)
                   ->first();

               $pic=new NewsPicturesModel();
               $pic->picture=str_replace('news/','',str_replace('photo/','',str_replace('data/','',$old_news->image)));
               $pic->news_id=$row->id;
               $pic->save();

           }*/
          /* $n = NewsModel::where('id', $row->id)->first();
            $n->status = '1';

            $n->save();

            $old_news = DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news_description')
                ->where('news_id', $row->old_web_id)
                ->where('language_id', '2')
                ->first();

            // echo htmlspecialchars_decode($old_news->description);

            $n = NewsModel::where('id', $row->id)->first();
            $n->title_en = @$old_news->title;
            $n->desc_en = @htmlspecialchars_decode($old_news->description);
            $n->save();

        }
*/

      /*  $news=NewsModel::get();
        foreach($news as $row){
            $news_picture=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news_image')
                ->where('news_id',$row->old_web_id)
                ->get();
            $nnn=DB::connection('MYSQL_DB_PRODUCTION_DATA')->table('news')->where('news_id',$row->old_web_id)->first();
            foreach($news_picture as $picture){
                $pic=new NewsPicturesModel();
                $pic->picture=str_replace('news/','',str_replace('photo/','',str_replace('data/','',$picture->image)));
                $pic->news_id=$row->id;
                if(str_replace('news/','',str_replace('photo/','',str_replace('data/','',$nnn->image)))==str_replace('news/','',str_replace('photo/','',str_replace('data/','',$picture->image)))){
                    $pic->first_picture='1';
                }else{
                    $pic->first_picture='0';
                }
                $pic->save();
            }
        }*/

      /*  $news=DB::connection('MYSQL_DB_PRODUCTION_DATA')
            ->table('news_image')
            ->get();
        foreach($news as $row){
            @copy('http://gd.ge/image/'.str_replace(' ','%20',$row->image), public_path('uploads/news/'.'/'.str_replace('news/','',str_replace('photo/','',str_replace('data/','',$row->image)))));

        }*/


        /*$news=NewsModel::get();
        foreach($news as $row){
            $old=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news')
                ->where('news_id',$row->id)
                ->first();
            if(@$old->video==''){
                $video='';
            }else{
                $video='https://www.youtube.com/watch?v='.$old->video;
            }
            $n=NewsModel::where('id',$row->id)->first();
                $n->youtube=$video;
            $n->save();

        }*/
        /*foreach($news as $row){
            $desc=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news_description')
                ->where('news_id',$row->old_web_id)
                ->where('language_id','2')
                ->first();

            $n=NewsModel::where('id',$row->id)->first();

           $n->title_en=@$desc->title;
            $n->desc_en=@$desc->title;
            $n->save();
        }*/



/*        $news=DB::connection('MYSQL_DB_PRODUCTION_DATA')
            ->table('blog')
            ->where('status','1')
            //->where('news_id','>','1217')
            ->get();*/
      /*  foreach($news as $row) {

            $news = new BulletinModel();
            $news->old_web_id = $row->blog_id;
            $news->created_at = $row->date;
            $news->status = '1';
            $news->save();
        }*/

           /* $desc=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news_description')
                ->where('news_id',$row->news_id)
                ->get();*/
           /* $image=DB::connection('MYSQL_DB_PRODUCTION_DATA')
                ->table('news_image')
                ->where('news_id',$row->news_id)
                ->get();*/
           /* foreach($desc as $r){

            }*/
       // }
        //dd($news);
    }
}
