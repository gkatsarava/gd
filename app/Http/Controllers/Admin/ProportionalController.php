<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProportionalModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProportionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $political_party=ProportionalModel::orderby('id','desc');

        if(isset($request->fname) && $request->fname!=''){
            $political_party->where('name_ge','like','%'.$request->fname.'%');
        }

        if(isset($request->number) && $request->number!=''){
            $political_party->where('number','=',$request->number);
        }






        $data['political_party']=$political_party->paginate(15);
        return view('admin.proportional_list.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/proportional_list'
            ]
        ];
        return view('admin.proportional_list.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

         /*   'image'=>'required',
            'name_ge'=>'required',*/

        ]);



        $political_party=new ProportionalModel();
      /*  $political_party->name_ge=$request->name_ge;
        $political_party->name_en=$request->name_en;




        $political_party->number=$request->number;

        $political_party->work_ge=$request->work_ge;
        $political_party->work_en=$request->work_en;
        $political_party->education_ge=$request->education_ge;
        $political_party->education_en=$request->education_en;
        $political_party->picture=$request->image;
        $political_party->facebook=$request->facebook;
        $political_party->twitter=$request->twitter;
        $political_party->instagram=$request->instagram;
        $political_party->youtube=$request->youtube;*/
        $political_party->desc_ge=$request->proportional_list_ge;
        $political_party->desc_en=$request->proportional_list_en;
        $political_party->user_id=Auth::user()->id;
        $political_party->save();


        return redirect(url('admin/proportional_list'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/proportional_list/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=ProportionalModel::where('id',$id)->first();
        return view('admin.proportional_list.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

           /* 'image'=>'required',
            'name_ge'=>'required',*/

        ]);



        $political_party=ProportionalModel::where('id',$id)->first();
       /* $political_party->name_ge=$request->name_ge;
        $political_party->name_en=$request->name_en;




        $political_party->number=$request->number;

        $political_party->work_ge=$request->work_ge;
        $political_party->work_en=$request->work_en;
        $political_party->education_ge=$request->education_ge;
        $political_party->education_en=$request->education_en;
        $political_party->picture=$request->image;
        $political_party->facebook=$request->facebook;
        $political_party->twitter=$request->twitter;
        $political_party->instagram=$request->instagram;
        $political_party->youtube=$request->youtube;
        $political_party->user_id=Auth::user()->id;*/
        $political_party->desc_ge=$request->proportional_list_ge;
        $political_party->desc_en=$request->proportional_list_en;
        $political_party->user_id=Auth::user()->id;
        $political_party->save();


        return redirect(url('admin/proportional_list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=ProportionalModel::where('id',$id)->delete();
        return back();
    }

    public function uploadFiles(Request $request){
        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/proportional_list'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }

    public function delete_proportional_list_picture(Request $request){
        $data=ProportionalModel::where('id',$request->id)->first();
        $data->picture='';
        $data->save();

    }
}
