<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProgramsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['about']=ProgramsModel::first();
        return view('admin.programs.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/programs/'.$id,
                'id'=>$id

            ]
        ];
        $data['about']=ProgramsModel::where('id',$id)->first();
        return view('admin.programs.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [




        ]);
        $about=ProgramsModel::where('id',$id)->first();

        $about->file_ge=$request->file_ge;
        $about->file_en=$request->file_en;
        $about->save();
        return redirect()->intended('admin/programs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function uploadFiles(Request $request){
        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/programs'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];
    }
    public function delete(Request $request){
        $update=ProgramsModel::where('id',$request->id)->first();
            if($request->lang=='ge'){
                $update->file_ge='';
            }
            if($request->lang=='en'){
                $update->file_en='';
            }
        $update->save();
        return back();
    }
}
