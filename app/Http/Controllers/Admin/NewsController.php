<?php

namespace App\Http\Controllers\Admin;

use App\Models\NewsModel;
use App\Models\NewsPicturesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    public function index(Request $request){
        if($request->slider==1){
            $data['partner']=NewsModel::where('slider',$request->slider)->orderBy('id','DESC')->paginate(15);
        }else{
            $data['partner']=NewsModel::orderBy('id','DESC')->paginate(15);
        }
        return view('admin.news.index',$data);
    }
    public function show(){

    }
    public function create(){
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/news'
            ]
        ];
        return view('admin.news.create',$data);

    }
    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/news'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function uploadNewFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/news_attachment'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }

    public function edit($id){
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/news/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=NewsModel::where('id',$id)->first();
        return view('admin.news.create',$data);
    }
    public function update(Request $request,$id){

      //  dd($request);

        $exp=explode(':',$request->created_at_time);




        $this->validate($request, [


            'image'=>'required'

        ]);
        $news=NewsModel::where('id',$id)->first();


        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        if($request->created_at_time!=''){
            $news->created_at=$request->created_at.' '.$request->created_at_time.(isset($exp[2]) ? "" : ":00");
        }else{
            $news->created_at=$request->created_at;
        }
        $news->user_id=Auth::user()->id;
        $news->file=$request->file;
        $news->save();


        if(isset($request->image)){
            foreach($request->image as $row){
                $check=NewsPicturesModel::where('picture',$row)->first();
                if(!$check){
                    $news_picture=new NewsPicturesModel();
                    $news_picture->picture=$row;
                    $news_picture->news_id=$news->id;
                    $news_picture->save();
                }
            }
        }
        return redirect()->intended('admin/news');
    }
    public function store(Request $request){




        $this->validate($request, [


            'image'=>'required',
            'checkedfirst'=>'required'

        ]);
        $news=new NewsModel();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        //Checking
        if($request->created_at_time!=''){
            $news->created_at=$request->created_at.' '.$request->created_at_time.':00';
        }else{
            $news->created_at=$request->created_at.' '.date('G:i:s');
        }
        $news->user_id=Auth::user()->id;
        $news->file=$request->file;
        $news->save();

        foreach($request->image as $row){
            $news_picture=new NewsPicturesModel();
            $news_picture->picture=$row;
            $news_picture->news_id=$news->id;



            $news_picture->save();
        }
        $news_picture_update=NewsPicturesModel::where('picture',$request->checkedfirst)->first();
        $news_picture_update->first_picture='1';
        $news_picture_update->save();
        return redirect()->intended('admin/news');
    }
    public function destroy(Request $request,$id){

        NewsModel::where('id',$id)->delete();
        return back();

    }
    public function delete_picture(Request $request){
        $news=NewsPicturesModel::where('id',$request->id)->delete();
    }
    public function delete_attachment(Request $request){
        $news=NewsModel::where('id',$request->id)->first();
            $news->file='';
        $news->save();
    }
    public function pin_home(Request $request){

        $news_picture=NewsPicturesModel::where('id',$request->id)->first();

        DB::table('news_pictures')
            ->where('news_id', $news_picture->news_id)
            ->update(['first_picture' => 0]);


            $news_picture->first_picture='1';
        $news_picture->save();
    }
    public function pin_to_slide(Request $request){
        $news=NewsModel::where('id',$request->id)->first();
            $news->slider=$request->status;
        $news->save();
    }
    public function pause_news(Request $request){
        $news=NewsModel::where('id',$request->id)->first();
        $news->status=$request->status;
        $news->save();
    }
}
