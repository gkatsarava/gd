<?php

namespace App\Http\Controllers\Admin;

use App\Models\TransferModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TransfersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['transfers']=TransferModel::paginate(20);
        return view('admin.transfers.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/transfers'
            ]
        ];
        return view('admin.transfers.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [

            'title_ge'=>'required',
            'file'=>'required',

        ]);
        $transfer=new TransferModel();

        $transfer->title_ge=$request->title_ge;
        $transfer->title_en=$request->title_en;
        $transfer->desc_ge=$request->desc_ge;
        $transfer->desc_en=$request->desc_en;
        $transfer->user_id=Auth::user()->id;
        $transfer->file=$request->file;
        $transfer->icon=$request->image;
        $transfer->save();

        return redirect()->intended('admin/transfers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/transfers/'.$id,
                'id'=>$id

            ]
        ];

        $data['transfer']=TransferModel::where('id',$id)->first();
        return view('admin.transfers.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'title_ge'=>'required',
            'file'=>'required',

        ]);
        $transfer=TransferModel::where('id',$id)->first();

        $transfer->title_ge=$request->title_ge;
        $transfer->title_en=$request->title_en;
        $transfer->desc_ge=$request->desc_ge;
        $transfer->desc_en=$request->desc_en;
        $transfer->user_id=Auth::user()->id;
        $transfer->file=$request->file;
        $transfer->icon=$request->image;
        $transfer->save();

        return redirect()->intended('admin/transfers');
    }

    public function delete_file(Request $request){
            $transfers=TransferModel::where('id',$request->id)->first();
                $transfers->file='';
            $transfers->save();

    }

    public function delete_icon(Request $request){
        $transfers=TransferModel::where('id',$request->id)->first();
        $transfers->icon='';
        $transfers->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transfer=TransferModel::where('id',$id)->delete();
        return redirect()->intended('admin/transfers');
    }
    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/transfers'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function uploadIconFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/transfers_icon'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
}
