<?php

namespace App\Http\Controllers\Admin;

use App\Models\DistrictModel;
use App\Models\RegionModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['region']=RegionModel::paginate(15);
        return view('admin.region.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/region'
            ]
        ];
        return view('admin.region.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'name_ge'=>'required',

        ]);
        $region=new RegionModel();
            $region->name_ge=$request->name_ge;
            $region->name_en=$request->name_en;
            $region->user_id=Auth::user()->id;
        $region->save();
        return redirect(url('admin/region'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/region/'.$id,
                'id'=>$id

            ]
        ];

        $data['region']=RegionModel::where('id',$id)->first();
        return view('admin.region.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'name_ge'=>'required',

        ]);
        $region=RegionModel::where('id',$id)->first();
        $region->name_ge=$request->name_ge;
        $region->name_en=$request->name_en;
        $region->user_id=Auth::user()->id;
        $region->save();
        return redirect(url('admin/region'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=RegionModel::where('id',$id)->delete();
        return back();
    }
    public function add_district(Request $request){
        $data['region']=RegionModel::where('id',$request->id)->first();
        $data['district']=DistrictModel::where('region_id',$request->id)->get();
        return view('admin.region.add_district',$data);
    }
    public function save_district(Request $request){
        $this->validate($request, [

            'name_ge'=>'required',

        ]);
        $district=new DistrictModel();
            $district->name_ge=$request->name_ge;
            $district->name_en=$request->name_en;
            $district->region_id=$request->id;
            $district->user_id=Auth::user()->id;
        $district->save();
    }
    public function edit_district(Request $request){
        return DistrictModel::where('id',$request->id)->first();
    }
    public function update_district(Request $request){
        $this->validate($request, [

            'name_ge'=>'required',

        ]);
        $district=DistrictModel::where('id',$request->id)->first();
        $district->name_ge=$request->name_ge;
        $district->name_en=$request->name_en;
        $district->user_id=Auth::user()->id;
        $district->save();
    }
    public function delete_district(Request $request){
        DistrictModel::where('id',$request->id)->delete();

    }
    public function get_districts(Request $request){
        return DistrictModel::where('region_id',$request->id)->get();
    }
}
