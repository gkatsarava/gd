<?php

namespace App\Http\Controllers\Admin;

use App\Models\DefaultSettingsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefaultSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['about']=DefaultSettingsModel::first();
        return view('admin.default_settings.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/default_settings/'.$id,
                'id'=>$id

            ]
        ];
        $data['about']=DefaultSettingsModel::where('id',$id)->first();
        return view('admin.default_settings.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [




        ]);
        $about=DefaultSettingsModel::where('id',$id)->first();
            $about->email=$request->email;
            $about->phone=$request->phone;
            $about->address_ge=$request->address_ge;
            $about->address_en=$request->address_en;
            $about->contact_email_for_form=$request->contact_email_for_form;
            $about->facebook=$request->facebook;
            $about->twitter=$request->twitter;
            $about->youtube=$request->youtube;
        $about->save();
        return redirect()->intended('admin/default_settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
