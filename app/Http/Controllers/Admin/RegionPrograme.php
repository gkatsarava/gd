<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RegionProgrameModel;
use App\Models\RegionProgramePictureModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegionPrograme extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['partner']=RegionProgrameModel::orderBy('id','DESC')->paginate(15);
        return view('admin.region_programe.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/region_programe'
            ]
        ];
        return view('admin.region_programe.create',$data);
    }

    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/region_programe'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function uploadNewFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/region_programe_attachment'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }

    public function pin_home(Request $request){

        $news_picture=RegionProgramePictureModel::where('id',$request->id)->first();

        DB::table('region_pictures')
            ->where('news_id', $news_picture->news_id)
            ->update(['first_picture' => 0]);


        $news_picture->first_picture='1';
        $news_picture->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {





        $this->validate($request, [


            'image'=>'required',
            'checkedfirst'=>'required'

        ]);
        $news=new RegionProgrameModel();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        //Checking
        if($request->created_at_time!=''){
            $news->created_at=$request->created_at.' '.$request->created_at_time.':00';
        }else{
            $news->created_at=$request->created_at.' '.date('G:i:s');
        }
        $news->user_id=Auth::user()->id;
        $news->file=$request->file;
        $news->save();

        foreach($request->image as $row){
            $news_picture=new RegionProgramePictureModel();
            $news_picture->picture=$row;
            $news_picture->news_id=$news->id;



            $news_picture->save();
        }
        $news_picture_update=RegionProgramePictureModel::where('picture',$request->checkedfirst)->first();
        $news_picture_update->first_picture='1';
        $news_picture_update->save();
        return redirect()->intended('admin/region_programe');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/region_programe/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=RegionProgrameModel::where('id',$id)->first();
        return view('admin.region_programe.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //  dd($request);

        $exp=explode(':',$request->created_at_time);




        $this->validate($request, [


            'image'=>'required'

        ]);
        $news=RegionProgrameModel::where('id',$id)->first();


        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        if($request->created_at_time!=''){
            $news->created_at=$request->created_at.' '.$request->created_at_time.(isset($exp[2]) ? "" : ":00");
        }else{
            $news->created_at=$request->created_at;
        }
        $news->user_id=Auth::user()->id;
        $news->file=$request->file;
        $news->save();


        if(isset($request->image)){
            foreach($request->image as $row){
                $check=RegionProgramePictureModel::where('picture',$row)->first();
                if(!$check){
                    $news_picture=new RegionProgramePictureModel();
                    $news_picture->picture=$row;
                    $news_picture->news_id=$news->id;
                    $news_picture->save();
                }
            }
        }
        return redirect()->intended('admin/region_programe');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RegionProgrameModel::where('id',$id)->delete();
        return back();
    }
    public function delete_picture(Request $request){
        $news=RegionProgramePictureModel::where('id',$request->id)->delete();
    }
    public function delete_attachment(Request $request){
        $news=RegionProgrameModel::where('id',$request->id)->first();
        $news->file='';
        $news->save();
    }
    public function pause_news(Request $request){
        $news=RegionProgrameModel::where('id',$request->id)->first();
        $news->status=$request->status;
        $news->save();
    }
}
