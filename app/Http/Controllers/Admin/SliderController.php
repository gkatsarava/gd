<?php

namespace App\Http\Controllers\Admin;

use App\Models\SliderModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['slider']=SliderModel::paginate(15);
        return view('admin.slide.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/slider'
            ]
        ];
        return view('admin.slide.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $slider=new SliderModel();
            $slider->title_ge=$request->title_ge;
            $slider->title_en=$request->title_en;
            $slider->title_ru=$request->title_ru;
            $slider->link=$request->link;
            $slider->picture=$request->image;
            $slider->user_id=Auth::user()->id;
        $slider->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/slider/'.$id,
                'id'=>$id

            ]
        ];

        $data['slider']=SliderModel::where('id',$id)->first();
        return view('admin.slide.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=SliderModel::where('id',$id)->first();
        $slider->title_ge=$request->title_ge;
        $slider->title_en=$request->title_en;
        $slider->title_ru=$request->title_ru;
        $slider->link=$request->link;
        $slider->picture=$request->image;
        $slider->user_id=Auth::user()->id;
        $slider->save();
        return back();
    }

    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/Slider'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SliderModel::where('id',$id)->delete();
        return back();
    }
}
