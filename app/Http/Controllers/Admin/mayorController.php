<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\mayorModel;
use App\Models\RegionModel;

class mayorController extends Controller
{
    public function index(Request $request)
    {
        $political_party=mayorModel::orderby('id','desc');

        if(isset($request->fname) && $request->fname!=''){
            $political_party->where('name_ge','like','%'.$request->fname.'%');
        }

        if(isset($request->region) && $request->region!=''){
            $political_party->where('region_id','=',$request->region);
        }





        $data['political_party']=$political_party->paginate(15);
        $data['region']=RegionModel::get();
        return view('admin.mayor.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/mayor'
            ]
        ];
        $data['region']=RegionModel::get();
        return view('admin.mayor.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'image'=>'required',
            'name_ge'=>'required',

        ]);

        /*
                "name_ge" => "test"
              "name_en" => "test"
              "majoritarian_percent" => "test"
              "cesco_link" => "test"
              "region" => "2"
              "district" => "2"
              "work_ge" => "<p>test</p>"
              "work_en" => "<p>test</p>"
              "education_ge" => "<p>test</p>"
              "education_en" => "<p>test</p>"
              "facebook" => "test"
              "instagram" => "test"
              "twitter" => "test"
              "youtube" => "test"
              "image" => "bcae5f6b4b8cc1b5c.png"
              "image_real_name" => "Screen Shot 2020-09-11 at 11.47.55 AM.png"*/

        $political_party=new mayorModel();
        $political_party->name_ge=$request->name_ge;
        $political_party->name_en=$request->name_en;


        $political_party->majoritarian_percent=$request->majoritarian_percent;
        $political_party->cesco_link=$request->cesco_link;
        $political_party->region_id=$request->region;

        $political_party->location_ge=$request->location_ge;
        $political_party->location_en=$request->location_en;


        $political_party->district_id=$request->district;
        $political_party->work_ge=$request->work_ge;
        $political_party->work_en=$request->work_en;
        $political_party->education_ge=$request->education_ge;
        $political_party->education_en=$request->education_en;
        $political_party->picture=$request->image;
        $political_party->facebook=$request->facebook;
        $political_party->twitter=$request->twitter;
        $political_party->instagram=$request->instagram;
        $political_party->youtube=$request->youtube;
        $political_party->user_id=Auth::user()->id;
        $political_party->save();


        return redirect(url('admin/mayor'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/mayor/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=mayorModel::where('id',$id)->first();
        $data['region']=RegionModel::get();
        return view('admin.mayor.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'image'=>'required',
            'name_ge'=>'required',

        ]);

        /*
                "name_ge" => "test"
              "name_en" => "test"
              "majoritarian_percent" => "test"
              "cesco_link" => "test"
              "region" => "2"
              "district" => "2"
              "work_ge" => "<p>test</p>"
              "work_en" => "<p>test</p>"
              "education_ge" => "<p>test</p>"
              "education_en" => "<p>test</p>"
              "facebook" => "test"
              "instagram" => "test"
              "twitter" => "test"
              "youtube" => "test"
              "image" => "bcae5f6b4b8cc1b5c.png"
              "image_real_name" => "Screen Shot 2020-09-11 at 11.47.55 AM.png"*/

        $political_party=mayorModel::where('id',$id)->first();
        $political_party->name_ge=$request->name_ge;
        $political_party->name_en=$request->name_en;


        $political_party->majoritarian_percent=$request->majoritarian_percent;
        $political_party->cesco_link=$request->cesco_link;
        $political_party->region_id=$request->region;
        $political_party->location_ge=$request->location_ge;
        $political_party->location_en=$request->location_en;


        $political_party->district_id=$request->district;
        $political_party->work_ge=$request->work_ge;
        $political_party->work_en=$request->work_en;
        $political_party->education_ge=$request->education_ge;
        $political_party->education_en=$request->education_en;
        $political_party->picture=$request->image;
        $political_party->facebook=$request->facebook;
        $political_party->twitter=$request->twitter;
        $political_party->instagram=$request->instagram;
        $political_party->youtube=$request->youtube;
        $political_party->user_id=Auth::user()->id;
        $political_party->save();


        return redirect(url('admin/mayor'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=mayorModel::where('id',$id)->delete();
        return back();
    }
    public function uploadFiles(Request $request){
        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/mayor'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function delete_mayor_picture(Request $request){
        $data=mayorModel::where('id',$request->id)->first();
        $data->picture='';
        $data->save();

    }
}
