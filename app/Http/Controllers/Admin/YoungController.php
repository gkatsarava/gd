<?php

namespace App\Http\Controllers\Admin;

use App\Models\YoungModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class YoungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['about']=YoungModel::first();
        return view('admin.young.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/young/'.$id,
                'id'=>$id

            ]
        ];
        $data['about']=YoungModel::where('id',$id)->first();
        return view('admin.young.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [




        ]);
        $about=YoungModel::where('id',$id)->first();

        $about->desc_ge=$request->desc_ge;
        $about->desc_en=$request->desc_en;

        $about->picture=$request->image;
        $about->save();
        return redirect()->intended('admin/about');
    }

    public function uploadFiles(Request $request){
        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/young'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
