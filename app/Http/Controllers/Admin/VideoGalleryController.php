<?php

namespace App\Http\Controllers\Admin;

use App\Models\VideoGalleryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VideoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['video_gallery']=VideoGalleryModel::paginate(20);
        return view('admin.video_gallery.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/video_gallery'
            ]
        ];
        return view('admin.video_gallery.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'title_ge'=>'required',
            'youtube'=>'required'

        ]);
        $news=new VideoGalleryModel();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->youtube=$request->youtube;
        $news->user_id=Auth::user()->id;
        $news->save();
        return redirect()->intended('admin/video_gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/video_gallery/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=VideoGalleryModel::where('id',$id)->first();
        return view('admin.video_gallery.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'title_ge'=>'required',
            'youtube'=>'required'

        ]);
        $news=VideoGalleryModel::where('id',$id)->first();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->youtube=$request->youtube;
        $news->user_id=Auth::user()->id;
        $news->save();
        return redirect()->intended('admin/video_gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video_gallery=VideoGalleryModel::where('id',$id)->delete();
        return back();
    }
}
