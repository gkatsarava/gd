<?php

namespace App\Http\Controllers\Admin;

use App\Models\BulletinModel;
use App\Models\BulletinPicturesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BulletinController extends Controller
{
    public function index(Request $request){
        if($request->slider==1){
            $data['partner']=BulletinModel::where('slider',$request->slider)->orderBy('id','DESC')->paginate(15);
        }else{
            $data['partner']=BulletinModel::orderBy('id','DESC')->paginate(15);
        }
        return view('admin.bulletin.index',$data);
    }
    public function show(){

    }
    public function create(){
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/bulletin'
            ]
        ];
        return view('admin.bulletin.create',$data);

    }
    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/bulletin'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function uploadNewFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/bulletin_attachment'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }

    public function edit($id){
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/bulletin/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=BulletinModel::where('id',$id)->first();
        return view('admin.bulletin.create',$data);
    }
    public function update(Request $request,$id){

        $this->validate($request, [


            'image'=>'required'

        ]);
        $news=BulletinModel::where('id',$id)->first();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        $news->created_at=$request->created_at;
        $news->user_id=Auth::user()->id;
        $news->file=$request->file;
        $news->save();


        if(isset($request->image)){
            foreach($request->image as $row){
                $check=BulletinPicturesModel::where('picture',$row)->first();
                if(!$check){
                    $news_picture=new BulletinPicturesModel();
                    $news_picture->picture=$row;
                    $news_picture->news_id=$news->id;
                    $news_picture->save();
                }
            }
        }
        return redirect()->intended('admin/bulletin');
    }
    public function store(Request $request){




        $this->validate($request, [


            'image'=>'required',
            'checkedfirst'=>'required'

        ]);
        $news=new BulletinModel();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        $news->created_at=$request->created_at;
        $news->user_id=Auth::user()->id;
        $news->file=$request->file;
        $news->save();

        foreach($request->image as $row){
            $news_picture=new BulletinPicturesModel();
            $news_picture->picture=$row;
            $news_picture->news_id=$news->id;



            $news_picture->save();
        }
        $news_picture_update=BulletinPicturesModel::where('picture',$request->checkedfirst)->first();
        $news_picture_update->first_picture='1';
        $news_picture_update->save();
        return redirect()->intended('admin/bulletin');
    }
    public function destroy(Request $request,$id){

        BulletinModel::where('id',$id)->delete();
        return back();

    }
    public function delete_picture(Request $request){
        $news=BulletinPicturesModel::where('id',$request->id)->delete();
    }
    public function delete_attachment(Request $request){
        $news=BulletinModel::where('id',$request->id)->first();
            $news->file='';
        $news->save();
    }
    public function pin_home(Request $request){

        $news_picture=BulletinPicturesModel::where('id',$request->id)->first();

        DB::table('bulletin_pictures')
            ->where('news_id', $news_picture->news_id)
            ->update(['first_picture' => 0]);


            $news_picture->first_picture='1';
        $news_picture->save();
    }
    public function pin_to_slide(Request $request){
        $news=BulletinModel::where('id',$request->id)->first();
            $news->slider=$request->status;
        $news->save();
    }
    public function pause_news(Request $request){
        $news=BulletinModel::where('id',$request->id)->first();
        $news->status=$request->status;
        $news->save();
    }
}
