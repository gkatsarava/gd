<?php

namespace App\Http\Controllers\Admin;

use App\Models\PoliticalPartyModel;
use App\Models\PoliticalPartyPositionsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PoliticalPartyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $political_party=PoliticalPartyModel::orderBy('sort','ASC');

        if(isset($request->fname) && $request->fname!=''){
            $political_party->where('name_ge','like','%'.$request->fname.'%');
        }

        if(isset($request->position) && $request->position!=''){
            $political_party->where('position_ge','like','%'.$request->position.'%');
        }

        if(isset($request->structure) && $request->structure!=''){
            $political_party->where('structure','like','%'.$request->structure.'%');
        }




        $data['political_party']=$political_party->paginate(50);
        return view('admin.political_party.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/political_party'
            ]
        ];
        return view('admin.political_party.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   /*     "name_ge" => "eqweq"
      "name_en" => "ewqeqweq"
      "position_ge" => "ewqeqw"
      "position_en" => "eqweq"
      "structure_id" => "1"
      "work_ge" => "<p>eqweqw</p>"
      "work_en" => "ewqeqwe"
      "education_ge" => "<p>eqweqw</p>"
      "education_en" => "<p>eqweqw</p>"
      "image" => "bcae5f6a3ffe85018.png"*/


        $this->validate($request, [

            'image'=>'required',
            'name_ge'=>'required',
            'position_ge'=>'required',
            'desc_ge'=>'required',
            'structure'=>'required',

        ]);

            $political_party=new PoliticalPartyModel();
                $political_party->name_ge=$request->name_ge;
                $political_party->name_en=$request->name_en;
                $political_party->position_ge=$request->position_ge;
                $political_party->position_en=$request->position_en;
                $political_party->desc_ge=$request->desc_ge;
                $political_party->desc_en=$request->desc_en;/*
                $political_party->education_ge=$request->education_ge;
                $political_party->education_en=$request->education_en;*/
                $political_party->picture=$request->image;
                $political_party->structure=$request->structure;
                $political_party->facebook=$request->facebook;
                $political_party->twitter=$request->twitter;
                $political_party->instagram=$request->instagram;
                $political_party->youtube=$request->youtube;
                $political_party->user_id=Auth::user()->id;
            $political_party->save();


            return redirect(url('admin/political_party'));


    }
    public function delete_politican_party_picture(Request $request){
        $political=PoliticalPartyModel::where('id',$request->id)->first();
        $political->picture='';
        $political->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/political_party/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=PoliticalPartyModel::where('id',$id)->first();
        return view('admin.political_party.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'image'=>'required',
            'name_ge'=>'required',
            'position_ge'=>'required',
            'desc_ge'=>'required',
            'structure'=>'required',

        ]);

        $political_party=PoliticalPartyModel::where('id',$id)->first();
        $political_party->name_ge=$request->name_ge;
        $political_party->name_en=$request->name_en;
        $political_party->position_ge=$request->position_ge;
        $political_party->position_en=$request->position_en;
        $political_party->desc_ge=$request->desc_ge;
        $political_party->desc_en=$request->desc_en;/*
        $political_party->education_ge=$request->education_ge;
        $political_party->education_en=$request->education_en;*/
        $political_party->picture=$request->image;
        $political_party->structure=$request->structure;
        $political_party->facebook=$request->facebook;
        $political_party->twitter=$request->twitter;
        $political_party->instagram=$request->instagram;
        $political_party->youtube=$request->youtube;
        $political_party->user_id=Auth::user()->id;
        $political_party->save();


        return redirect(url('admin/political_party'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PoliticalPartyModel::where('id',$id)->delete();
        PoliticalPartyPositionsModel::where('political_party_id',$id)->delete();
        return back();
    }
    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/political_party'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function add_positions(Request $request){


        $data['politican']=PoliticalPartyModel::where('id',$request->id)->first();
        $data['Positions']=PoliticalPartyPositionsModel::where('political_party_id',$request->id)->get();
        return view('admin.political_party.add_positions',$data);
    }
    public function save_other_position(Request $request){

        $this->validate($request, [

            'name_ge'=>'required',

        ]);
            $other=new PoliticalPartyPositionsModel();
            $other->political_party_id=$request->id;
            $other->name_ge=$request->name_ge;
            $other->name_en=$request->name_en;
            $other->user_id=Auth::user()->id;
            $other->save();
    }
    public function edit_other_position(Request $request){
        return PoliticalPartyPositionsModel::where('id',$request->id)->first();
    }
    public function update_other_position(Request $request){
        $this->validate($request, [

            'name_ge'=>'required',

        ]);
        $other=PoliticalPartyPositionsModel::where('id',$request->id)->first();
            $other->name_ge=$request->name_ge;
            $other->name_en=$request->name_en;
            $other->user_id=Auth::user()->id;
        $other->save();
    }
    public function delete_other_position(Request $request){
        $other=PoliticalPartyPositionsModel::where('id',$request->id)->delete();

    }
    public function Sort(Request $request){

        foreach($request->sort as $key=> $row) {

            $other = PoliticalPartyModel::where('id', $row)->first();
            $other->sort=$key+1;
            $other->save();
        }


    }

}
