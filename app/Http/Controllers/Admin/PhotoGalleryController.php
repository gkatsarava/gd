<?php

namespace App\Http\Controllers\Admin;

use App\Models\ImageGalleryChildModel;
use App\Models\ImageGalleryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PhotoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['gallery']=ImageGalleryModel::paginate(15);
        return view('admin.photo_gallery.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/photo_gallery'
            ]
        ];
        return view('admin.photo_gallery.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [

            'title_ge'=>'required',
            'desc_ge'=>'required',
            'image'=>'required',

        ]);
        $gallery=new ImageGalleryModel();

        $gallery->title_ge=$request->title_ge;
        $gallery->title_en=$request->title_en;
        $gallery->desc_ge=$request->desc_ge;
        $gallery->desc_en=$request->desc_en;
        $gallery->user_id=Auth::user()->id;
        $gallery->save();

        foreach($request->image as $row){
            $gallery_picture=new ImageGalleryChildModel();
            $gallery_picture->picture=$row;
            $gallery_picture->gallery_id=$gallery->id;
            $gallery_picture->save();
        }
        return redirect()->intended('admin/photo_gallery');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/photo_gallery/'.$id,
                'id'=>$id

            ]
        ];

        $data['news']=ImageGalleryModel::where('id',$id)->first();
        return view('admin.photo_gallery.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'title_ge'=>'required',
            'desc_ge'=>'required',
            'image'=>'required'

        ]);
        $news=ImageGalleryModel::where('id',$id)->first();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->user_id=Auth::user()->id;
        $news->save();


        if(isset($request->image)){
            foreach($request->image as $row){
                $check=ImageGalleryChildModel::where('picture',$row)->first();
                if(!$check){
                    $news_picture=new ImageGalleryChildModel();
                    $news_picture->picture=$row;
                    $news_picture->gallery_id=$news->id;
                    $news_picture->save();
                }
            }
        }
        return redirect()->intended('admin/photo_gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image=ImageGalleryModel::where('id',$id)->delete();
        $child=ImageGalleryChildModel::where('gallery_id',$id)->delete();
        return back();
    }
    public function delete_picture(Request $request){
        $news=ImageGalleryChildModel::where('id',$request->id)->delete();
    }
    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/photo_gallery/'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
}
