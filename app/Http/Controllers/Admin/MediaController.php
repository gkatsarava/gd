<?php

namespace App\Http\Controllers\Admin;

use App\Models\MediaModel;
use App\Models\MediaPicturesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MediaController extends Controller
{
    public function index(){
        $data['partner']=MediaModel::paginate(15);
        return view('admin.media.index',$data);
    }
    public function show(){

    }
    public function create(){
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/media'
            ]
        ];
        return view('admin.media.create',$data);

    }
    public function uploadFiles(Request $request){

        $image = $request->file('file');
        $imageName = uniqid(substr(hash('sha256', $image->getClientOriginalName()), 0, 4)).'.'.$image->getClientOriginalExtension();
        $image->move(public_path('uploads/media'),$imageName);

        return [
            'success' => $imageName,
            'real_name'=>$image->getClientOriginalName()
        ];

    }
    public function edit($id){
        $data = [
            'form' => [
                'method' => 'POST',
                'action' => '/admin/media/'.$id,
                'id'=>$id

            ]
        ];


        $data['news']=MediaModel::where('id',$id)->first();

        return view('admin.media.create',$data);
    }
    public function update(Request $request,$id){

        $this->validate($request, [

            'title_ge'=>'required',
            'desc_ge'=>'required',
            'image'=>'required'

        ]);
        $news=MediaModel::where('id',$id)->first();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        $news->source=$request->source;
        $news->user_id=Auth::user()->id;
        $news->save();


        if(isset($request->image)) {


            foreach($request->image as $row) {

                $check=MediaPicturesModel::where('picture',$row)->first();
                if(!$check){
                    $news_picture = new MediaPicturesModel();
                    $news_picture->picture = $row;
                    $news_picture->news_id = $news->id;
                    $news_picture->save();
                }
            }
        }
        return redirect()->intended('admin/media');
    }
    public function store(Request $request){




        $this->validate($request, [

            'title_ge'=>'required',
            'desc_ge'=>'required',
            'image'=>'required',
            'checkedfirst'=>'required'

        ]);
        $news=new MediaModel();

        $news->title_ge=$request->title_ge;
        $news->title_en=$request->title_en;
        $news->desc_ge=$request->desc_ge;
        $news->desc_en=$request->desc_en;
        $news->youtube=$request->youtube;
        $news->user_id=Auth::user()->id;
        $news->source=$request->source;
        $news->save();

        if(isset($request->image)){
            foreach($request->image as $row){
                $news_picture=new MediaPicturesModel();
                $news_picture->picture=$row;
                $news_picture->news_id=$news->id;



                $news_picture->save();
            }
        }
        $news_picture_update=MediaPicturesModel::where('picture',$request->checkedfirst)->first();
        $news_picture_update->first_picture='1';
        $news_picture_update->save();
        return redirect()->intended('admin/media');
    }
    public function destroy(Request $request,$id){

        MediaModel::where('id',$id)->delete();
        return back();

    }
    public function delete_picture(Request $request){
        $news=MediaPicturesModel::where('id',$request->id)->delete();
    }
    public function pin_home(Request $request){

        $news_picture=MediaPicturesModel::where('id',$request->id)->first();

        DB::table('media_pictures')
            ->where('news_id', $news_picture->news_id)
            ->update(['first_picture' => 0]);


        $news_picture->first_picture='1';
        $news_picture->save();
    }
}
